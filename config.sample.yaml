# vim: ts=2 sts=2 sw=2 et

# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.


# The RSS options.
RSS:
  # A name of this RSS feed which will print in log events.
  Name: Some RSS feed news

  # The RSS Source should be an URL or, a filename (normally for test).
  Source: https://www.engadget.com/rss.xml

  # How long would the next update occurs.
  #
  # This option is a human friendly time duration representation. A duration
  # string is a possibly signed sequence of decimal numbers, each with optional
  # fraction and a unit suffix, such as "300ms", "1.5h" or "2h45m". Valid time
  # units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
  #
  # If you want to update the RSS as defined in XML `ttl` tag by itelf, set this
  # option to 0s (zero second). If RSS does not defined `ttl` and this option
  # is zero, RSS will update in every 20 minutes, as it defined as default ttl
  # in RSS specification.
  TTL: 20m

# The Matrix options.
Matrix:
  # A name of this section which will print in log events.
  Name: Some RSS feed news

  # This is the Matrix home server. It could be a hostname whatever with or
  # without "https://" prefixed, which will be added automatically by the
  # Coybot.
  Home Server: matrix.org

  # The Access Token is required for sending messages to the Matrix.
  # You can make this token by command:
  #
  # curl -X POST https://matrix.org/_matrix/client/r0/login \
  #      -d '{
  #        "type": "m.login.password",
  #        "user": "<username>",
  #        "password": "<password>"
  #      }'
  #
  # This option can be separated into multiple lines for human good reading.
  # Coybot will join them into a single line automatically.
  Access Token:
    MDAxOGx**************************DAxM2lk
    ZW50aWZ**************************D0gMQow
    MDIzY2l**************************XRyaXgu
    b3JnCjA**************************wowMDIx
    Y2lkIG5**************************WJ4Sgow
    MDJmc2l**************************E4ZX6It
    gTkSpdi********G5Yt8Cg

  # This is the ID of the Matrix client device. If this does not correspond to a
  # known client device, a new device will be created. Or just leave it empty,
  # the Matrix will auto-generate a new one.
  Device ID:

  # The Matrix handles any kind of confersision in a logic called room, even
  # there're only two persons chat directly, or some nerd just make notes in a
  # only-one-person room. This ID can be found in Riot by click on room's
  # Settings.
  Room ID: "!GnZsRcR***********:matrix.org"

  # Template is the way the RSS feed news showing up in Matrix/Telegram.
  # There're 2 options about template: Format and Template.
  #
  # Format indicates which format the Template written in.
  # Coybot suports template in 3 formats: Text, HTML and Markdown.
  #
  #   * Text Template renders news in plain text format. Almost all platforms
  #     supports this format. Matrix and Telegram support it fully but beleave
  #     me it's really plain-no paragram, no bold, no italic, no link...
  #     Anyway, it's supported on almost all platforms as the last chance.
  #   * HTML Template renders news in HTML format. Matrix supports full HTML
  #     syntax, but Telegram supports limited HTML syntax only. See
  #     https://core.telegram.org/bots/api#html-style.
  #   * Markdown Template renders news in Markdown format. Matrix dose not
  #     support Markdown, and Telegram supports limited Markdown only. See
  #     https://core.telegram.org/bots/api#markdown-style.
  #
  # The following option is the template for rendering HTML. The syntax can be
  # found at https://golang.org/pkg/text/template/.
  #
  # There're 2 template functions:
  #
  # 1. htmlToMarkdown <HTML content>
  #
  #    This function is able to transform HTML content into Markdown.
  #
  # 2. removeAd <ad string>
  #
  #    This function is able to remove Ads from content.
  #
  # Multiple functions can be cascaded in brackets. See sample below.
  #
  # NOTE: Pay attention to the tailling "|-" at the first line. YAML file
  # acts a string value wrote in multiple lines as a single line by join them
  # with a space character. To keep the lines in format, the "|-" is required.
  # Also, you can add a "\n" at the end of each line, so that the "|-" is not
  # reuired.
  Format: html
  Template:
    <p>On {{.PubDate.Format "2006-01-02 15:04:05 MST"}} Engadget published</p>
    <h6><a href="{{.Link}}">《{{.Title}}》</a></h6>
    <div>{{removeAd .Description `some AD string`}}</div>
    <hr>

# The Telegram options.
Telegram:
  # A name of this section which will print in log events.
  Name: Some RSS feed news

  # Token is used to authorizing when message is sending on Telegram.
  # See https://core.telegram.org/bots/api#authorizing-your-bot.
  Token: 123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11

  # Chat ID is a number which indicates a Telegram chat target.
  # See https://core.telegram.org/bots/api#sendmessage.
  Chat ID: 123456

  Format: markdown
  Template: |-
    On {{.PubDate.Format "2006-01-02 15:04:05 MST"}}, Engadget published
    [{{.Title}}]({{.Link}})

    {{htmlToMarkdown (removeAd .Description `some AD string`)}}

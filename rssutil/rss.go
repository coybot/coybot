// Copyright 2018 coy <https://gitlab.com/coygo>. All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package rssutil

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"html"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"gitlab.com/coybot/coybot/log"

	"golang.org/x/net/html/charset"
)

// DefaultTTL is the TTL who will be used when when there's no TTL specified,
// e.g. Serve was called without forceTTL when RSS has no TTL attribute.
const DefaultTTL = time.Duration(20) * time.Minute

// RSSFeedInitNotifier represents a notifier who will be triggered at initial
// RSS content update. The initial update which means the first time RSS
// content get updated. All the RSS items will be sent to this notifier.
type RSSFeedInitNotifier func(items RSSItems, userData interface{})

// RSSUpdateNotifier represents a notifier who will be triggered at the time
// RSS content updated. All the newly created and changed RSS items will be sent
// to this notifier.
type RSSUpdateNotifier func(items RSSItems, userData interface{})

type RSSUtil struct {
	rss          RSS
	lastUpdateAt time.Time

	source string // url or filename
	origin []byte // the content

	userData interface{} // user custom data will be send to notifiers.

	stopSignal chan struct{} // stop-signal

	FeedInitNotifier RSSFeedInitNotifier
	UpdateNotifier   RSSUpdateNotifier
}

// Feed feeds RSS content from source.
func (ru *RSSUtil) Feed(source string) error {
	var f func(string) ([]byte, error)
	if strings.HasPrefix(source, "http") {
		log.Tracef("feed from url: %q", source)
		f = readURL
	} else {
		log.Tracef("feed from file: %q", source)
		f = ioutil.ReadFile
	}

	data, err := f(source)
	if err != nil {
		return err
	}

	if err = ru.feed(data); err != nil {
		return err
	}

	// NOTE: execute only when ru.source is not set.
	if ru.source == "" && ru.FeedInitNotifier != nil {
		log.Traceln("trigger FeedInitNotifier()")
		ru.FeedInitNotifier(ru.rss.Channel.Items, ru.userData)
	}

	ru.source = source

	return nil
}

func (ru *RSSUtil) feed(data []byte) error {
	var x struct {
		Version string     `xml:"version,attr" json:"version"`
		Channel RSSChannel `xml:"channel"      json:"channel"`
	}
	// The default xml.Unmarshal handles only UTF-8 encoded text. To support
	// other encoding, e.g. ISO-8859-1, we defined unmarshaler as below.
	unmarshal := func(data []byte, v interface{}) error {
		// Remove illegal XML characters.
		re := regexp.MustCompile("[\u0000-\u0008\u000b\u000c\u000e-\u001f\u0080-\u0084\u0086-\u009f]")
		data = re.ReplaceAll(data, []byte(""))

		// Decode it.
		decoder := xml.NewDecoder(bytes.NewReader(data))
		decoder.CharsetReader = charset.NewReaderLabel
		return decoder.Decode(v)
	}
	if err := unmarshal(data, &x); err != nil {
		return err
	}

	ru.origin = data
	ru.lastUpdateAt = time.Now().UTC()
	ru.rss.Version = x.Version
	ru.rss.Channel = x.Channel

	channel := &ru.rss.Channel // A shortcut

	// Trim spaces
	cutset := " \n\t"
	channel.Title = strings.Trim(channel.Title, cutset)
	channel.Link = strings.Trim(channel.Link, cutset)
	channel.Description = strings.Trim(channel.Description, cutset)
	channel.Language = strings.Trim(channel.Language, cutset)
	channel.Copyright = strings.Trim(channel.Copyright, cutset)
	channel.ManagingEditor = strings.Trim(channel.ManagingEditor, cutset)
	channel.WebMaster = strings.Trim(channel.WebMaster, cutset)
	channel.Generator = strings.Trim(channel.Generator, cutset)
	channel.Docs = strings.Trim(channel.Docs, cutset)
	for i := 0; i < len(channel.Items); i++ {
		channel.Items[i].Title = strings.Trim(channel.Items[i].Title, cutset)
		channel.Items[i].Link = strings.Trim(channel.Items[i].Link, cutset)
		channel.Items[i].Description = strings.Trim(channel.Items[i].Description, cutset)
		channel.Items[i].Author = strings.Trim(channel.Items[i].Author, cutset)
		channel.Items[i].Comments = strings.Trim(channel.Items[i].Comments, cutset)
	}

	// Unescape HTML
	channel.Title = html.UnescapeString(channel.Title)
	channel.Link = html.UnescapeString(channel.Link)
	channel.Description = html.UnescapeString(channel.Description)
	channel.Language = html.UnescapeString(channel.Language)
	channel.Copyright = html.UnescapeString(channel.Copyright)
	channel.ManagingEditor = html.UnescapeString(channel.ManagingEditor)
	channel.WebMaster = html.UnescapeString(channel.WebMaster)
	channel.Generator = html.UnescapeString(channel.Generator)
	channel.Docs = html.UnescapeString(channel.Docs)
	for i := 0; i < len(channel.Items); i++ {
		channel.Items[i].Title = html.UnescapeString(channel.Items[i].Title)
		channel.Items[i].Link = html.UnescapeString(channel.Items[i].Link)
		channel.Items[i].Description = html.UnescapeString(channel.Items[i].Description)
		channel.Items[i].Author = html.UnescapeString(channel.Items[i].Author)
		channel.Items[i].Comments = html.UnescapeString(channel.Items[i].Comments)
	}

	return nil
}

func readURL(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("getting %s: %s", url, resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return data, nil
}

// Update updates RSS content from URL.
func (ru *RSSUtil) Update() error {
	if ru.source == "" {
		return fmt.Errorf("empty source")
	}

	originItems := ru.rss.Channel.Items // NOTE: a copy of ru.rss.Channel.Items

	if err := ru.Feed(ru.source); err != nil {
		return err
	}

	if ru.UpdateNotifier != nil {
		newItems := RSSItems{}
		seen := make(map[string]bool)

		for _, it := range originItems {
			seen[it.MD5()] = true
		}

		for _, it := range ru.rss.Channel.Items {
			if !seen[it.MD5()] {
				newItems = append(newItems, it)
			}
		}

		log.Debugf("Update %d new items", len(newItems))

		if len(newItems) > 0 {
			log.Traceln("trigger UpdateNotifier()")
			ru.UpdateNotifier(newItems, ru.userData)
		}
	}

	return nil
}

// Serve starts the default RSS service to keep the content update automatically
// in background in duration forceTTL.
func (ru *RSSUtil) Serve(forceTTL time.Duration) {
	var ttl time.Duration

	if ru.FeedInitNotifier != nil {
		ru.FeedInitNotifier(ru.rss.Channel.Items, ru.userData)
	}

	if forceTTL > 0 {
		ttl = forceTTL
	} else if ru.rss.Channel.TTL > 0 {
		ttl = time.Duration(ru.rss.Channel.TTL) * time.Minute
	} else {
		ttl = DefaultTTL
	}
	log.Infof("Schedule RSS update at %s", time.Now().Add(ttl).Format("2006-01-02 15:04:05"))

	ticker := time.NewTicker(ttl) // TODO: use time.NewTimer() to handle ttl change in-place.
	defer ticker.Stop()

	ru.stopSignal = make(chan struct{})

endless:
	for {
		select {
		case t := <-ticker.C:
			if err := ru.Update(); err != nil {
				log.Error(err)
			}

			log.Infof("Schedule RSS update at %s", t.Add(ttl).Format("2006-01-02 15:04:05"))

		case <-ru.stopSignal:
			log.Info("Stopped")
			break endless
		}
	}
}

// Stop send a stop-signal to stop the default RSS service.
func (ru *RSSUtil) Stop() { ru.stopSignal <- struct{}{} }

var ru RSSUtil

// Serve starts the default RSS service to keep the content update automatically
// in background in duration forceTTL. It always return an error.
func Serve(source string, forceTTL time.Duration, userData interface{},
	onRSSFeedInit RSSFeedInitNotifier, onRSSUpdate RSSUpdateNotifier) (err error) {

	if err = ru.Feed(source); err != nil {
		return err
	}

	log.Infof("Start to serve at %s", time.Now().Format("2006-01-02 15:04:05"))
	items := ru.rss.Channel.Items // a shortcut
	log.Infof("Initialized by %d items", len(items))
	log.Debugf("Initialized by %d items: %q", len(items), items)

	ru.userData = userData
	ru.FeedInitNotifier = onRSSFeedInit
	ru.UpdateNotifier = onRSSUpdate

	ru.Serve(forceTTL)

	// The line above, `ru.Serve(forceTTL)`, returns only on stop-signal is
	// received.
	return fmt.Errorf("force stopped")
}

// Stop send a stop-signal to stop the default RSS service.
func Stop() { ru.Stop() }

// vim: ts=4 sts=4 sw=4 noet

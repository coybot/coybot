// Copyright 2018 coy <https://gitlab.com/coygo>. All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package rssutil

import (
	"testing"
	"time"

	"gitlab.com/coybot/coybot/log"
)

var Beijing = time.FixedZone("Asia/Beijing", int((8 * time.Hour).Seconds()))
var NewYork = time.FixedZone("America/New_York", int((-4 * time.Hour).Seconds()))

func TestFeedFromFile(t *testing.T) {
	var ru RSSUtil
	var err error
	var source string

	source = "sampleRss091.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "0.91"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "WriteTheWeb"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "http://writetheweb.com"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "News for web users that write back"; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "en-us"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if target := "Copyright 2000, WriteTheWeb team."; ch.Copyright != target {
			t.Errorf("must equal %q: %q", target, ch.Copyright)
		}

		if target := "editor@writetheweb.com"; ch.ManagingEditor != target {
			t.Errorf("must equal %q: %q", target, ch.ManagingEditor)
		}

		if target := "webmaster@writetheweb.com"; ch.WebMaster != target {
			t.Errorf("must equal %q: %q", target, ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if ch.LastBuildDate != nil {
			t.Errorf("must nil: %q", ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if ch.Docs != "" {
			t.Errorf("must empty: %q", ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image == nil {
			t.Errorf("must not nil: %q", ch.Image)
		} else {
			img := RSSImage{
				URL:         "http://writetheweb.com/images/mynetscape88.gif",
				Title:       "WriteTheWeb",
				Link:        "http://writetheweb.com",
				Width:       88,
				Height:      31,
				Description: "News for web users that write back",
			}

			if target := img; !ch.Image.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Image)
			}

			if target := img.URL; ch.Image.URL != target {
				t.Errorf("must equal %q: %q", target, ch.Image.URL)
			}

			if target := img.Title; ch.Image.Title != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Title)
			}

			if target := img.Link; ch.Image.Link != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Link)
			}

			if target := img.Width; ch.Image.Width != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Width)
			}

			if target := img.Height; ch.Image.Height != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Height)
			}

			if target := img.Description; ch.Image.Description != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Description)
			}
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 6; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title string
				link  string
				desc  string
			}{
				{
					title: "Giving the world a pluggable Gnutella",
					link:  "http://writetheweb.com/read.php?item=24",
					desc:  "WorldOS is a framework on which to build programs that work like Freenet or Gnutella -allowing distributed applications using peer-to-peer routing.",
				}, {
					title: "Syndication discussions hot up",
					link:  "http://writetheweb.com/read.php?item=23",
					desc:  "After a period of dormancy, the Syndication mailing list has become active again, with contributions from leaders in traditional media and Web syndication.",
				}, {
					title: "Personal web server integrates file sharing and messaging",
					link:  "http://writetheweb.com/read.php?item=22",
					desc:  "The Magi Project is an innovative project to create a combined personal web server and messaging system that enables the sharing and synchronization of information across desktop, laptop and palmtop devices.",
				}, {
					title: "Syndication and Metadata",
					link:  "http://writetheweb.com/read.php?item=21",
					desc:  "RSS is probably the best known metadata format around. RDF is probably one of the least understood. In this essay, published on my O'Reilly Network weblog, I argue that the next generation of RSS should be based on RDF.",
				}, {
					title: "UK bloggers get organised",
					link:  "http://writetheweb.com/read.php?item=20",
					desc:  "Looks like the weblogs scene is gathering pace beyond the shores of the US. There's now a UK-specific page on weblogs.com, and a mailing list at egroups.",
				}, {
					title: "Yournamehere.com more important than anything",
					link:  "http://writetheweb.com/read.php?item=19",
					desc:  "Whatever you're publishing on the web, your site name is the most valuable asset you have, according to Carl Steadman.",
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range ch.Items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}
			}
		}
	})

	source = "sampleRss092.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "0.92"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Dave Winer: Grateful Dead"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "http://www.scripting.com/blog/categories/gratefulDead.html"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "A high-fidelity Grateful Dead song every day. This is" +
			" where we're experimenting with enclosures on RSS news items" +
			" that download when you're not using your computer. If it works" +
			" (it will) it will be the end of the Click-And-Wait multimedia" +
			" experience on the Internet."; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if ch.Language != "" {
			t.Errorf("must empty: %q", ch.Language)
		}

		if ch.Copyright != "" {
			t.Errorf("must empty: %q", ch.Copyright)
		}

		if target := "dave@userland.com (Dave Winer)"; ch.ManagingEditor != target {
			t.Errorf("must equal %q: %q", target, ch.ManagingEditor)
		}

		if target := "dave@userland.com (Dave Winer)"; ch.WebMaster != target {
			t.Errorf("must equal %q: %q", target, ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if target := RFC822(time.Date(
			2001, 4, 13, 19, 23, 2, 0, time.UTC),
		); !ch.LastBuildDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if target := "http://backend.userland.com/rss092"; ch.Docs != target {
			t.Errorf("must equal %q: %q", target, ch.Docs)
		}

		if ch.Cloud == nil {
			t.Errorf("must not nil: %q", ch.Cloud)
		} else {
			cloud := RSSCloud{
				Domain:            "data.ourfavoritesongs.com",
				Port:              80,
				Path:              "/RPC2",
				RegisterProcedure: "ourFavoriteSongs.rssPleaseNotify",
				Protocol:          "xml-rpc",
			}

			if target := cloud; !ch.Cloud.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Cloud)
			}

			if target := cloud.Domain; ch.Cloud.Domain != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.Domain)
			}

			if target := cloud.Port; ch.Cloud.Port != target {
				t.Errorf("must equal %d: %d", target, ch.Cloud.Port)
			}

			if target := cloud.Path; ch.Cloud.Path != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.Path)
			}

			if target := cloud.RegisterProcedure; ch.Cloud.RegisterProcedure != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.RegisterProcedure)
			}

			if target := cloud.Protocol; ch.Cloud.Protocol != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.Protocol)
			}
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image != nil {
			t.Errorf("must nil: %q", ch.Image)
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 22; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []RSSItem{
				{
					Description: "It's been a few days since I added a song to the Grateful Dead channel. Now that there are all these new Radio users, many of whom are tuned into this channel (it's #16 on the hotlist of upstreaming Radio users, there's no way of knowing how many non-upstreaming users are subscribing, have to do something about this..). Anyway, tonight's song is a live version of Weather Report Suite from Dick's Picks Volume 7. It's wistful music. Of course a beautiful song, oft-quoted here on Scripting News. <i>A little change, the wind and rain.</i>",
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/weatherReportDicksPicsVol7.mp3",
						Length: 6182912,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Kevin Drennan started a <a href="http://deadend.editthispage.com/">Grateful Dead Weblog</a>. Hey it's cool, he even has a <a href="http://deadend.editthispage.com/directory/61">directory</a>. <i>A Frontier 7 feature.</i>`,
					Source: &RSSSource{
						Source: "Scripting News",
						URL:    "http://scriptingnews.userland.com/xml/scriptingNews2.xml",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/GDead/AGDL/other1.html">The Other One</a>, live instrumental, One From The Vault. Very rhythmic very spacy, you can listen to it many times, and enjoy something new every time.`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/theOtherOne.mp3",
						Length: 6666097,
						Type:   "audio/mpeg",
					},
				}, {
					Description: "This is a test of a change I just made. Still diggin..",
				}, {
					Description: `The HTML rendering almost <a href="http://validator.w3.org/check/referer">validates</a>. Close. Hey I wonder if anyone has ever published a style guide for ALT attributes on images? What are you supposed to say in the ALT attribute? I sure don't know. If you're blind send me an email if u cn rd ths.`,
				}, {
					Description: `<a href="http://www.cs.cmu.edu/~mleone/gdead/dead-lyrics/Franklin's_Tower.txt">Franklin's Tower</a>, a live version from One From The Vault.`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/franklinsTower.mp3",
						Length: 6701402,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Moshe Weitzman says Shakedown Street is what I'm lookin for for tonight. I'm listening right now. It's one of my favorites. "Don't tell me this town ain't got no heart." Too bright. I like the jazziness of Weather Report Suite. Dreamy and soft. How about The Other One? "Spanish lady come to me.."`,
					Source: &RSSSource{
						Source: "Scripting News",
						URL:    "http://scriptingnews.userland.com/xml/scriptingNews2.xml",
					},
				}, {
					Description: `<a href="http://www.scripting.com/mp3s/youWinAgain.mp3">The news is out</a>, all over town..<p>` + "\n" +
						`You've been seen, out runnin round. <p>` + "\n" +
						`The lyrics are <a href="http://www.cs.cmu.edu/~mleone/gdead/dead-lyrics/You_Win_Again.txt">here</a>, short and sweet. <p>` + "\n" +
						`<i>You win again!</i>`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/youWinAgain.mp3",
						Length: 3874816,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://www.getlyrics.com/lyrics/grateful-dead/wake-of-the-flood/07.htm">Weather Report Suite</a>: "Winter rain, now tell me why, summers fade, and roses die? The answer came. The wind and rain. Golden hills, now veiled in grey, summer leaves have blown away. Now what remains? The wind and rain."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/weatherReportSuite.mp3",
						Length: 12216320,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/gdead/agdl/darkstar.html">Dark Star</a> crashes, pouring its light into ashes.`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/darkStar.mp3",
						Length: 10889216,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `DaveNet: <a href="http://davenet.userland.com/2001/01/21/theUsBlues">The U.S. Blues</a>.`,
				}, {
					Description: `Still listening to the US Blues. <i>"Wave that flag, wave it wide and high.."</i> Mistake made in the 60s. We gave our country to the assholes. Ah ah. Let's take it back. Hey I'm still a hippie. <i>"You could call this song The United States Blues."</i>`,
				}, {
					Description: `<a href="http://www.sixties.com/html/garcia_stack_0.html"><img src="http://www.scripting.com/images/captainTripsSmall.gif" height="51" width="42" border="0" hspace="10" vspace="10" align="right"></a>In celebration of today's inauguration, after hearing all those great patriotic songs, America the Beautiful, even The Star Spangled Banner made my eyes mist up. It made my choice of Grateful Dead song of the night realllly easy. Here are the <a href="http://searchlyrics2.homestead.com/gd_usblues.html">lyrics</a>. Click on the audio icon to the left to give it a listen. "Red and white, blue suede shoes, I'm Uncle Sam, how do you do?" It's a different kind of patriotic music, but man I love my country and I love Jerry and the band. <i>I truly do!</i>`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/usBlues.mp3",
						Length: 5272510,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Grateful Dead: "Tennessee, Tennessee, ain't no place I'd rather be."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/tennesseeJed.mp3",
						Length: 3442648,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Ed Cone: "Had a nice Deadhead experience with my wife, who never was one but gets the vibe and knows and likes a lot of the music. Somehow she made it to the age of 40 without ever hearing Wharf Rat. We drove to Jersey and back over Christmas with the live album commonly known as Skull and Roses in the CD player much of the way, and it was cool to see her discover one the band's finest moments. That song is unique and underappreciated. Fun to hear that disc again after a few years off -- you get Jerry as blues-guitar hero on Big Railroad Blues and a nice version of Bertha."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/darkStarWharfRat.mp3",
						Length: 27503386,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/GDead/AGDL/fotd.html">Tonight's Song</a>: "If I get home before daylight I just might get some sleep tonight."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/friendOfTheDevil.mp3",
						Length: 3219742,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/GDead/AGDL/uncle.html">Tonight's song</a>: "Come hear Uncle John's Band by the river side. Got some things to talk about here beside the rising tide."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/uncleJohnsBand.mp3",
						Length: 4587102,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://www.cs.cmu.edu/~mleone/gdead/dead-lyrics/Me_and_My_Uncle.txt">Me and My Uncle</a>: "I loved my uncle, God rest his soul, taught me good, Lord, taught me all I know. Taught me so well, I grabbed that gold and I left his dead ass there by the side of the road."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/meAndMyUncle.mp3",
						Length: 2949248,
						Type:   "audio/mpeg",
					},
				}, {
					Description: "Truckin, like the doo-dah man, once told me gotta play your hand. Sometimes the cards ain't worth a dime, if you don't lay em down.",
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/truckin.mp3",
						Length: 4847908,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Two-Way-Web: <a href="http://www.thetwowayweb.com/payloadsForRss">Payloads for RSS</a>. "When I started talking with Adam late last year, he wanted me to think about high quality video on the Internet, and I totally didn't want to hear about it."`,
				}, {
					Description: "A touch of gray, kinda suits you anyway..",
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/touchOfGrey.mp3",
						Length: 5588242,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://www.sixties.com/html/garcia_stack_0.html"><img src="http://www.scripting.com/images/captainTripsSmall.gif" height="51" width="42" border="0" hspace="10" vspace="10" align="right"></a>In celebration of today's inauguration, after hearing all those great patriotic songs, America the Beautiful, even The Star Spangled Banner made my eyes mist up. It made my choice of Grateful Dead song of the night realllly easy. Here are the <a href="http://searchlyrics2.homestead.com/gd_usblues.html">lyrics</a>. Click on the audio icon to the left to give it a listen. "Red and white, blue suede shoes, I'm Uncle Sam, how do you do?" It's a different kind of patriotic music, but man I love my country and I love Jerry and the band. <i>I truly do!</i>`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/usBlues.mp3",
						Length: 5272510,
						Type:   "audio/mpeg",
					},
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i]; !ch.Items[i].Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i])
				}
			}
		}
	})

	source = "rss2sample.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "2.0"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Liftoff News"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "http://liftoff.msfc.nasa.gov/"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "Liftoff to Space Exploration."; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "en-us"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if ch.Copyright != "" {
			t.Errorf("must empty: %q", ch.Copyright)
		}

		if target := "editor@example.com"; ch.ManagingEditor != target {
			t.Errorf("must equal %q: %q", target, ch.ManagingEditor)
		}

		if target := "webmaster@example.com"; ch.WebMaster != target {
			t.Errorf("must equal %q: %q", target, ch.WebMaster)
		}

		if target := RFC822(time.Date(
			2003, 6, 10, 4, 0, 0, 0, time.UTC,
		)); !ch.PubDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.PubDate)
		}

		if target := RFC822(time.Date(
			2003, 6, 10, 9, 41, 1, 0, time.UTC,
		)); !ch.LastBuildDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if target := "Weblog Editor 2.0"; ch.Generator != target {
			t.Errorf("must equal %q: %q", target, ch.Generator)
		}

		if target := "http://blogs.law.harvard.edu/tech/rss"; ch.Docs != target {
			t.Errorf("must equal %q: %q", target, ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image != nil {
			t.Errorf("must nil: %q", ch.Image)
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 4; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title   string
				link    string
				desc    string
				pubDate RFC822
				guid    RSSGUID
			}{
				{
					title:   "Star City",
					link:    "http://liftoff.msfc.nasa.gov/news/2003/news-starcity.asp",
					desc:    `How do Americans get ready to work with Russians aboard the International Space Station? They take a crash course in culture, language and protocol at Russia's <a href="http://howe.iki.rssi.ru/GCTC/gctc_e.htm">Star City</a>.`,
					pubDate: RFC822(time.Date(2003, 6, 3, 9, 39, 21, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/06/03.html#item573"},
				}, {
					desc:    `Sky watchers in Europe, Asia, and parts of Alaska and Canada will experience a <a href="http://science.nasa.gov/headlines/y2003/30may_solareclipse.htm">partial eclipse of the Sun</a> on Saturday, May 31st.`,
					pubDate: RFC822(time.Date(2003, 5, 30, 11, 6, 42, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/05/30.html#item572"},
				}, {
					title:   "The Engine That Does More",
					link:    "http://liftoff.msfc.nasa.gov/news/2003/news-VASIMR.asp",
					desc:    "Before man travels to Mars, NASA hopes to design new engines that will let us fly through the Solar System more quickly.  The proposed VASIMR engine would do that.",
					pubDate: RFC822(time.Date(2003, 5, 27, 8, 37, 32, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/05/27.html#item571"},
				}, {
					title:   "Astronauts' Dirty Laundry",
					link:    "http://liftoff.msfc.nasa.gov/news/2003/news-laundry.asp",
					desc:    "Compared to earlier spacecraft, the International Space Station has many luxuries, but laundry facilities are not one of them.  Instead, astronauts have other options.",
					pubDate: RFC822(time.Date(2003, 5, 20, 8, 56, 2, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/05/20.html#item570"},
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}

				if ch.Items[i].Author != "" {
					t.Errorf("must empty: %q", ch.Items[i].Author)
				}

				if ch.Items[i].Categories != nil {
					t.Errorf("must nil: %q", ch.Items[i].Categories)
				}

				if ch.Items[i].Comments != "" {
					t.Errorf("must empty: %q", ch.Items[i].Comments)
				}

				if ch.Items[i].Enclosure != nil {
					t.Errorf("must nil: %q", ch.Items[i].Enclosure)
				}

				if target := items[i].guid; !ch.Items[i].GUID.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].GUID)
				}

				if target := items[i].pubDate; !ch.Items[i].PubDate.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].PubDate)
				}

				if ch.Items[i].Source != nil {
					t.Errorf("must nil: %q", ch.Items[i].Source)
				}
			}
		}
	})

	source = "engadget_en-us.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "2.0"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Engadget RSS Feed"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "https://www.engadget.com/rss.xml"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "Engadget is a web magazine with obsessive daily" +
			" coverage of everything new in gadgets and consumer" +
			" electronics"; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "en-us"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if target := "Copyright 2018 AOL Inc. The contents of this feed are" +
			" available for non-commercial use only."; ch.Copyright != target {
			t.Errorf("must equal %q: %q", target, ch.Copyright)
		}

		if ch.ManagingEditor != "" {
			t.Errorf("must empty: %q", ch.ManagingEditor)
		}

		if ch.WebMaster != "" {
			t.Errorf("must empty: %q", ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if ch.LastBuildDate != nil {
			t.Errorf("must nil: %q", ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if ch.Docs != "" {
			t.Errorf("must empty: %q", ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image == nil {
			t.Errorf("must not nil: %q", ch.Image)
		} else {
			img := RSSImage{
				URL:   "https://www.blogsmithmedia.com/www.engadget.com/media/feedlogo.gif?cachebust=true",
				Title: "Engadget RSS Feed",
				Link:  "https://www.engadget.com/rss.xml",
			}

			if target := img; !ch.Image.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Image)
			}

			if target := img.URL; ch.Image.URL != target {
				t.Errorf("must equal %q: %q", target, ch.Image.URL)
			}

			if target := img.Title; ch.Image.Title != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Title)
			}

			if target := img.Link; ch.Image.Link != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Link)
			}

			if target := 0; ch.Image.Width != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Width)
			}

			if target := 0; ch.Image.Height != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Height)
			}

			if ch.Image.Description != "" {
				t.Errorf("must empty: %q", ch.Image.Description)
			}
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 25; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title      string
				link       string
				guid       RSSGUID
				comments   string
				desc       string
				categories []RSSCategory
				pubDate    RFC822
			}{
				{
					title:    "First 'State of Decay 2' patch is as large as the game itself",
					link:     "https://www.engadget.com/2018/06/03/first-state-of-decay-2-patch/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/first-state-of-decay-2-patch/"},
					comments: "https://www.engadget.com/2018/06/03/first-state-of-decay-2-patch/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/3fea43fc73f24ec46e57f8ad9f70afc1/206422734/state-of-decay-2-leap.jpg" />If you have State of Decay 2 but haven't fired it up in a while, you'll have to be... patient.  Undead Labs has released its first patch for the zombie-slaying title, and it's a whopping 20GB -- that's about as large as the install on the Xbox One....`,
					categories: []RSSCategory{
						{Category: "av"},
						{Category: "games"},
						{Category: "gaming"},
						{Category: "patch"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "stateofdecay2"},
						{Category: "undeadlabs"},
						{Category: "update"},
						{Category: "videogames"},
						{Category: "windows"},
						{Category: "xboxone"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 5, 0, 0, NewYork)),
				}, {
					title:    "Square Enix has stopped working on 'Go' mobile games",
					link:     "https://www.engadget.com/2018/06/03/square-enix-stops-work-on-go-mobile-games/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/square-enix-stops-work-on-go-mobile-games/"},
					comments: "https://www.engadget.com/2018/06/03/square-enix-stops-work-on-go-mobile-games/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/72e358c0b5220acfca6990bc907e5fa1/206422511/lara-croft-go-snakes.jpg" />In many ways, Square Enix' Go titles were textbook examples of how to make good mobile games: for a little cash up front, you'd receive a truly mobile-friendly experience that felt like it received just as much attention as a blockbuster console rele...`,
					categories: []RSSCategory{
						{Category: "deusexgo"},
						{Category: "games"},
						{Category: "gaming"},
						{Category: "gear"},
						{Category: "hitmango"},
						{Category: "laracroftgo"},
						{Category: "mobile"},
						{Category: "smartphone"},
						{Category: "squareenix"},
						{Category: "squareenixmontreal"},
						{Category: "videogames"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 21, 33, 0, 0, NewYork)),
				}, {
					title:    "Google incubator's New York City subway app offers Waze-like alerts",
					link:     "https://www.engadget.com/2018/06/03/google-incubator-pigeon-new-york-city-subway-app/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/google-incubator-pigeon-new-york-city-subway-app/"},
					comments: "https://www.engadget.com/2018/06/03/google-incubator-pigeon-new-york-city-subway-app/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/2c28c7a25aa1d91dceb317857c72784/206422636/pigeon-area-120.jpg" />Crowdsourced navigation apps like Waze are helpful for avoiding the real-world hazards that conventional apps don't mention.  But what if you use mass transit -- why can't you get alerts that go beyond official route closures and delays?  You can now...`,
					categories: []RSSCategory{
						{Category: "area120"},
						{Category: "crowdsourcing"},
						{Category: "gear"},
						{Category: "google"},
						{Category: "internet"},
						{Category: "ios"},
						{Category: "iphone"},
						{Category: "masstransit"},
						{Category: "mobile"},
						{Category: "pigeon"},
						{Category: "publictransportation"},
						{Category: "subway"},
						{Category: "transportation"},
						{Category: "waze"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 20, 4, 0, 0, NewYork)),
				}, {
					title:    "Apple Music's web player now streams whole songs",
					link:     "https://www.engadget.com/2018/06/03/apple-music-web-player-now-streams-whole-songs/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/apple-music-web-player-now-streams-whole-songs/"},
					comments: "https://www.engadget.com/2018/06/03/apple-music-web-player-now-streams-whole-songs/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=5000%2C3258%2C0%2C0&quality=85&format=jpg&resize=1600%2C1043&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F5689c776d472bd07039ee179a9ff708%2F205936468%2Fman-holds-an-apple-iphone5s-as-he-uses-apple-music-app-on-october-11-picture-id860462658&client=a1acac3e1b3290917d92&signature=dc1b72e846fa9c5b7270dd1ac7b6f20a59bd13b1" />To date, playing full songs on Apple Music has meant firing up iTunes or the mobile app. You could listen to samples on the web, but that wasn't much different than playing iTunes Store clips years ago. This appears to be changing, though. Reddit use...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "applemusic"},
						{Category: "gear"},
						{Category: "internet"},
						{Category: "music"},
						{Category: "services"},
						{Category: "streaming"},
						{Category: "web"},
						{Category: "wwdc"},
						{Category: "wwdc2018"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 18, 32, 0, 0, NewYork)),
				}, {
					title:    "Outgoing Missouri governor signs revenge porn law",
					link:     "https://www.engadget.com/2018/06/03/missouri-governor-revenge-porn-law/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/missouri-governor-revenge-porn-law/"},
					comments: "https://www.engadget.com/2018/06/03/missouri-governor-revenge-porn-law/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=2400%2C1497%2C0%2C26&quality=85&format=jpg&resize=1600%2C998&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F456438545aabffc5bea62efa9b28e095%2F206363399%2Fgov-eric-greitens-delivers-the-keynote-address-at-the-st-louis-area-picture-id953959958&client=a1acac3e1b3290917d92&signature=e7b9ee8b83315a2b57e9d7377e7f02e48ffde883" />Missouri governor Eric Greitens quit his position on Friday, but he approved 77 new laws before he officially left the building. One of those criminalizes "nonconsensual dissemination of private sexual images" (or threatening to do so)... which was t...`,
					categories: []RSSCategory{
						{Category: "gear"},
						{Category: "internet"},
						{Category: "politics"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 16, 58, 0, 0, NewYork)),
				}, {
					title:    "Xbox One may work with Alexa and Google Assistant",
					link:     "https://www.engadget.com/2018/06/03/xbox-one-may-work-with-alexa-and-google-assistant/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/xbox-one-may-work-with-alexa-and-google-assistant/"},
					comments: "https://www.engadget.com/2018/06/03/xbox-one-may-work-with-alexa-and-google-assistant/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/b0ce2382eb85523e9dde7db22a299913/206422682/xbox-one-x-review-gallery-9-1.jpg" />You can already use Cortana if you want to command an Xbox One with your voice, but that's not very practical now that Kinect is no longer an option for the console.  Microsoft may have a simple solution: take advantage of the smart speakers you alre...`,
					categories: []RSSCategory{
						{Category: "ai"},
						{Category: "alexa"},
						{Category: "amazon"},
						{Category: "av"},
						{Category: "console"},
						{Category: "games"},
						{Category: "gear"},
						{Category: "google"},
						{Category: "googleassistant"},
						{Category: "internet"},
						{Category: "microsoft"},
						{Category: "videogames"},
						{Category: "voiceassistant"},
						{Category: "xboxone"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 15, 31, 0, 0, NewYork)),
				}, {
					title:    "Turkey's president says Uber is 'over'",
					link:     "https://www.engadget.com/2018/06/03/turkey-president-says-uber-is-over/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/turkey-president-says-uber-is-over/"},
					comments: "https://www.engadget.com/2018/06/03/turkey-president-says-uber-is-over/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=3500%2C2333%2C0%2C0&quality=85&format=jpg&resize=1600%2C1067&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fd868b4a1b8fa63dbd63911522aaf096a%2F206421931%2FRTS1NYCL.jpeg&client=a1acac3e1b3290917d92&signature=29b39a3eabadb1bb99d96b7d8b2c7a79ee4cb1e1" />You might not want to count on hailing an Uber car in Istanbul any time soon.  Turkish President Recep Tayyip Erdogan has claimed at a Ramadan dinner that the ridesharing outfit's business is "over" in the country, with the interior ministry reported...`,
					categories: []RSSCategory{
						{Category: "erdogan"},
						{Category: "gear"},
						{Category: "internet"},
						{Category: "politics"},
						{Category: "ridesharing"},
						{Category: "services"},
						{Category: "transportation"},
						{Category: "turkey"},
						{Category: "uber"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 13, 59, 0, 0, NewYork)),
				}, {
					title:    "We're liveblogging Apple's WWDC 2018 event tomorrow at 10AM PT",
					link:     "https://www.engadget.com/2018/06/03/wwdc-2018-liveblog/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/wwdc-2018-liveblog/"},
					comments: "https://www.engadget.com/2018/06/03/wwdc-2018-liveblog/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/dcf1fb02df91f4bddf569e1daec710f7/206414707/WWDC2018.jpeg" />Oh, hey! Did you enjoy this short holiday work week? Having a relaxing weekend? Good -- because things are about to get busy. First thing tomorrow, Apple kicks off its annual developer conference, and Engadget will be doing what it always does: repor...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "gear"},
						{Category: "internet"},
						{Category: "mobile"},
						{Category: "wwdc"},
						{Category: "wwdc2018"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 12, 30, 0, 0, NewYork)),
				}, {
					title:    "After Math: You get what you pay for",
					link:     "https://www.engadget.com/2018/06/03/after-math-you-get-what-you-pay-for/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/after-math-you-get-what-you-pay-for/"},
					comments: "https://www.engadget.com/2018/06/03/after-math-you-get-what-you-pay-for/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=3000%2C2000%2C0%2C0&quality=85&format=jpg&resize=1600%2C1067&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fc61f3bfb9121d5aa37b543b3c945c882%2F206420957%2Ftraders-work-on-the-floor-of-the-new-york-stock-exchange-on-may-29-picture-id963613736&client=a1acac3e1b3290917d92&signature=6604b851b4ca0a952563b9a871a7f73ca39a2d1c" />It's been yet another sterling week for late-stage capitalism. Amazon is doing its best to ensure you never leave your house again, ASUS debuted a 20-core bitcoin miner, and Uganda is now charging its citizens to gossip online. But the kids are alrig...`,
					categories: []RSSCategory{
						{Category: "after math"},
						{Category: "business"},
						{Category: "column"},
						{Category: "tomorrow"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 11, 0, 0, 0, NewYork)),
				}, {
					title:    "The best extension cords",
					link:     "https://www.engadget.com/2018/06/03/the-best-extension-cords/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/the-best-extension-cords/"},
					comments: "https://www.engadget.com/2018/06/03/the-best-extension-cords/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/d8640c4da37c305c3f4766a9421dcee4/206420815/01-extension-cords-1600.jpg" />By Mark Smirniotis` + "\n\n" +
						`This post was done in partnership with Wirecutter. When readers choose to buy Wirecutter's independently chosen editorial picks, it may earn affiliate commissions that support its work. Read the full article here.`,
					categories: []RSSCategory{
						{Category: "extensioncord"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "gear"},
						{Category: "gewallhugger"},
						{Category: "home"},
						{Category: "partner"},
						{Category: "syndicated"},
						{Category: "thewirecutter"},
						{Category: "uswire"},
						{Category: "utilitechpro"},
						{Category: "voltecindustries"},
						{Category: "wirecutter"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 9, 30, 0, 0, NewYork)),
				}, {
					title:    "Ben Heck's Yobo NES portable",
					link:     "https://www.engadget.com/2018/06/03/ben-hecks-yobo-nes-portable/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/ben-hecks-yobo-nes-portable/"},
					comments: "https://www.engadget.com/2018/06/03/ben-hecks-yobo-nes-portable/#comments",
					desc: `<img src="https://img.vidible.tv/prod/2018-04/05/5ac62f1147fd6903309b9355/5ac62f207b7a991bf39f4a9e_o_A_v1.jpg" />` + "\n\n\n\n" +
						`Every console should be portable, even the replicated ones. This is why Ben is taking apart a "Yobo NES" and designing an enclosure to see how small he can make it and still be able to play Nintendo Entertainment System games. The fun starts with...`,
					categories: []RSSCategory{
						{Category: "benheck"},
						{Category: "benheckshow"},
						{Category: "diy"},
						{Category: "do it yourself"},
						{Category: "doityourself"},
						{Category: "element14"},
						{Category: "gaming"},
						{Category: "gear"},
						{Category: "nes"},
						{Category: "nintendo"},
						{Category: "thebenheckshow"},
						{Category: "video"},
						{Category: "yobo"},
						{Category: "yobones"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 8, 0, 0, 0, NewYork)),
				}, {
					title:    "Microsoft has discussed buying code giant GitHub (updated)",
					link:     "https://www.engadget.com/2018/06/03/microsoft-github-acquisition-talks/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/03/microsoft-github-acquisition-talks/"},
					comments: "https://www.engadget.com/2018/06/03/microsoft-github-acquisition-talks/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=3444%2C2294%2C437%2C3&quality=85&format=jpg&resize=1600%2C1066&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F7e3cd7f2f19c55f1948989c353128814%2F206421969%2Fbillboard-for-github-inc-is-displayed-on-the-facade-of-a-parking-in-picture-id459236528&client=a1acac3e1b3290917d92&signature=a424e85861fa87716b01706500f6c2f59d03c4f3" />GitHub is practically a household name among developers looking to store, share and discuss code, but it's not in a great position when it's floundering in a bid to replace its outgoing CEO. And it appears that Microsoft might seize this opportunity....`,
					categories: []RSSCategory{
						{Category: "acquisition"},
						{Category: "developer"},
						{Category: "gear"},
						{Category: "github"},
						{Category: "internet"},
						{Category: "microsoft"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "programming"},
						{Category: "software"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 4, 41, 0, 0, NewYork)),
				}, {
					title:    "Fiat Chrysler will launch over 30 EVs and hybrids by 2022",
					link:     "https://www.engadget.com/2018/06/02/fiat-chrysler-launching-over-30-electric-and-hybrid-cars-by-2022/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/fiat-chrysler-launching-over-30-electric-and-hybrid-cars-by-2022/"},
					comments: "https://www.engadget.com/2018/06/02/fiat-chrysler-launching-over-30-electric-and-hybrid-cars-by-2022/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=4713%2C3083%2C0%2C0&quality=85&format=jpg&resize=1600%2C1047&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fc7d56f24d08fd47367a8a4f17b380365%2F206421956%2Fthe-new-maserati-concept-car-alfieri-is-presented-at-the-2014-paris-picture-id456529640&client=a1acac3e1b3290917d92&signature=ce2ef89af1ac1fca82fb330ebae08733c44bade7" />Fiat Chrysler isn't exactly the first name that comes to mind when you think of electric cars. Where Ford, Volkswagen and other brands are basing their long-term roadmaps around EVs, Fiat Chrysler has been hesitant to make more than the occasional hy...`,
					categories: []RSSCategory{
						{Category: "alfaromeo"},
						{Category: "chrysler"},
						{Category: "electriccar"},
						{Category: "electricvehicle"},
						{Category: "ev"},
						{Category: "fiat"},
						{Category: "fiat500"},
						{Category: "fiatchrysler"},
						{Category: "gear"},
						{Category: "green"},
						{Category: "hybrid"},
						{Category: "jeep"},
						{Category: "maserati"},
						{Category: "phev"},
						{Category: "ram"},
						{Category: "transportation"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 23, 10, 0, 0, NewYork)),
				}, {
					title:    "A closer look at RED's audacious Hydrogen One phone",
					link:     "https://www.engadget.com/2018/06/02/red-hydrogen-one-hands-on-preview/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/red-hydrogen-one-hands-on-preview/"},
					comments: "https://www.engadget.com/2018/06/02/red-hydrogen-one-hands-on-preview/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/b4172d0df972cbe9cd222f72104bf50d/206422007/red-lede.jpg" />In a van sitting between the high school from Pretty Little Liars and the Stars Hollow gazebo from Gilmore Girls, RED founder Jim Jannard takes out his smartphone &mdash; the Hydrogen One &mdash; and starts whipping through demos with me. We're at AT...`,
					categories: []RSSCategory{
						{Category: "4v"},
						{Category: "camera"},
						{Category: "gear"},
						{Category: "hands-on"},
						{Category: "hydrogen"},
						{Category: "hydrogenone"},
						{Category: "mobile"},
						{Category: "red"},
						{Category: "smartphone"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 20, 50, 0, 0, NewYork)),
				}, {
					title:    "Google quietly killed its Android tablet web page (update: it's back)",
					link:     "https://www.engadget.com/2018/06/02/google-killed-android-tablet-section/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/google-killed-android-tablet-section/"},
					comments: "https://www.engadget.com/2018/06/02/google-killed-android-tablet-section/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/a3cebc43f16f25b3d6e5288804d333d/206421745/galaxy-tab-s-2.jpg" />Next time you visit Google's official Android website, you might notice a tiny but important change. The tablet section, which used to occupy the space between "wear" and "TV" in the menu, no longer exists (*it's back now -- please see the update bel...`,
					categories: []RSSCategory{
						{Category: "android"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "gear"},
						{Category: "google"},
						{Category: "tablet"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 20, 4, 0, 0, NewYork)),
				}, {
					title:    "Trump nominates Geoffrey Starks as FCC Commissioner",
					link:     "https://www.engadget.com/2018/06/02/trump-nominates-geoffrey-starks-as-fcc-commissioner/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/trump-nominates-geoffrey-starks-as-fcc-commissioner/"},
					comments: "https://www.engadget.com/2018/06/02/trump-nominates-geoffrey-starks-as-fcc-commissioner/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=4000%2C2666%2C0%2C0&quality=85&format=jpg&resize=1600%2C1066&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F7a27f64e94c54b9b4e95970014dd2095%2F206275640%2Fthe-federal-communications-commission-seal-hangs-inside-a-meeting-at-picture-id892269224&client=a1acac3e1b3290917d92&signature=5da7cc7112dfee13a2d8a237cccea2b54757840b" />It's now clear who will (likely) take FCC Commissioner Mignon Clyburn's seat now that she's stepping down.  President Trump has signaled his intent to nominate Geoffrey Starks to fulfill the remainder of a five-year Commissioner term that began on Ju...`,
					categories: []RSSCategory{
						{Category: "fcc"},
						{Category: "gear"},
						{Category: "geoffreystarks"},
						{Category: "internet"},
						{Category: "politics"},
						{Category: "presidenttrump"},
						{Category: "regulation"},
						{Category: "trump"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 18, 33, 0, 0, NewYork)),
				}, {
					title:    "Spotify hip-hop programming head leaves for YouTube",
					link:     "https://www.engadget.com/2018/06/02/spotify-hip-hop-programming-head-leaves-for-youtube/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/spotify-hip-hop-programming-head-leaves-for-youtube/"},
					comments: "https://www.engadget.com/2018/06/02/spotify-hip-hop-programming-head-leaves-for-youtube/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=3000%2C2000%2C0%2C0&quality=85&format=jpg&resize=1600%2C1067&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F3d97ff73ca2d08f87c12331064bfa57b%2F206421806%2Ftuma-basa-and-lyor-cohen-attend-the-big-smoke-pregrammy-celebration-picture-id910140906&client=a1acac3e1b3290917d92&signature=5c993ddc83f9d352a96901edafe96216a21f15e8" />YouTube's latest streaming music initiative may be off to a rough start, but it's determined to step up its game.  The service has confirmed months-long rumors that it hired Spotify's hip-hop programming head Tuma Basa (pictured at left) to become it...`,
					categories: []RSSCategory{
						{Category: "entertainment"},
						{Category: "gear"},
						{Category: "google"},
						{Category: "internet"},
						{Category: "music"},
						{Category: "services"},
						{Category: "spotify"},
						{Category: "streaming"},
						{Category: "tumabasa"},
						{Category: "youtubemusic"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 17, 1, 0, 0, NewYork)),
				}, {
					title:    "Denver Uber driver kills passenger in alleged self-defense incident",
					link:     "https://www.engadget.com/2018/06/02/uber-driver-in-denver-kills-passenger-after-alleged-attack/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/uber-driver-in-denver-kills-passenger-after-alleged-attack/"},
					comments: "https://www.engadget.com/2018/06/02/uber-driver-in-denver-kills-passenger-after-alleged-attack/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=2598%2C1731%2C0%2C0&quality=85&format=jpg&resize=1600%2C1066&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2Fc34e9916e9183c2f21c86c4beb0584f7%2F206421704%2Fdenver-crime-scene-investigator-on-scene-of-a-fatal-shooting-which-an-picture-id965283952&client=a1acac3e1b3290917d92&signature=987a29a3974f4c5f75f81ce22ab7fe82e1e914d9" />An Uber driver in Denver has been arrested for an investigation into first-degree murder charges after he killed his passenger in the early morning on June 1st.  Michael Hancock told a witness that he shot his customer, Hyun Kim, several times in sel...`,
					categories: []RSSCategory{
						{Category: "denver"},
						{Category: "gear"},
						{Category: "internet"},
						{Category: "murder"},
						{Category: "ridesharing"},
						{Category: "services"},
						{Category: "transportation"},
						{Category: "uber"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 15, 29, 0, 0, NewYork)),
				}, {
					title:    "macOS leak hints at dark mode and desktop News app",
					link:     "https://www.engadget.com/2018/06/02/macos-dark-mode-and-news-app-leak/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/macos-dark-mode-and-news-app-leak/"},
					comments: "https://www.engadget.com/2018/06/02/macos-dark-mode-and-news-app-leak/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/647f74874aea285e324590b74a7aaed3/206421788/macos-10-14-dark-mode-leak.jpg" />Apple has inadvertently spoiled some of WWDC 2018's mysteries in advance.  Well-known developer Steve Troughton-Smith has spotted a hidden video on the Mac App Store showing off what looks like a new version of macOS with a system-wide dark mode -- n...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "darkmode"},
						{Category: "gear"},
						{Category: "internet"},
						{Category: "mac"},
						{Category: "macos"},
						{Category: "news"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "software"},
						{Category: "wwdc2018"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 14, 2, 0, 0, NewYork)),
				}, {
					title:    "Recommended Reading: An NBA exec and some mysterious Twitter accounts",
					link:     "https://www.engadget.com/2018/06/02/recommended-reading-philadelphia-76er-bryan-colangelo/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/recommended-reading-philadelphia-76er-bryan-colangelo/"},
					comments: "https://www.engadget.com/2018/06/02/recommended-reading-philadelphia-76er-bryan-colangelo/#comments",
					desc: `<img src="https://o.aolcdn.com/images/dims?crop=3180%2C2120%2C0%2C0&quality=85&format=jpg&resize=1600%2C1067&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F5abc79f7f3f1715e2d74ca21079a3625%2F206421185%2Fmarkelle-fultz-of-the-philadelphia-76ers-passes-the-ball-against-of-picture-id946340122&client=a1acac3e1b3290917d92&signature=c8324c78dbe10a7c50d2791c2ccd4cb237a3ae41" />` + "\n" +
						`The curious case of Bryan Colangelo and the secret Twitter account` + "\n" +
						`Ben Detrick,` + "\n" +
						`The Ringer` + "\n\n\n" +
						`Even though he won't admit it, one of the NBA's biggest stars, Kevin Durant, almost certainly used a burner Twitter account to clap back at the haters. That...`,
					categories: []RSSCategory{
						{Category: "bryancolangelo"},
						{Category: "entertainment"},
						{Category: "facebook"},
						{Category: "gaming"},
						{Category: "gear"},
						{Category: "green"},
						{Category: "internet"},
						{Category: "marvel"},
						{Category: "philadelphia76ers"},
						{Category: "podcast"},
						{Category: "recommendedreading"},
						{Category: "twitter"},
						{Category: "wolverine"},
						{Category: "wolverinethelongnight"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 12, 30, 0, 0, NewYork)),
				}, {
					title:    "Apple approves first Telegram update since Russia ban",
					link:     "https://www.engadget.com/2018/06/02/apple-approves-first-telegram-update-since-russia-ban/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/apple-approves-first-telegram-update-since-russia-ban/"},
					comments: "https://www.engadget.com/2018/06/02/apple-approves-first-telegram-update-since-russia-ban/#comments",
					desc:     `<img src="https://o.aolcdn.com/images/dims?crop=2607%2C1615%2C0%2C0&quality=85&format=jpg&resize=1600%2C991&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F32c4399b3ecb71ef41527684bb243651%2F205997115%2Fwhatsapp-and-telegram-application-picture-id477051393&client=a1acac3e1b3290917d92&signature=053179e75acdcfcc0572d87b83d25db3fb90132a" />Apple has finally rolled out the latest version of Telegram on the App Store, a day after company chief Pavel Durov said that the tech giant has been blocking its updates since April. Telegram version 4.8.2 will make the app GDPR-compliant -- somethi...`,
					categories: []RSSCategory{
						{Category: "app"},
						{Category: "apple"},
						{Category: "appstore"},
						{Category: "gear"},
						{Category: "ios"},
						{Category: "mobile"},
						{Category: "russia"},
						{Category: "telegram"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 10, 58, 0, 0, NewYork)),
				}, {
					title:    "The Morning After: Weekend Edition",
					link:     "https://www.engadget.com/2018/06/02/the-morning-after-weekend-edition/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/the-morning-after-weekend-edition/"},
					comments: "https://www.engadget.com/2018/06/02/the-morning-after-weekend-edition/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/9207c332ea4e09b812c0de1452b2b3b4/206421728/street-sharks.gif" />Hey, good morning! You look fabulous.` + "\n\n" +
						`Welcome to the weekend. Along with our highlights from the past week, it's time to prepare for WWDC and Computex, plus we go for a ride in Chrysler's hybrid-powered Pacifica.`,
					categories: []RSSCategory{
						{Category: "entertainment"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "gaming"},
						{Category: "gear"},
						{Category: "themorningafter"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 10, 15, 0, 0, NewYork)),
				}, {
					title:    "PS4's 'Detroit' doesn't take place in the Motor City I know",
					link:     "https://www.engadget.com/2018/06/02/detroit-ps4-motor-city-realism/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/detroit-ps4-motor-city-realism/"},
					comments: "https://www.engadget.com/2018/06/02/detroit-ps4-motor-city-realism/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/dd18845d7d26c9bbc59e7d9326b3cbf3/206417908/Detroit_+Become+Human%3F_20180517214436.jpeg" />When Sony debuted Detroit: Become Human at E3 two years ago, writer-director David Cage said he and his team were taking great care to respect Motown's heritage, its people and what they've been through. "When you set your story in a specific city, i...`,
					categories: []RSSCategory{
						{Category: "av"},
						{Category: "davidcage"},
						{Category: "detroit"},
						{Category: "detroitbecomehuman"},
						{Category: "gaming"},
						{Category: "michigan"},
						{Category: "motorcitytech"},
						{Category: "opinion"},
						{Category: "playstation4"},
						{Category: "ps4"},
						{Category: "quanticdream"},
						{Category: "sharktoof"},
						{Category: "sony"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 9, 30, 0, 0, NewYork)),
				}, {
					title:    "'PES 2019' loses Borussia Dortmund ahead of launch",
					link:     "https://www.engadget.com/2018/06/02/pes-2019-keeps-taking-Ls/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/02/pes-2019-keeps-taking-Ls/"},
					comments: "https://www.engadget.com/2018/06/02/pes-2019-keeps-taking-Ls/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/a7f81c7330973c12c81912d50ec6b45c/206421117/PES_2019_Borussia_Dortmund.jpg" />Konami's Pro Evolution Soccer series can't catch a break. It's been second fiddle to EA's FIFA series for years, and with news that another team has ended its licensing agreement with the underdog football franchise likely won't help matters any. Bor...`,
					categories: []RSSCategory{
						{Category: "borussiadortmund"},
						{Category: "business"},
						{Category: "easports"},
						{Category: "fifa"},
						{Category: "gaming"},
						{Category: "itscalledfootballdamnit"},
						{Category: "licensing"},
						{Category: "proevolutionsoccer"},
						{Category: "proevolutionsoccer2019"},
						{Category: "soccer"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 4, 14, 0, 0, NewYork)),
				}, {
					title:    "2018 Bugatti Chiron Lego Technic kit is amazingly detailed",
					link:     "https://www.engadget.com/2018/06/01/bugatti-chiron-lego-technic-kit/",
					guid:     RSSGUID{GUID: "https://www.engadget.com/2018/06/01/bugatti-chiron-lego-technic-kit/"},
					comments: "https://www.engadget.com/2018/06/01/bugatti-chiron-lego-technic-kit/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/44ac8725117d9ecf80585f86aa26fff8/206420899/bugattichiron14-1.jpg" />By Joel Stocksdale` + "\n\n" +
						`Lego has finally followed up its amazing Porsche 911 GT3 Technic kit with a kit that's arguably more amazing, and certainly has more pieces. It's the 2018 Bugatti Chiron, and the 1/8 scale kit features 3,599 pieces, which is nearl...`,
					categories: []RSSCategory{
						{Category: "bugatti chiron"},
						{Category: "bugattichiron"},
						{Category: "gear"},
						{Category: "lego"},
						{Category: "lego technic"},
						{Category: "official"},
						{Category: "toys"},
						{Category: "transportation"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 23, 47, 0, 0, NewYork)),
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}

				if ch.Items[i].Author != "" {
					t.Errorf("must empty: %q", ch.Items[i].Author)
				}

				if ch.Items[i].Categories == nil {
					t.Errorf("must not nil: %q", ch.Items[i].Categories)
				} else {
					if target := items[i].categories; !ch.Items[i].Categories.Equal(target) {
						t.Errorf("must equal %q: %q", target, ch.Items[i].Categories)
					}
				}

				if target := items[i].comments; ch.Items[i].Comments != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Comments)
				}

				if ch.Items[i].Enclosure != nil {
					t.Errorf("must nil: %q", ch.Items[i].Enclosure)
				}

				if target := items[i].guid; !ch.Items[i].GUID.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].GUID)
				}

				if target := items[i].pubDate; !ch.Items[i].PubDate.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].PubDate)
				}

				if ch.Items[i].Source != nil {
					t.Errorf("must nil: %q", ch.Items[i].Source)
				}
			}
		}
	})

	source = "engadget_zh-hant.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "2.0"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Engadget 中文版 RSS Feed"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "https://chinese.engadget.com/rss.xml"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "來自 Engadget 中文版團隊的科技新聞和評測。掌握最新消費性電子產品" +
			"消息。- Engadget 中文版 RSS Feed"; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "zh-Hant"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if target := "2018 年 Oath Inc. 版權所有。此頻道的內容僅適用於非商業用途。"; ch.Copyright != target {
			t.Errorf("must equal %q: %q", target, ch.Copyright)
		}

		if ch.ManagingEditor != "" {
			t.Errorf("must empty: %q", ch.ManagingEditor)
		}

		if ch.WebMaster != "" {
			t.Errorf("must empty: %q", ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if ch.LastBuildDate != nil {
			t.Errorf("must nil: %q", ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if ch.Docs != "" {
			t.Errorf("must empty: %q", ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image == nil {
			t.Errorf("must not nil: %q", ch.Image)
		} else {
			img := RSSImage{
				URL:   "https://www.blogsmithmedia.com/chinese.engadget.com/media/feedlogo.gif?cachebust=true",
				Title: "Engadget 中文版 RSS Feed",
				Link:  "https://chinese.engadget.com/rss.xml",
			}

			if target := img; !ch.Image.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Image)
			}

			if target := img.URL; ch.Image.URL != target {
				t.Errorf("must equal %q: %q", target, ch.Image.URL)
			}

			if target := img.Title; ch.Image.Title != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Title)
			}

			if target := img.Link; ch.Image.Link != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Link)
			}

			if target := 0; ch.Image.Width != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Width)
			}

			if target := 0; ch.Image.Height != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Height)
			}

			if ch.Image.Description != "" {
				t.Errorf("must empty: %q", ch.Image.Description)
			}
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 25; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title      string
				link       string
				guid       RSSGUID
				comments   string
				desc       string
				categories []RSSCategory
				pubDate    RFC822
			}{
				{
					title:    "Apple Music 網頁播放器可以播放完整歌曲了",
					link:     "https://chinese.engadget.com/2018/06/03/apple-music-web-player-now-streams-whole-songs/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/03/apple-music-web-player-now-streams-whole-songs/"},
					comments: "https://chinese.engadget.com/2018/06/03/apple-music-web-player-now-streams-whole-songs/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/43724ddab4a9195d816ce12865cac076/206423435/dims.jpg" />Apple Music 作為一個相當成功的音樂串流服務，其實到今天都還是要依附在 iTunes 桌面應用之中，只有行動版本是個獨立的 app；而網頁版本的則只能作為預覽用途，只有 30 至 90 秒的試聽，不過趕在 WWDC 前夕，這網頁版工具似乎迎來了更新。有 Reddit 用戶發現 Apple Music 嵌入式網頁版播放器能夠讓有訂閱的用戶播放完整歌曲，甚至能直接在這網頁版工具裡把歌曲加入收藏，過程完全不需要離開瀏覽器。` + "\n\n" +
						`相比於 Spotify 提供了完整功能的網頁版播放器，Apple...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "applemusic"},
						{Category: "gear"},
						{Category: "internet"},
						{Category: "music"},
						{Category: "services"},
						{Category: "streaming"},
						{Category: "web"},
						{Category: "wwdc"},
						{Category: "wwdc2018"},
						{Category: "蘋果 apple"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 6, 0, 0, NewYork)),
				}, {
					title:    "華為 P20 Pro 評測：一道叫人重新注目的極光",
					link:     "https://chinese.engadget.com/2018/06/03/huawei-p20-pro-review/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/03/huawei-p20-pro-review/"},
					comments: "https://chinese.engadget.com/2018/06/03/huawei-p20-pro-review/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/d00452a35f9b6ff3535b48a8dfa9a42f/206416438/IMG_1746_web.jpg" />華為晉身國際手機名牌之列已經好一陣子，還拉攏傳統品牌 Leica 來加持相機的表現。今年華為更是超越手機同業的步伐，帶來首台搭載三相機的智慧型手機 P20 Pro。不過多一顆相機是否就代表更好呢？說是擁有 AI 助力的軟體系統又能否提供更好的使用體驗給用戶？最重要的，還是華為能否洗去前代 P10 的陰霾，透過這台 P20 Pro 重新贏得大眾的支持呢？小編就透過這篇評測來跟大家分享使用這一段時間下來後的感覺吧。`,
					categories: []RSSCategory{
						{Category: "feature"},
						{Category: "featured"},
						{Category: "features"},
						{Category: "huawei"},
						{Category: "kirin970"},
						{Category: "leica"},
						{Category: "notch"},
						{Category: "p20"},
						{Category: "p20pro"},
						{Category: "review"},
						{Category: "smartphones"},
						{Category: "手機"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 7, 0, 0, 0, NewYork)),
				}, {
					title:    "來自攝影機大廠 RED 的 Hydrogen One 手機預覽",
					link:     "https://chinese.engadget.com/2018/06/03/red-hydrogen-one-hands-on-preview/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/03/red-hydrogen-one-hands-on-preview/"},
					comments: "https://chinese.engadget.com/2018/06/03/red-hydrogen-one-hands-on-preview/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/8f3b68684fe9d18ce9a4e1951bde9604/206422213/red-lede.jpg" />去年專業級影片品牌 RED 透露了會自己開發一隻擁有「全息螢幕」和模組化配件的手機 Hydrogen One，但一直以來都沒有什麼太多有關這台手機的詳細資訊，直至後來他們開始安排給一些媒體試玩，這樣才能一窺它的端倪。今天我們主站終於也獲得機會能親手試玩這台 Hydrogen One 手機，而且更獲該公司的創辦人 Jim Jannard 來解釋這台一反傳統的智慧型手機，是如何反映他對於手機產品應有的形態。` + "\n\n" +
						`曇花一現於 LG G5 和其朋友的模組化手機概念，雖然 Motorola 算是有延續下去，...`,
					categories: []RSSCategory{
						{Category: "4v"},
						{Category: "camera"},
						{Category: "gear"},
						{Category: "hands-on"},
						{Category: "hydrogen"},
						{Category: "hydrogenone"},
						{Category: "mobile"},
						{Category: "red"},
						{Category: "smartphone"},
						{Category: "手機"},
					},
					pubDate: RFC822(time.Date(2018, 6, 3, 3, 45, 0, 0, NewYork)),
				}, {
					title:    "導盲犬和火烈鳥 emoji 有機會將在明年出現",
					link:     "https://chinese.engadget.com/2018/06/02/2019-emoji-candidates/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/02/2019-emoji-candidates/"},
					comments: "https://chinese.engadget.com/2018/06/02/2019-emoji-candidates/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/67c8f13843a3f792ff74c70db1c3646e/206421467/dims-3.jpg" />Unicode 發出了下一版本 Unicode 12 的預覽，給各位知道預計來年三明正式推出的 emoji 列表成員將會有什麼改動。有可能會加入的 emoji 包括潛水面鏡、斧頭、窩夫、印度教廟宇、白色的心形、火烈鳥等。為了更能涵蓋多元人士，更會有加入戴有助聽器的耳朵、電動輪椅、導盲犬等，可以代表身障人士的 emoji。`,
					categories: []RSSCategory{
						{Category: "disabilities"},
						{Category: "disability"},
						{Category: "emoji"},
						{Category: "gear"},
						{Category: "inclusion"},
						{Category: "mobile"},
						{Category: "unicode"},
						{Category: "unicode12"},
						{Category: "update"},
						{Category: "軟體應用"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 23, 0, 0, 0, NewYork)),
				}, {
					title:    "VLC 成為首款 ARM64 架構的 Windows 應用",
					link:     "https://chinese.engadget.com/2018/06/02/vlc-one-of-first-arm64-windows-apps/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/02/vlc-one-of-first-arm64-windows-apps/"},
					comments: "https://chinese.engadget.com/2018/06/02/vlc-one-of-first-arm64-windows-apps/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/ff39709777125a5081ab0d405ad74d47/206421455/dims-2.jpg" />採用 ARM 架構處理器的 PC 產品雖然已經推出，不過 ARM 版的 Windows 10 當時卻只有支援 32 位元的應用程式可用，微軟在最近完結的 BUILD 大會上才推出開發適用於這平台的 64 位元應用的 SDK。在一輪等待之後，終於首款以 ARM64 架構的 Windows 應用：VLC for Windows 正式登場了。` + "\n\n" +
						`如果你是入手了這些以連網和續航力為賣點的 ARM 架構 Windows PC 的話，現在已經可以去 VLC 的官網下載這應用，記緊要在下拉式選單中選擇 ARM...`,
					categories: []RSSCategory{
						{Category: "arm64"},
						{Category: "entertainment"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "vlc"},
						{Category: "window10"},
						{Category: "軟體應用"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 8, 0, 0, 0, NewYork)),
				}, {
					title:    "下一款借鏡 iPhone X 設計的可能就是 Motorola One Power",
					link:     "https://chinese.engadget.com/2018/06/02/iphone-x-motorola-one-power/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/02/iphone-x-motorola-one-power/"},
					comments: "https://chinese.engadget.com/2018/06/02/iphone-x-motorola-one-power/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/84d21ed20600b515c633233752902bc2/206421427/dims-1.jpg" />年初的時候就被曝光的 Motorola 2018 年產品目錄裡，出現了一款帶有瀏海設計的機款，可是上半年 Moto 所推出的幾款手機都似乎沒有這種設計。難道是放棄了？別傻了，Android Headlines 和 91mobiles 再找到了另一款 Motorola One Power，它不光用上已經見慣不怪的異形屏的，就連後面垂直擺放的雙相機佈局都一併「借鏡」過來。` + "\n\n" +
						`據報這款 Motorola One Power 會一如其名，屬於 Android One 的成員，將會有快速獲得系統更新的優勢...`,
					categories: []RSSCategory{
						{Category: "android"},
						{Category: "androidone"},
						{Category: "gear"},
						{Category: "leak"},
						{Category: "lenovo"},
						{Category: "mobile"},
						{Category: "motorola"},
						{Category: "motorolaonepower"},
						{Category: "onepower"},
						{Category: "smartphone"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 5, 0, 0, 0, NewYork)),
				}, {
					title:    "加州將會批准自駕車接載乘客",
					link:     "https://chinese.engadget.com/2018/06/02/california-allow-autonomous-cars-pick-up-passengers/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/02/california-allow-autonomous-cars-pick-up-passengers/"},
					comments: "https://chinese.engadget.com/2018/06/02/california-allow-autonomous-cars-pick-up-passengers/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/d129371a3545b8bb60288e422ed8d5b1/206421369/dims-1.jpg" />自駕車接載服務似乎快將成為現實了，因為 San Francisco Examiner 報導指加州政府正推出兩個批准自駕車接載乘客的先導計劃。其一是自駕車裡將會有人類司機坐在駕駛座裡的；另一個則是讓完全無人駕駛汽車去接載乘客，不過這種則需要有人類控制員遙距監控著。加州的公用事業專員 Liane Randolph 在新聞稿裡表示，他們很高興在加州推出進化的載客運輸系統的先導計劃，並期待著這些客運服務能提供著高度的安全性。` + "\n\n" +
						`四月的時候，加州政府開始接受公司申請在當地進行無司機的自駕車路試，包括 Wa...`,
					categories: []RSSCategory{
						{Category: "autonomousvehicles"},
						{Category: "california"},
						{Category: "driverlesscars"},
						{Category: "gear"},
						{Category: "selfdrivingcar"},
						{Category: "services"},
						{Category: "transportation"},
						{Category: "uber"},
						{Category: "waymo"},
						{Category: "交通運輸"},
					},
					pubDate: RFC822(time.Date(2018, 6, 2, 2, 0, 0, 0, NewYork)),
				}, {
					title:    "新一代 Samsung Galaxy Note 9 或許會在 8 月 9 日亮相",
					link:     "https://chinese.engadget.com/2018/06/01/samsung-reportedly-debut-next-galaxy-note-august-9th/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/samsung-reportedly-debut-next-galaxy-note-august-9th/"},
					comments: "https://chinese.engadget.com/2018/06/01/samsung-reportedly-debut-next-galaxy-note-august-9th/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/12240ccb2a350598d5cfb0e10ee6b744/206421349/dims.jpg" />Samsung 在年初推出的 Galaxy S9 系列獲得不少好評，尤其是所用的可變光圈主相機的相片品質，來到下半年也該是他們更新 Galaxy Note 產品線的時候。據 Bloomberg 獲得的消息指出，Samsung 將會在 8 月 9 日於紐約發表新一代的 Galaxy Note 手機，上面更會搭載著更強的相機。` + "\n\n" +
						`不過有期待新的 Galaxy Note 會新設計的朋友可能要失望了，因為據指 Note 9（雖然官方沒有確認，但要不然叫什麼名字？）的外觀並不會有大改變。可是升級後的 Bi...`,
					categories: []RSSCategory{
						{Category: "camera"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "galaxynote"},
						{Category: "galaxynote9"},
						{Category: "gear"},
						{Category: "mobile"},
						{Category: "samsung"},
						{Category: "samsunggalaxynote"},
						{Category: "smartphone"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 23, 0, 0, 0, NewYork)),
				}, {
					title:    "Apple 的自產晶片大計據稱已在「秘密」實驗室中展開了",
					link:     "https://chinese.engadget.com/2018/06/01/apple-custom-mac-processors-secret-lab/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/apple-custom-mac-processors-secret-lab/"},
					comments: "https://chinese.engadget.com/2018/06/01/apple-custom-mac-processors-secret-lab/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/13169e2287f0916dc3ee38ff6d4318a6/206420426/mac.jpg" />Apple 上月被爆出準備由 Intel 的處理器轉用自家開發的晶片，現在 The Oregonian 又為大家更新了一些與這項計畫有關的動態。據稱新晶片的籌備工作目前已在一個位於俄勒岡的「秘密」工程實驗室中展開，設施內已經安排了「二十多名」前 Intel 員工和俄勒岡本地的技術工人。` + "\n\n" +
						`據稱 Apple 在決定自己來做晶片以前，本來也考慮過用一顆「Power Nap」晶片搭配 Intel 處理器一起使用的方案。但最終他們還是決定自己著手，可能還是為了減少後期開支以及進一步對生產過程加以控制吧。...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "tomorrow"},
						{Category: "蘋果 apple"},
						{Category: "零組件相關"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 11, 38, 0, 0, NewYork)),
				}, {
					title:    "Telegram 創始人：Apple 在全球範圍內都禁止了軟體 iOS 版的更新",
					link:     "https://chinese.engadget.com/2018/06/01/apple-telegram-ios-app-russia-app-store/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/apple-telegram-ios-app-russia-app-store/"},
					comments: "https://chinese.engadget.com/2018/06/01/apple-telegram-ios-app-russia-app-store/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/bb56c9b30e79affb8ce5c53ab54d69fb/206419365/RTS19YM9.jpeg" />如果你覺得自己 iPhone 上的 Telegram 怎麼好久沒更新了有點奇怪的話，創始人、CEO Pavel Durov 現在已經給出了明確的解釋。按照他的說法，在俄羅斯政府上月向 Apple 提出將 Telegram 從 App Store 中移除的要求後，Apple 就一直在全球範圍內禁止開發團隊對 iOS 版本進行更新。也正是出於這一原因，他們的軟體甚至都沒法在全新的 iOS 11.4 裡完全正常使用，比如舊版本裡的貼紙功能在 iOS 的新韌體裡就是沒法運行的。` + "\n\n" +
						`不光如此，Apple...`,
					categories: []RSSCategory{
						{Category: "軟體應用"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 9, 0, 0, 0, NewYork)),
				}, {
					title:    "華碩最新的挖礦主機板可以一次控制 20 個 GPU",
					link:     "https://chinese.engadget.com/2018/06/01/asus-cryptocurrency-motherboard-h370-mining-master/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/asus-cryptocurrency-motherboard-h370-mining-master/"},
					comments: "https://chinese.engadget.com/2018/06/01/asus-cryptocurrency-motherboard-h370-mining-master/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/4411b191876b8829c3bcf7c077e85bac/206419071/h370.png" />華碩在去年的時候做了一張可以透過 PCIe 連接 19 張顯卡的挖礦主機板「B250 Mining Expert」來試水溫，想不到竟然大受好評，達到六位數的銷量遠超過預期，因此今年華碩又向這個坑裡更進了一步，推出名為「H370 Mining Master」的新主機板，除了簡化連接之外，也增添了新的監測選項功能來讓你更清楚每張顯卡的運作狀態。` + "\n\n" +
						`H370 Mining Master 的一切都針對挖礦來進行優化，不必要的東西全都移除來節省成本，只留下輔助顯卡運作所必需的功能。上一代配置了 18 個...`,
					categories: []RSSCategory{
						{Category: "asus"},
						{Category: "cryptocurrency"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "gear"},
						{Category: "h370miningmaster"},
						{Category: "motherboard"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "零組件相關"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 7, 30, 0, 0, NewYork)),
				}, {
					title:    "Withings 創辦人收回公司後，將於 2018 年年底回歸",
					link:     "https://chinese.engadget.com/2018/06/01/withings-brand-returning-nokia-health/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/withings-brand-returning-nokia-health/"},
					comments: "https://chinese.engadget.com/2018/06/01/withings-brand-returning-nokia-health/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/3576723f4eb12ee0a51e4cf1ff983ea4/206419008/dims.jpg" />兩年前 Nokia 收下 Withings 並把自己品牌掛在這些智慧型健康用品之上，試圖以消費級產品為他們的企業向醫療服務平台帶來協同效應。可是當 Nokia 重新審視過自己在健康用品的市場潛力後，今年年初就宣佈了會退出有關市場，現在更把公司正式交還給 Withings 的創辦人 Eric Carreel。一如之前的報導，Carreel 將會再次接管 Withings，並繼續以法國為總部，不過涉及的金額卻沒有透露。掛著 Nokia 品牌的現有產品將會繼續販售，同時分析健康數據的 Health M...`,
					categories: []RSSCategory{
						{Category: "connectedhealth"},
						{Category: "ericcareel"},
						{Category: "gear"},
						{Category: "medicine"},
						{Category: "nokia"},
						{Category: "nokiahealth"},
						{Category: "wearables"},
						{Category: "withings"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 6, 0, 0, 0, NewYork)),
				}, {
					title:    "Arm 帶來三款更省電的行動晶片設計",
					link:     "https://chinese.engadget.com/2018/06/01/arm-processor-design-more-efficient-mobile/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/arm-processor-design-more-efficient-mobile/"},
					comments: "https://chinese.engadget.com/2018/06/01/arm-processor-design-more-efficient-mobile/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/37d6aaf6792866c9fa2423fd189ae944/206419198/circuit-board-with-cpu-motherboard-system-chip-with-glowing-processor-picture-id870283878.jpg" />在早些時候於舊金山舉行的一場活動中，Arm 公開了三款為行動裝置而設的全新晶片設計。首先在 CPU 的部分，新發表的 Cortex-A76 是 Arm 高效能系列的最新產品。相較去年的 A75，它在效能上提升了 35%，用電效率也改進了 40%。至今為止已有不少 A7x 的晶片被用在 Windows 10 電腦之上，其中不少續航力都超過了 20 小時，A76 的出現可能又會把便攜機的功耗控制水平帶上一個新的台階。` + "\n\n" +
						`接著是 Mali-G 系列 GPU 的最新成員 G76，對比現有的 G72，它在...`,
					categories: []RSSCategory{
						{Category: "arm"},
						{Category: "cortex-a76"},
						{Category: "gear"},
						{Category: "mali-g76"},
						{Category: "mobile"},
						{Category: "personal computing"},
						{Category: "personalcomputing"},
						{Category: "trillium"},
						{Category: "零組件相關"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 5, 0, 0, 0, NewYork)),
				}, {
					title:    "Canon 正式終止底片相機的銷售",
					link:     "https://chinese.engadget.com/2018/06/01/canon-kills-eos-1v-its-last-film-camera/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/canon-kills-eos-1v-its-last-film-camera/"},
					comments: "https://chinese.engadget.com/2018/06/01/canon-kills-eos-1v-its-last-film-camera/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/71f8b73798e820519bc448ef9547fb0c/206419117/dims.jpg" />Canon 在八年前宣布停產底片相機以來，就只有賣餘下的庫存。今天他們更在日本網站上面發出告示，宣告最後的底片單眼相機 EOS-1V 已經正式停售，這也是為他們自 1934 年以來的底片相機業務寫下句號。Canon 表示會繼續提供相關相機的維修服務至 2025 年，當然零件就是用完就沒了。不過底片相機拍出來的特殊質感，的確是現今數位相機不能完全複製的，也難怪直至今天也有不少底片相機的支持者。只是舊世代產物的退場也是在所難免的，各位有興趣的朋友，還是可以去二手攤碰碰運氣。`,
					categories: []RSSCategory{
						{Category: "cameras"},
						{Category: "canon"},
						{Category: "canoneos1v"},
						{Category: "filmcamera"},
						{Category: "gear"},
						{Category: "nostalgia"},
						{Category: "slr"},
						{Category: "相機"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 4, 30, 0, 0, NewYork)),
				}, {
					title:    "時代雜誌的無人機專題用 958 台 Intel 無人機組成封面",
					link:     "https://chinese.engadget.com/2018/06/01/intel-drones-time-cover/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/intel-drones-time-cover/"},
					comments: "https://chinese.engadget.com/2018/06/01/intel-drones-time-cover/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/600cf6c1603a00c4d010b1f2acc0acc5/206418973/timedrones.jpg" />時代雜誌（Time Magazine）在最新一期中以「無人機時代」為題進行特別報導，配合這個題材，Time 找上了曾在今年奧運開幕式當中精彩演出的 Intel 團隊，用 958 台的無人機，在加州 Folsom 組成了時代雜誌著名的紅邊框與「TIME」字樣。` + "\n\n" +
						`由於無人機有高度限制（400 英呎），因此無人機群不得不組成一個稍為「傾斜」的圖案，以便將整個框塞進 100 公尺（328 英呎）高的範圍內，並留下一點安全空間。同時為了能更清楚看到「TIME」的字樣，平常無人機間應該要有 3 公尺的安全...`,
					categories: []RSSCategory{
						{Category: "cameras"},
						{Category: "design"},
						{Category: "drone"},
						{Category: "entertainment"},
						{Category: "gear"},
						{Category: "intel"},
						{Category: "intellightshow"},
						{Category: "robots"},
						{Category: "time"},
						{Category: "timemagazine"},
						{Category: "uav"},
						{Category: "video"},
						{Category: "無人機"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 3, 30, 0, 0, NewYork)),
				}, {
					title:    "Waymo 自駕車隊將會加入 62,000 輛 Chrysler 廂型車",
					link:     "https://chinese.engadget.com/2018/06/01/waymo-add-62000-chrysler-hybrid-minivans-to-fleet/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/waymo-add-62000-chrysler-hybrid-minivans-to-fleet/"},
					comments: "https://chinese.engadget.com/2018/06/01/waymo-add-62000-chrysler-hybrid-minivans-to-fleet/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/110546856d4ae1ef289d2dc69d303688/206418866/dims-2.jpg" />早前有報導指 Waymo 將向 FCA 購入逾千輛的 Chrysler 廂型車，預計在 2018 年年底付運。不過這數字原來只是其中的一角，據 CNBC 的發現，原來 Waymo 的訂單總共包括了 62,000 輛 Chrysler 廂型車。` + "\n\n" +
						`Waymo 將會把擁有的 600 輛已經改裝的 Chrysler 廂型車重新指派到鳳凰城用作共乘服務的用途，接下來就會在舊金山灣區開展這服務。同時 Waymo 和 FCA 的合作形式也會有所調整，當中包括會授權 Waymo 的自駕車技術，用以彌補 FCA...`,
					categories: []RSSCategory{
						{Category: "autonomous"},
						{Category: "autonomousvehicles"},
						{Category: "bayarea"},
						{Category: "chrysler"},
						{Category: "gear"},
						{Category: "google"},
						{Category: "minivan"},
						{Category: "selfdrivingcar"},
						{Category: "tomorrow"},
						{Category: "transportation"},
						{Category: "waymo"},
						{Category: "交通運輸"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 2, 0, 0, 0, NewYork)),
				}, {
					title:    "Softbank 向 GM 自駕車注資 22.5 億美元",
					link:     "https://chinese.engadget.com/2018/06/01/softbank-invests-in-gm-cruise-self-driving-unit/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/06/01/softbank-invests-in-gm-cruise-self-driving-unit/"},
					comments: "https://chinese.engadget.com/2018/06/01/softbank-invests-in-gm-cruise-self-driving-unit/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/573390a036c7dce670f4af4adae35197/206418806/dims-1.jpg" />Softbank 對於自駕車領域的投資毫不猶豫，他們今天再透過旗下的 Vision Fund 向 GM 的自駕車部門 Cruise 注資 22.5 億美元。首輪 9 億美元會在合約簽署後存入，其餘的 13.5 億美元則會在完全無人駕駛的 Cruise AV 汽車準備好推出後就會支付。這次的注資也讓 Vision Fund 在 Cruise 的擁有權增加至 19.6%。` + "\n\n" +
						`Vision Fund 早前已經向 Uber、滴滴和 Grab 等公司注資，所以他們會再向 GM 增加投資也不叫人意外。最重要...`,
					categories: []RSSCategory{
						{Category: "autonomous"},
						{Category: "car"},
						{Category: "cruise"},
						{Category: "gear"},
						{Category: "gm"},
						{Category: "investment"},
						{Category: "self-driving"},
						{Category: "self-drivingcar"},
						{Category: "softbank"},
						{Category: "transportation"},
						{Category: "vehicle"},
						{Category: "交通運輸"},
						{Category: "產業新聞"},
					},
					pubDate: RFC822(time.Date(2018, 6, 1, 0, 30, 0, 0, NewYork)),
				}, {
					title:    "數位健康將會是 Apple 今年 WWDC 的一大焦點",
					link:     "https://chinese.engadget.com/2018/05/31/apple-digital-health-focal-point-at-wwdc/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/apple-digital-health-focal-point-at-wwdc/"},
					comments: "https://chinese.engadget.com/2018/05/31/apple-digital-health-focal-point-at-wwdc/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/fbb3f3f6b3eee17c2787b3e38f74c967/206418767/dims.jpg" />即將於美國時間 6 月 4 日開始的 Apple 開發者大會 WWDC，大家都在期待他們會在台上發佈什麼新品，不過今年的重點似乎會放在數位健康之上。據 Blooomberg 的報導，例行的 MacBook 和 MacBook Pro 更新&minus;&minus;用上新世代 Intel 處理器&minus;&minus;以及新的更低價版本 MacBook Air，都會與 iPad Pro 的更新一起留在年底才會出現。不過據報這次的 WWDC 上會為軟體帶來大更新，包括是新的主頁面、Photos...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "applewatch"},
						{Category: "design"},
						{Category: "digitalhealth"},
						{Category: "gadgetry"},
						{Category: "gadgets"},
						{Category: "gear"},
						{Category: "ipad"},
						{Category: "ipadpro"},
						{Category: "iphone"},
						{Category: "macbook"},
						{Category: "macbookpro"},
						{Category: "mobile"},
						{Category: "wwdc2018"},
						{Category: "蘋果 apple"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 23, 5, 0, 0, NewYork)),
				}, {
					title:    "Oppo：Find X 讓大家久等了！",
					link:     "https://chinese.engadget.com/2018/05/31/oppo-find-x-confirmed/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/oppo-find-x-confirmed/"},
					comments: "https://chinese.engadget.com/2018/05/31/oppo-find-x-confirmed/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/4547a939d3617b07ef72d32a81baf908/206419108/oppo-lede.jpg" />雖然還沒確定發表時間，但這個消息估計已經能讓 Oppo 老粉絲完全興奮起來了。（話說這幾年培養起來的 Oppo 粉絲可能都完全不是一批人了吧...）`,
					categories: []RSSCategory{
						{Category: "find"},
						{Category: "findx"},
						{Category: "gadget"},
						{Category: "mobile"},
						{Category: "oppo"},
						{Category: "smartphone"},
						{Category: "手機"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 22, 3, 0, 0, NewYork)),
				}, {
					title:    "一加 6 的面部解鎖也被照片騙過了",
					link:     "https://chinese.engadget.com/2018/05/31/oneplus-6-face-unlock-fooled-by-printout/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/oneplus-6-face-unlock-fooled-by-printout/"},
					comments: "https://chinese.engadget.com/2018/05/31/oneplus-6-face-unlock-fooled-by-printout/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/4eb2b5ee8ae1d4dc0d3fc3be6777c91b/206417779/oneplus-6-review-1.jpg" />自從 iPhone X 的 Face ID 帶起一股面部識別的潮流之後，Android 各廠紛紛跟進，但大部分用的都是純依賴相機和軟體的方案而已。正因為此，手機被印出來照片騙過的情況並不少見，而現在一加 6 便是最新一款防線告破的產品。早些時候 Twitter 用戶 @rikvduijn 發現，只要用跟自己臉大小相近的照片，無論彩色還是黑白，都可以將手中的一加 6 順利解鎖。很顯然，即便是發展了大半年以後，Android 這邊的面部識別方案還是很容易出現漏洞啊。` + "\n\n" +
						`是說，在 Vivo X20 發...`,
					categories: []RSSCategory{
						{Category: "faceid"},
						{Category: "faceunlock"},
						{Category: "gear"},
						{Category: "mobile"},
						{Category: "oneplus"},
						{Category: "oneplus6"},
						{Category: "security"},
						{Category: "手機"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 21, 0, 0, 0, NewYork)),
				}, {
					title:    "Dyson Cyclone V10 評測：一顆射向老用戶的子彈",
					link:     "https://chinese.engadget.com/2018/05/31/dyson-cyclone-v10-review/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/dyson-cyclone-v10-review/"},
					comments: "https://chinese.engadget.com/2018/05/31/dyson-cyclone-v10-review/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/879caea799787f64e89a5d5c9792bdbb/206416914/dyson-v10-lede.jpg" />老實講，在 Cyclone V10 發表以前，筆者真的沒料到這幾年業績蒸蒸日上、一路順風順水的 Dyson，會下決心給自己的主力產品做那麼大的設計改變。而且不光如此，馬達技術上的演進，甚至強到了讓 James Dyson 爵士直接「停止開發」有線吸塵器的地步。在個人看來，要做出這樣兩個決定，勢必意味著 Dyson 對 V10 充滿信心。那在原本已經很厲害的 V8 之後，這款新品又能進化到什麼地步？接下來筆者就跟大家聊聊用過一段時間後的體會吧。`,
					categories: []RSSCategory{
						{Category: "cord-free"},
						{Category: "cordless"},
						{Category: "cyclonev10"},
						{Category: "dyson"},
						{Category: "feature"},
						{Category: "featured"},
						{Category: "features"},
						{Category: "hand-stick"},
						{Category: "review"},
						{Category: "v10"},
						{Category: "vacuum"},
						{Category: "家電"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 9, 30, 0, 0, NewYork)),
				}, {
					title:    "Google 正式把 AR 帶入 Expedition 教育平台",
					link:     "https://chinese.engadget.com/2018/05/31/google-brings-ar-tours-to-expeditions-app/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/google-brings-ar-tours-to-expeditions-app/"},
					comments: "https://chinese.engadget.com/2018/05/31/google-brings-ar-tours-to-expeditions-app/#comments",
					desc:     `<img src="https://s.aolcdn.com/hss/storage/midas/5f86d2267690b13ce73f3eebbd78ac23/206415956/dims.jpg" />Google 旗下的 Expedition 教育平台本來就已經可以透過 VR 方式，讓學生們能「走出教室」到不同的場景考察。今天這平台更如約加入了 AR 能力，簡單以手機就可以獲得超過 100 款不同的教材，包括達文西的發明、骨骼系統、藝術作品等。這 AR 功能之前已經向約 100 萬個學生試用，現在就正式推出予所有人使用。 Google 的新聞稿裡引用加州一名老師 Darin Nakakihara 的說法，他表示 AR 能把抽象的概念具現化給學生看到，因為他們之前並不可能在桌面上就看一顆心臟在...`,
					categories: []RSSCategory{
						{Category: "ar"},
						{Category: "av"},
						{Category: "expeditions"},
						{Category: "gear"},
						{Category: "google"},
						{Category: "googleexpeditions"},
						{Category: "mobile"},
						{Category: "vr"},
						{Category: "虛擬及擴增實境"},
						{Category: "軟體應用"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 9, 0, 0, 0, NewYork)),
				}, {
					title:    "美國將限制科技專業中國大陸留學生的簽證期限",
					link:     "https://chinese.engadget.com/2018/05/31/us-will-limit-visas-for-chinese-tech-students/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/us-will-limit-visas-for-chinese-tech-students/"},
					comments: "https://chinese.engadget.com/2018/05/31/us-will-limit-visas-for-chinese-tech-students/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/76e9360b55eaf2f466517b9263b44c96/206416676/senior-three-student-of-handan-no1-high-school-studies-in-classrooms-picture-id961935848.jpg" />沒想到中美兩國間貿易戰的影響，居然那麼快就波及到了美方針對中方留學生的簽證政策上。早些時候美國國務院官員向美聯社證實，接下來會將航空、高科技製造和機器人等專業中國留學生簽證的時效縮短到一年。不光如此，如果某個申請美國簽證的中國公民，其職業是美國商務部特別審查名單內組織的管理者或研究人員的話，相關申請也必須得到「多個」美國機構的許可，而這可能會導致長達數月的辦理時間。` + "\n\n" +
						`據稱新的政策將會從 6 月 11 日起開始執行，美方目前還沒有說明修改政策的真正原因，但從表面上看似乎是不願意幫中國培養人才吧....`,
					categories: []RSSCategory{
						{Category: "china"},
						{Category: "departmentofstate"},
						{Category: "donaldtrump"},
						{Category: "gear"},
						{Category: "politics"},
						{Category: "statedepartment"},
						{Category: "students"},
						{Category: "trump"},
						{Category: "visa"},
						{Category: "產業新聞"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 8, 1, 0, 0, NewYork)),
				}, {
					title:    "Apple Music 自設出版部門，狙擊 Spotify 的痛處",
					link:     "https://chinese.engadget.com/2018/05/31/apple-music-publishing-division/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/apple-music-publishing-division/"},
					comments: "https://chinese.engadget.com/2018/05/31/apple-music-publishing-division/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/ff8b7a530d374046d17e5a05c1dcd69a/206415822/dims-2.jpg" />Apple Music 的付費訂閱用戶超越了他們主要對手 Spotify 之後，也迎來了新的老大 Oliver Schusser。在上任後的首個大動作，就是把曾任 iTunes 法律主管的 Elena Segal 拉過來，並讓他掌管新成立的出版部門。據匿名線報向 Music Business Worldwide 的爆料，指 Oliver 希望強調出版和歌曲作者對於 Apple 的重要性。` + "\n\n" +
						`同樣是在音串流服務市場上的對手，Spotify 把重心放於科技之上，像是硬體產品和語音控制功能；Apple...`,
					categories: []RSSCategory{
						{Category: "apple"},
						{Category: "applemusic"},
						{Category: "business"},
						{Category: "gear"},
						{Category: "music"},
						{Category: "publishing"},
						{Category: "spotify"},
						{Category: "蘋果 apple"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 7, 0, 0, 0, NewYork)),
				}, {
					title:    "小米旗下尺寸最大的電視登場",
					link:     "https://chinese.engadget.com/2018/05/31/xiaomi-75-inch-mi-tv-4/",
					guid:     RSSGUID{GUID: "https://chinese.engadget.com/2018/05/31/xiaomi-75-inch-mi-tv-4/"},
					comments: "https://chinese.engadget.com/2018/05/31/xiaomi-75-inch-mi-tv-4/#comments",
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/f8c8d76850e4bf9dc8a62c1360a4750/206416497/40d0c0d43881284ca436fdebd2ca9c26.jpg" />雖然在發表會上幾乎是被簡單的三言兩語帶過，但小米今天新發的 75 吋版小米電視 4，對其電視產品線卻是有著頗為重要意義的。這是至今為止螢幕尺寸最大的小米電視，其寬度為 168cm，含底座的高度達到 104.3cm，最薄處的厚度只有 11.4mm。顯示面板的解析度為 4K，刷新度 60Hz。裝置搭載了四核心 1.8GHz 的 Amlogic Cortex-A53 晶片，同時有 Mali-T830 GPU 並支援 HDR。` + "\n\n" +
						`除此之外，這款產品還配備了 2GB RAM 和 32GB 儲存，支援雙頻...`,
					categories: []RSSCategory{
						{Category: "hd"},
						{Category: "mitv4"},
						{Category: "smarttv"},
						{Category: "tv"},
						{Category: "xiaomi"},
						{Category: "電視"},
					},
					pubDate: RFC822(time.Date(2018, 5, 31, 6, 3, 0, 0, NewYork)),
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}

				if ch.Items[i].Author != "" {
					t.Errorf("must empty: %q", ch.Items[i].Author)
				}

				if ch.Items[i].Categories == nil {
					t.Errorf("must not nil: %q", ch.Items[i].Categories)
				} else {
					if target := items[i].categories; !ch.Items[i].Categories.Equal(target) {
						t.Errorf("must equal %q: %q", target, ch.Items[i].Categories)
					}
				}

				if target := items[i].comments; ch.Items[i].Comments != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Comments)
				}

				if ch.Items[i].Enclosure != nil {
					t.Errorf("must nil: %q", ch.Items[i].Enclosure)
				}

				if target := items[i].guid; !ch.Items[i].GUID.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].GUID)
				}

				if target := items[i].pubDate; !ch.Items[i].PubDate.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].PubDate)
				}

				if ch.Items[i].Source != nil {
					t.Errorf("must nil: %q", ch.Items[i].Source)
				}
			}
		}
	})

	source = "engadget_ja-jp.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "2.0"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Engadget Japanese RSS Feed"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "https://japanese.engadget.com/rss.xml"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "Engadget Japanese"; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "ja-jp"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if target := "Copyright 2018 Weblogs, Inc. The contents of this feed" +
			" are available for non-commercial use only."; ch.Copyright != target {
			t.Errorf("must equal %q: %q", target, ch.Copyright)
		}

		if ch.ManagingEditor != "" {
			t.Errorf("must empty: %q", ch.ManagingEditor)
		}

		if ch.WebMaster != "" {
			t.Errorf("must empty: %q", ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if ch.LastBuildDate != nil {
			t.Errorf("must nil: %q", ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if target := "Blogsmith https://www.blogsmith.com/"; ch.Generator != target {
			t.Errorf("must equal %q: %q", target, ch.Generator)
		}

		if ch.Docs != "" {
			t.Errorf("must empty: %q", ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image == nil {
			t.Errorf("must not nil: %q", ch.Image)
		} else {
			img := RSSImage{
				URL:   "https://s.blogsmithmedia.com/japanese.engadget.com/assets-h9ef1ea9758788a6fe78f4133621a6ce7/images/eng-logo-232x51.png?h=ac620b78857ef8a24572e7ebae197d8a",
				Title: "Engadget Japanese RSS Feed",
				Link:  "https://japanese.engadget.com/rss.xml",
			}

			if target := img; !ch.Image.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Image)
			}

			if target := img.URL; ch.Image.URL != target {
				t.Errorf("must equal %q: %q", target, ch.Image.URL)
			}

			if target := img.Title; ch.Image.Title != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Title)
			}

			if target := img.Link; ch.Image.Link != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Link)
			}

			if target := 0; ch.Image.Width != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Width)
			}

			if target := 0; ch.Image.Height != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Height)
			}

			if ch.Image.Description != "" {
				t.Errorf("must empty: %q", ch.Image.Description)
			}
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 25; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title   string
				link    string
				guid    RSSGUID
				desc    string
				pubDate RFC822
			}{
				{
					title: "Xperiaでホーム画面を自分好みにカスタマイズする方法：Xperia Tips",
					link:  "https://japanese.engadget.com/2018/06/04/xperia-xperia-tips/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/04/xperia-xperia-tips/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/f3bb8ee186e1bae6ff01099ba93401af/206381669/1+-+%3F%3F%3F.png" />` + "\n" +
						`Xperiaでホーム画面の整理をするとき、アプリをカテゴリ別にフォルダ分けしたり、不要なアプリを削除したりするのはよくあること。でも、ホーム画面でできることはそれだけではありません。` + "\n\n" +
						`Xperiaなら、ホーム画面をめくるときのエフェクトを自分好みに変えたり、アプリを配置する画面比率を変える「グリッド」なども設定できるんです。` + "\n\n" +
						`ここでは、標準のホーム画面では物足りない、もっと細部までこだわりたいという人におすすめの機能を紹介します。` + "\n" +
						`<p><a href="https://japanese.engadget.com/gallery/-531/" title="ホーム画面をカスタマイズする方法">【ギャラリー】ホーム画面をカスタマイズする方法 (10枚)</a></p>`,
					pubDate: RFC822(time.Date(2018, 6, 4, 0, 5, 0, 0, NewYork)),
				}, {
					title: "アップルが6コアMacBook Proを準備中？　今夜開催のWWDCで発表か",
					link:  "https://japanese.engadget.com/2018/06/03/6-macbook-pro-wwdc/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/6-macbook-pro-wwdc/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/28a06f183521ebeba5792a2cea8fed1d/206423474/MBP.jpg" />` + "\n" +
						`日本時間6月5日2時に開催されるAppleの開発者会議WWDC 2018。昨年はiMacやMacBook ProなどのアップデートやiMac Proの発表など、ハードウェア製品の発表が多くありましたが、今年はハードウェアに関する発表はほとんどなく、ソフトウェアが中心になると予想されています。` + "\n\n \n" +
						`WWDC 2018、話題はiOS 12のDigital HealthとARKit 2.0か。ハードウェア製品発表はほぼなし...？` + "\n\n" +
						`そんな中、6コア仕様のMacBook ProがベンチマークサイトGeekBenchで見つかりました。` + "\n\n" +
						`見つかったのは「MacBookPro14,3」。これは2017年の15インチ Macbook Proの機種IDにあたります。GeekBenchで確認できるCPUはIntelの最新プロセッサー、Coffee LakeシリーズのCore i7-8750H。` + "\n\n" +
						`現行MackBook ProのCPUは上位モデルでもKaby LakeのCore i7-7920HQ（4コア）。これに対し、Core i7-8750Hは6コア、12スレッドでキャッシュは9MB。各コアのベースクロックは2.20GHz、ターボブーストの使用で4.1GHzとなります。さらにベンチマーク上では32GBのメモリを搭載しているのが確認できます。` + "\n\n" +
						`GeekBenchのスコアはシングルコアで4920、マルチコアで22316。現行MacBook Pro（15-inch Mid 2017）のマルチコアスコアが16999なので、大幅にスコアアップしているのがわかります。` + "\n\n" +
						`もっとも、ベンチマークサイトの表示は簡単に詐称できるため、見つかった情報が正しいかどうかはわかりません。ただ、冒頭で書いた通り、昨年のWWDC 2017では現行のMacBook Proが発表されおり、2018年モデルがWWDC 2018で発表されてもおかしくはありません。` + "\n\n" +
						`このほか、WWDC 2018では、iPhone SE2が発表されるとの噂があるほか、以前には13インチのMackBookが発表されるのではとの話もでていました。こうしてみると、ソフトウェア中心と言われながらも、意外とハードウェアの発表もあるのかもしれません。` + "\n\n\n" +
						`関連：` + "\n" +
						`iPhone SE2（仮称）は6月15日発売？WWDC直前に様々なうわさ` + "\n" +
						`13インチRetina版MacBookが今年中に登場？価格はMacBook Airと同程度か、少し高いぐらいとのうわさ` + "\n" +
						`アップルのWWDC 2018に期待するもの（本田雅一）`,
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 55, 0, 0, NewYork)),
				}, {
					title: "スマートネックレス「Medallion」をノキアに再投入してほしい：山根博士のスマホよもやま話",
					link:  "https://japanese.engadget.com/2018/06/03/medallion/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/medallion/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/3989cd87f3c3dbb33d2ec0544b799ede/206421849/EG180601_01.jpg" />` + "\n" +
						`ノキアはWithingsから買収したウェアラブルデバイスを含むスマートヘルスケアビジネスから撤退すると発表しました。同事業はWithingsの共同設立者、Eric Carreel氏に売却されます。「NOKIA」ロゴのついたスマートウォッチ「Steel」シリーズは、2016年の買収からわずか2年で市場から消え、再びWithingsブランドで販売されることになります。` + "\n\n" +
						`スマートフォンで一度ならずとも二度の失敗を繰り返し、HMDグローバルによる新生・ノキアブランドとしてコンシューマー市場での復活の道を歩み始めたノキア。しかしウェアラブルデバイス市場はそう簡単には攻めきれなかったようです。` + "\n\n\n\n" +
						`ところがノキアのウェアラブルデバイスは、このSteelシリーズが最初の製品ではありません。実はスマートフォン黎明期に一度は製品を展開し、日本でも発売されたことがあったのです。しかし数年で撤退したという歴史がありました。それは今から10年以上も前のこと。` + "\n\n" +
						`ノキアが本格的なウェアラブルデバイスとして市場に投入したのは「Medallion」（メダリオン）シリーズ。2004年冬に初代モデル「Medallion I」が発売されました。発売はなんと世界に先駆け日本が最初。iPhoneも登場していなかったそのころ、日本の携帯電話業界は世界で最も進んでいました。ノキアは日本市場ならこの先進的なデバイスが売れるだろうとにらんだのです。その後「Medallion II」も日本で発売される予定でした。` + "\n\n\n\n" +
						`Medallion Iの形状はネックレスの形をしています。今風に言えばスマートネックレスと呼ばれるのでしょう。首からぶら下げるストラップは、軟質樹脂のものと金属製のチェーンが付属しファッションに合わせて交換できました。日本ではセレクトショップで販売されるなど、IT製品としてではなくファッション製品としても販売されたほどです。` + "\n\n" +
						`ネックレスの本体部分にはカラーディスプレイが搭載されています。サイズは1辺が16.7ミリなので、画面サイズ表示では約0.7インチとなります。解像度は96x96ピクセルで表示数は4096色。ドットが荒く色数も少ないものの、表示サイズが小さいため表示はそれなりに見ることができました。` + "\n\n\n\n" +
						`ストレージは内蔵メモリーのみで、容量は写真が8枚保存可能。つまり100MB以下でしょう。赤外線を搭載し、ほかの携帯電話から赤外線で写真を送り、それをMedallion Iの画面で表示することができたのです。普段は時計表示にもなり、文字盤がいくつかプリインストールされていました。つまりフォトビュワーとして使うファッションアイテムだったのです。` + "\n\n" +
						`当時のスマートフォンのOS能力やBluetoothの性能は今と比べるとまだまだ低く、スマートフォンの通知を受けるデバイスを製品化することはできませんでした。ソニー・エリクソンがBluetoothを搭載し、スマートフォンからの通知を受けられる初代スマートウォッチ「MBW-100」を製品化したのはMedallionから2年後の2006年でした。` + "\n\n\n\n" +
						`ノキアとしてはカメラ機能が年々高まる携帯電話とペアで使えるウェアラブルデバイスを出すことで、新たなビジネスを開拓しようと考えたのでしょう。「Medallion II」は付属の革のストラップや自分の好きなストラップで首からぶら下げることができました。また「Kaleidoscope I」はその名前の通り、万華鏡のようにのぞき込んで使うデバイスでした。ノキアは一気に3製品もラインナップを登場させ、この分野でマーケットの先駆者を狙ったのでした。` + "\n\n\n\n" +
						`では売れ行きはどうだったのでしょうか？そのころのノキアの携帯電話のカメラ画素数は30万画素。わざわざMedallionを買っても、携帯電話で写した写真を転送して表示して楽しもうとは思える画質ではありませんでした。日本でも同時に発売になった木の葉型のフィーチャーフォン「Nokia 7600」も、パンフォーカス30万画素カメラでした。` + "\n\n\n\n" +
						`一方、Medallion Iは日本の携帯電話にも対応していました。たとえば対応機種のドコモ「SO505i」は130万画素のカメラを搭載。高画質な写真を撮影できる日本の携帯電話のユーザーなら、Medallion Iを活用してくれるとノキアは考えたのでしょう。しかし目の肥えた日本のユーザーにはMedallion Iの画面は貧相に見えたでしょうし、転送できるファイルサイズ制限や8枚だけという容量制限は不自由でした。ノキアのブランドも日本では確立しておらず、Medallion Iを首からぶら下げることがカッコいい、というムーブメントは起きなかったのです。` + "\n\n" +
						`ということでノキアのビューアータイプのウェアラブルデバイスはこの3機種であっけなく終了してしまいました。ノキアは他にも据え置き型の画像ビューアーなどを複数展開し、デジタルイメージング事業にも力を入れましたが、そもそもペアとなる自社のスマートフォンやフィーチャーフォンのカメラ画質が低くては話にならなかったのでしょう。` + "\n\n\n\n" +
						`その後2006年にはBluetoothヘッドセットでネックレス型、128x128ピクセルのディスプレイを搭載した「HS-13W Wireless Image Headset」を発売しましたが、こちらはあまり宣伝されることもなく、市場でも大きな話題になることはありませんでした。そしてそれ以降、ノキアの端末事業は2007年に登場したiPhone、そしてグーグルのAndroidの出現により苦境を迎えていきます。ウェアラブルに再参入する余裕すらなくなっていったのです。` + "\n\n\n\n" +
						`しかしMedallion Iのデザイン・形状を改めて見てみると、2018年でも十分通用すると思えないでしょうか。ディスプレイ部分は今ならスマートウォッチの本体をそのまま取り付ければよいでしょう。実際に、スマートウォッチの中には本体部分を取り外して首からぶら下げて使える製品もあるくらいです。` + "\n\n" +
						`HMDグローバルは5月にもノキアのスマートフォン3機種を発表するなど、ノキアブランドの復活を着々と推し進めています。Wear OSを搭載したノキアブランドのスマートウォッチが発売される日もいつかくるのかもしれません。その時はぜひMedallionのような、他社にはないデザインのウェアラブルデバイスも投入してほしいものです。`,
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 45, 0, 0, NewYork)),
				}, {
					title: "ノキア、廉価端末のNokia 5.1/3.1/2.1発表。Android OneモデルやOreo Go搭載",
					link:  "https://japanese.engadget.com/2018/06/03/nokia-5-1-3-1-2-1-android-one-oreo-go/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/nokia-5-1-3-1-2-1-android-one-oreo-go/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/1aa327bb15570337adba084ae7c8464/206422932/20180604nnokia1.jpg" />` + "\n\n" +
						`ノキアブランドの端末を開発・製造するHMD Globalは、新型スマートフォン「Nokia 5.1」「Nokia 3.1」「Nokia 2.1」を発表しました。これらは、エントリーレベルからミッドレンジをカバーする端末となります。` + "\n\n" +
						`まず上画像は、2年間のソフトウェア・アップデートと3年間のセキュリティ・アップデートが保証されている「Android One」ブランドの端末となるNokia 5.1です。5.5インチのIPS方式の液晶ディスプレイはフルHD+解像度（1080×2160ドット）/縦横比18:9で、プロセッサはMediaTekの「Helio P18」、RAMは2GB/3GBで内蔵ストレージは16GB/32GB、背面カメラは1600万画素で前面カメラは800万画素。ボディーは酸化アルミ製で質感にもこだわっています。` + "\n\n" +
						`Nokia 5.1のカラーバリエーションはカッパー、テンパード・ブルー、ブラックの3色で、RAMやストレージ容量違いで189ユーロ（約2万4000円）より2018年7月から販売されます。` + "\n\n\n\n" +
						`Nokia 3.1も「Android One」ブランドの端末です。5.2インチのIPS方式液晶ディスプレイは720p+解像度（720×1440ドット）/縦横比18:9で、プロセッサはMediaTekの「MT6750」、RAMは2GB/3GBで内蔵ストレージは16GB/32GB、背面カメラは1300万画素で前面カメラは800万画素。ボディは酸化アルミ製フレームにポリカーボネート製のバックパネルを組み合わせています。` + "\n\n" +
						`Nokia 3.1のカラーバリエーションはブルー/カッパー、ブラック/クローム、ホワイト/アイロンの3色で、RAMやストレージ容量違いで139ユーロ（約1万8000円）より6月から販売されます。` + "\n\n\n\n" +
						`Nokia 2.1は、OSに軽量化カスタムを施した「Android Oreo Go Edition」を採用した端末です。5.5インチのIPS方式液晶ディスプレイはHD解像度（720×1280ドット）/縦横比16:9で、プロセッサはクアルコムのSnapdragon 425、RAMは1GBで内蔵ストレージは8GB、背面カメラは800万画素で前面カメラは500万画素。フロントスピーカーの搭載がこの端末の特徴でしょうか。` + "\n\n" +
						`Nokia 3.1のカラーバリエーションはブルー/カッパー、ブルー/シルバー、グレー/シルバーの3色で、99ユーロ（約1万3000円）にて7月から販売されます。`,
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 30, 0, 0, NewYork)),
				}, {
					title: "ドコモの新アシスタント「my daiz」担当者インタビュー。サービス連携の狙いや“豆腐”キャラに込められた思い",
					link:  "https://japanese.engadget.com/2018/06/03/my-daiz/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/my-daiz/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/9bcdfff32bf5394655e394db7d591e79/206422003/22.jpg" />` + "\n\n" +
						`NTTドコモが5月31日提供を開始した新しいAIアシスタント「my daiz」（マイデイズ）。ユーザーの問いかけに答えていく方式のアシスタントアプリで、ユーザーの行動に先回りして情報を表示する「先読み機能」を備えているほか、宅配便の問い合わせやギフト購入など、ドコモ以外のパートナーが提供するサービスとも連携します。` + "\n\n" +
						`今回は、NTTドコモでmy daizのサービス開発に携わっているコンシューマービジネス推進部の関﨑宜史氏（エージェントサービス担当部長）、大場さおり氏（エージェントサービス 第一エージェントサービス担当）、近藤佳代子氏（エージェントサービス 第一エージェントサービス担当課長）にサービスのコンセプトや特徴についてお話をうかがいました。巷では「豆腐」などと呼ばれている謎のキャラクターについても聞いています。` + "\n" +
						`【関連記事】` + "\n" +
						`動画：アナタを一番よく知るスマホ、AIエージェントに。ドコモ、my daiz（マイデイズ）開始` + "\n" +
						`ドコモスマホの「ひつじのしつじ」引退へ、新キャラ「マイデイズ」に──新AI機能発表`,
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 0, 0, 0, NewYork)),
				}, {
					title: "台北で開催中の無駄づくりアーティスト藤原麻里菜さんの個展をチェックしてきた：旅人目線のデジタルレポ 中山智",
					link:  "https://japanese.engadget.com/2018/06/03/muda/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/muda/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/2833fdba4e8224ae572dbd75572d7fe3/206423545/33090335_10209715764610327_4201970224565583872_n.gif" />` + "\n" +
						`旅人ITライターの中山です。現在6月5日から開催されるCOMPUTEX TAIPEI 2018の取材のため台北に来ています。COMPUTEX以外にもなにかおもしろいイベントがないかとチェツクしていたら、無駄づくりYouTuberとして最近注目を集めている藤原麻里菜さんが、台北で個展を開くという情報をキャッチ。早速現地で取材をしてきました。`,
					pubDate: RFC822(time.Date(2018, 6, 3, 22, 30, 0, 0, NewYork)),
				}, {
					title: "アップルのWWDC 2018に期待するもの（本田雅一）",
					link:  "https://japanese.engadget.com/2018/06/03/wwdc-2018/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/wwdc-2018/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/3caa8b74c8f2bd9dc7c4a0c2190ad139/206423446/wwdc.jpg" />` + "\n" +
						`人生でブログというものを初めて......ではないけれど、お仕事の契約書に"ブロガー"と書かれていて、少し気楽なような、気楽なんて言ったらプロのブロガーさんに失礼なような、じゃぁお前は何者だ、なんてリカーリング思考に入っている本田です。` + "\n\n" +
						`さて今（記事執筆時点）まさにアップルのソフトウェア開発者向け会議「WWDC 2018」に向け、空港に向けて出かける準備をしているところ。僕には僕の仕事があっての取材旅行ですが、本誌編集長も同じ日程のようですから、きっとACCNが見事な切れ味の最速レポートを掲載することでしょう。ええ最速です！（他人事ですから）` + "\n\n" +
						`（編集部注：WWDC 2018は日本時間の6月5日午前2時に開幕予定）` + "\n\n" +
						`●アップルが新ハードウェアを発表する場合の「2つの理由」` + "\n\n" +
						`WWDCはソフトウェア開発者向けの会議ですから、ハードウェアの発表はない場合もありました。しかし、大まかに言うと2つの理由から新製品がWWDCの基調講演で発表されることもあります（アップルが"2つの理由"と言っているのではなく、毎年取材している経験則からのものです）。` + "\n\n" +
						`1つはWWDCで扱うOS（基本ソフト）......すなわち、iOS、macOS、watchOS、tvOSの4つ......の改修と、それに伴ってアプリケーションソフト側にも手を入れる必要がある場合。たとえばMacbook ProがはじめてRetina対応した時には、WWDCで発表が行われましたし、昨年はiOSのiPad向け機能が大きく拡充される中で、iPad Proの10.5インチ版が発表されました。` + "\n\n" +
						`WWDCは開発者向けなので、ほぼ同時期にMacのアップデートが行われる場合もありますが、とりわけWWDCの中身との関連が重要ではない場合は、発表しないままWWDCが終了し、サラッと新モデルに入れ替えるといったことも過去にはありました。` + "\n\n" +
						`今回はMacbook Airの後継、あるいはMacbookの大型化などの噂がありますが、もし発表されるとするなら、macOSのアップデートと何らかの関係があるかもしれません。そういえば現行Macbook Proのタッチバー内蔵モデルが出たのもWWDCでしたね。対応ソフトの開発を促す上では重要なことだったと考えられます。` + "\n\n" +
						`もう1つはMac Pro、iMac Proといった、プロフェッショナルにこそ使ってほしいという製品を世の中に投入する場合。WWDCに集まるエンジニアたちは、まさにトッププライオリティで紹介したい相手なのですから、ここで発表するのは自然なことです。` + "\n\n" +
						`●新iPhoneは望み薄、iOSは「常駐型ソフトウェア」への制約緩和に期待` + "\n\n" +
						`しかし当然ながら、年末に決まってアップデートするiPhoneシリーズは、特殊なメインストリームから枝分かれしたモデルを投入する場合を除き、登場することはまずありません。iOSの新機能を紹介する際にも、新型iPhoneの機能やデザインの方向性を感じさせるアップデートは紹介を避けて次世代iOSの内容が伝えられ、それ以外の多くは守秘義務を伴う形で開発者に紹介。` + "\n\n" +
						`あるいは、本当に秘密にしたい部分は秋の製品ローンチ時まで一部のパートナーとのみ協業したり、アップル自身の開発したアプリで新しさを訴求するといったことをしますから、iOSに関しては次に向けての興味を引きつつも、ハードウェアが絡む要素については"チラ見せ"ぐらいに考えるほうがいいでしょう。` + "\n\n" +
						`いずれにしろ、間もなく基調講演で今年後半に向けてのOSアップデートについて（すべてではないものの）語られることになるのですが、個人的には"必ずここに手を入れてほしい"と思っている部分がiOSにはあります。` + "\n\n" +
						`それは常駐型ソフトウェアを伴うアプリの受け入れ幅を拡大するような何らかの工夫です。` + "\n\n" +
						`iOSはメモリ内に起動中アプリが残っている場合は、何らかのシステム動作をトリガーに処理を走らせることができますが、メモリーから消えると外部から、あるいは何らかの動作をトリガーにしてアプリ機能を呼び出す手段には一定の制約が出てきます。` + "\n\n" +
						`なぜなら常駐動作するアプリの種類に制約があるからです。` + "\n\n" +
						`この制約は、かつてはiOSデバイスの拡張性に制約を与えてきましたが、現在はセキュリティとバッテリー消費の両方の問題から下記の用途にのみ、長時間バックグラウンドで待機することが許されています。` + "\n\n" +
						`音楽プレーヤーのように、バックグラウンドで音声を再生するアプリケーション` + "\n" +
						`バックグラウンドで音声を録音するアプリケーション` + "\n" +
						`ナビゲーションのように、常に位置情報を知らせるアプリケーション` + "\n" +
						`VoIP（Voice over Internet Protocol）対応アプリケーション` + "\n" +
						`定期的に最新号をダウンロードして処理する必要があるアプリケーション` + "\n" +
						`外付けアクセサリから定期的に更新情報を受け取るアプリケーション` + "\n\n" +
						`（以上、アップルの開発者向けサポート情報より引用）` + "\n\n" +
						`の6種類に限定されます。スマートフォン本体の機能拡張や性能向上に引っ張られてきた時期は、それでもあまり大きな問題ではありませんでした。しかし近年、BLE（Bluetooth Low Energy）に対応する製品が増えてくると、こうした制約が邪魔になる場合もあります。` + "\n\n" +
						`BluetoothとWiFiのスマートな連携は、もともとはアップルがやり始めた手法ですから、アップル製品同士の連携ではよく使われています。iPhoneのテザリングをリモートでオンにしたり、AirDropでファイル転送したりといった機能ですね。` + "\n\n" +
						`たとえば、今年のソニー製品ではもっともお気に入りのXperia Ear Duo。いわゆる"ヒアラブル"というジャンルに属する製品ですが、iPhoneとの接続では単なるSiri対応の左右独立型ワイヤレスイヤホンになってしまいます。` + "\n\n" +
						`もちろん、周囲の音を感じながら自然な形で音を楽しむというコンセプトはそのままですが、使ってみるとその良さを実感する通知読み上げ機能が、iPhoneでは使うことができません。` + "\n\n" +
						`ヒアラブルデバイスのトレンドは近年、盛り上がってきた分野ですから、個別にiOS側の調整が入るかも知れませんが、今後、同じように新しいトレンドが生まれていく中で後追いになっていくことは望ましくないでしょう。` + "\n\n" +
						`"なんでも開放しろ"というつもりはありません。しかし、セキュリティやバッテリーなどの問題から守りながら、拡張性を高める方法はあるように思います。` + "\n\n" +
						`アップル製品同士のタイトな統合もいいのですが、iOSとiOSデバイスには他社製アプリ（あるいはそれと連動するハードウェア）を活用するプラットフォームとしての立ち位置も大きいのですから、より幅広いIoT連携を進めていく上での仕掛けなどが出てくればいいなぁ......と、失礼。妄想に過ぎますな。` + "\n\n" +
						`このあたりを上手に交通整理させることができたなら、たとえばBLEからWiFi Directを連動させる際の機器登録の手間なども、もっと簡略化できるんじゃないでしょうかね。もちろん、アップル側にしてみれば、大量の互換製品に認証を与え、連動できますよマークを発行する仕組みなど周辺整備が大変という事情もあるかもしれませんが......。` + "\n\n\n" +
						`●もし新しいハードウェアが登場するとしたら` + "\n\n" +
						`ところで、最初に戻ってハードウェアの面では2つの点に注目しています。` + "\n\n" +
						`ひとつは新しいMac。久々に購入意欲を刺激してくれるMacが登場してくれると嬉しいなぁという反面、iPad Proがさらにクリエイティブな道具として進化し、キーボードで仕事をする人にも不満のない製品になったらどうしましょう？ という、オールドPC世代の恐怖を感じています。` + "\n\n" +
						`が、やはりこのところ話題になっているバタフライキーボードの不具合に対して、何らかの対策が語られることはあるのか？ ないのか。もちろん、ハードウェアの発表がなければ何も語られないのかもしれませんが、今回は"ある"という見方が有力ですから、では新製品のキーボード構造は？ と気になるところ。` + "\n\n" +
						`ちなにみにバタフライ構造のキー。筆者の周囲で真面目に壊してしまった例を伺うと、キーボードトラブルの後、キートップを剥がして掃除してみたり、接点復活スプレーで注油するなどして、なおさら駄目にしてしまった話を聞きます。` + "\n\n" +
						`しかし、この構造のキー、もっとも多いトラブルは接点やキーのメカ構造などが主因で故障するのではなく、微細なゴミが紛れ込んでバタフライ構造部に入り込み、それによってストロークが阻害されて動きにくくなるとうことが多いようですね。` + "\n\n" +
						`キーボード交換は極めて高価なので、個人的には強くアップルケアの延長保証に入ることを勧めますが、筆者の場合、アップル推奨の方法でエアダスターを用い、入り込んだゴミを問題のない場所に移動させるという対処療法で何度か復旧させています。` + "\n\n" +
						`が、しかし、もともと隙間が少なくゴミが入りにくいバタフライ構造のキーボード。入りにくいということは、出てくるときもなかなか出てこない。排出は諦めて、問題ない場所への移動を心がけるのが良さそうです。` + "\n\n" +
						`キーボード問題は集団訴訟なんて話題にまで発展しています。最初に登場したのは2005年の現行Macbookに採用された時のこと。さすがに遡っての対応は難しいかもしれませんが、何らかの回答をアップルには期待したいところです。` + "\n\n" +
						`なにしろMacbook Airが新モデルになってしまうと、Macbookは全機種がバタフライ構造のキーボードになるんですからね。`,
					pubDate: RFC822(time.Date(2018, 6, 3, 21, 45, 0, 0, NewYork)),
				}, {
					title: "2010年の今日、5インチAndroid機の「Dell Streak」が英国で発売されました：今日は何の日？",
					link:  "https://japanese.engadget.com/2018/06/03/2010-5-android-dell-streak/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/2010-5-android-dell-streak/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/e71636e2f7e0525ca66bd7aacd0003b9/206418898/00.jpg" />` + "\n" +
						`おもなできごと` + "\n" +
						`・2010年6月4日、デルの5インチAndroid機「Streak」が英O2から発売` + "\n" +
						`・2011年6月4日、ソフトバンクがツインカメラ搭載の「AQUOS PHONE 006SH」を発売` + "\n" +
						`・2012年6月4日、任天堂がWii Uのタッチ画面付きコントローラー「Wii U GamePad」を発表` + "\n" +
						`・2016年6月4日、auが370Mbps対応のモバイルルーター「Speed Wi-Fi NEXT W03」を発売`,
					pubDate: RFC822(time.Date(2018, 6, 3, 16, 30, 0, 0, NewYork)),
				}, {
					title: "アローラナッシー出現止まる・ヴァージン宇宙機が再びロケット飛行試験・Spotify「モラル警察」化回避 #egjp 週末版119",
					link:  "https://japanese.engadget.com/2018/06/03/spotify-egjp-118/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/03/spotify-egjp-118/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/5750b2e5eca25ff99676a69a78479a3a/206422270/main118.jpg" />` + "\n" +
						`1週間のあいだに拾いきれなかったニュースをいくつかピックアップしてお届けします。今回は「アローラナッシー、ポケモンGOでの出現止まる」「ヴァージンのVSS Unity、2度めのロケット推進飛行」「Spotify、モラル警察化を回避」といった話題を取り上げました。`,
					pubDate: RFC822(time.Date(2018, 6, 3, 7, 50, 0, 0, NewYork)),
				}, {
					title: "8型タブレット3年目の浮気 MediaPad M5 × iPad mini 4 実機比較レビュー",
					link:  "https://japanese.engadget.com/2018/06/02/8-3-mediapad-m5-ipad-mini-4/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/02/8-3-mediapad-m5-ipad-mini-4/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/893ad620e4d20351b874a4afba4a3bf6/206420470/20180601_173432.jpg" />` + "\n" +
						`8インチ前後でそこそこスペックのいいタブレット端末って、日本以外では人気なくオワコンの傾向にあり残念ですよね（AmazonのFireはSIMカードも入らないオモチャなので触れませんよ）。そんな状況下、救世主のようにHUAWEI MediaPad M5が天から舞い降り、私の周りの8タブ難民も大いにざわついていたわけですが、その割に買ったという声はあまり届きません。どした？` + "\n\n \n" +
						`関連: 丁度いいサイズ感が魅力 MediaPad M5シリーズ` + "\n" +
						`関連: MediaPad M5発表 ペン・LTE対応Androidタブレット` + "\n\n" +
						`まぁ「悩む前に買う」がモットーなので買いましたよ。最近、ジャイアンばりに買いすぎなんですが。当然、LTEモデルです、Wi-Fiモデルなんてオモチャです。価格は4万5800円（税別）。ただ、外部ストレージ用のマイクロSDカードは別途調達しないとです。` + "\n\n" +
						`スペック詳細などは上記関連記事をたどっていただくとし、MediaPad M5の比較対象は約3年前の製品であり、かつ現役のiPad mini 4（Apple）が引き合いに出されることが多い点は、業界的にも異例なことと思います。iPad miniに関してはずっと後継機が待ち望まれており、MediaPad M5が出たのだから、これ以上待つのはヘルシーではない！ というような記事を、ウチにも気が向くと書いてくれる猫のようなテクニカルライター中山氏がビジネス インサイダーに寄稿していました。` + "\n\n \n" +
						`関連: 新型iPad miniを待つならMediaPad M5の方がいい（BUSINESS INSIDER）` + "\n\n" +
						`私も長年、連れ添ってきたiPad mini 4と最近レスになりつつあり悩んでいたところ。本妻に愛人を会わせるのって財前五郎くらいかと思っていましたが、レビューのため致し方ありません。ひっぱたかれる覚悟で、実機を隣り合わせで比較していきたいと思います。`,
					pubDate: RFC822(time.Date(2018, 6, 2, 21, 57, 0, 0, NewYork)),
				}, {
					title: "2010年の今日、ミラーレス一眼「NEX-5」が発売されました：今日は何の日？",
					link:  "https://japanese.engadget.com/2018/06/02/2010-nex-5/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/02/2010-nex-5/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/285cfdfb4793c4d6b098492dbc6a3e11/206418746/00.jpg" />` + "\n" +
						`おもなできごと` + "\n" +
						`・2009年6月3日、デルがゲーミングノートPC「Alienware M17x」を発売` + "\n" +
						`・2010年6月3日、ソニーがミラーレス一眼「NEX-5」を発売` + "\n" +
						`・2014年6月3日、インテルがモバイル用の低消費電力CPUの新ブランドとして「Core M」を発表` + "\n" +
						`・2015年6月3日、VAIOが13.3インチのモバイルノートPC「VAIO Pro 13 | mk2」を発売` + "\n" +
						`・2016年6月3日、レノボが999gと軽量な12.5インチ変形2in1ノートPC「YOGA 900S」を発売`,
					pubDate: RFC822(time.Date(2018, 6, 2, 16, 30, 0, 0, NewYork)),
				}, {
					title: "iPhone SE2（仮称）は6月15日発売？WWDC直前に様々なうわさ",
					link:  "https://japanese.engadget.com/2018/06/02/iphone-se2-6-15-wwdc/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/02/iphone-se2-6-15-wwdc/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/a7c19d34ccf810bfe0769d753bcb91f5/206421392/iPhone+SE2.jpg" />` + "\n" +
						`新ハードウェア製品の発表はほぼないと予測されるWWDC 2018が近づくなか、iPhone SEの新モデル（いわゆるiPhone SE2（仮称））の新たな噂がいくつか報じられています。` + "\n\n" +
						`一つはiPhoneのリーク情報に定評あるSonny Dickson氏のツイート。iPhone Xの液晶保護フィルムとiPhone SE2用と称するフィルムを並べ、iPhone SE2にiPhone Xよりも小さめのノッチ（上部の切り欠き）があるようだとほのめかすもの。` + "\n\n" +
						`もう一つは、やはりiPhone製品のリーク情報で知られるBen Geskin氏のツイート。こちらは「iPhone SE2がiPhone Xのようなディスプレイになる可能性がある」こと。そして流出した広告代理店の資料に「iPhone SE2」「6月15日」という記載があったとの2点を伝えています。`,
					pubDate: RFC822(time.Date(2018, 6, 2, 4, 0, 0, 0, NewYork)),
				}, {
					title: "将来iPhoneは割れにくくなる？  アップルがガラスを強化する特許を申請",
					link:  "https://japanese.engadget.com/2018/06/01/iphone/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/iphone/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/c4bd16b5846ad4dbf1a0a1fa3add4a0d/206418971/glasses.jpeg" />` + "\n" +
						`アップルがポータブル電子機器のガラスを強化する技術の特許を出願していることが明らかとなりました。` + "\n\n" +
						`米特許商標庁（USPTO）により公開された同社の特許出願は、「ポータブル電子機器ハウジング（外部ケース）用の強化された剛性を備える露出ガラス」と題されたもの。` + "\n\n" +
						`その応用範囲は「メディアプレイヤーや携帯電話（PDAなど）、リモコン、ノートブック、タブレットPC、モニター、オールインワンPCなど」ということで、iPhoneやiPad、MacBookへの応用を予感させる例が上げられています。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 23, 30, 0, 0, NewYork)),
				}, {
					title: "Sponsored By Galaxy :     Galaxy S9|S9+は試してわかる機能満載、思わず「おおっ！」と声が出た：Engadget TV × Galaxy感謝祭動画レポ",
					link:  "https://japanese.engadget.com/pr/galaxy-s9-engadget-tv/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/pr/galaxy-s9-engadget-tv/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/55dbb3e1c90e62dc99b26c88d09b342d/206419717/hame.jpg" />` + "\n\n" +
						`5月19日に開催されたリアルイベント「Engadget TV ×Galaxy感謝祭」では、参加者にGalaxy S9|S9+の魅力を思う存分体験してもらう、そんな催しになりました。参加募集期間こそ短かったものの、当日の会場は超満員。ご来場いただいたみなさん、ありがとうございました。` + "\n\n" +
						`動画では登壇者や参加者のリアルな声を集め、問答無用で楽しく盛り上がったイベントの様子を3分40秒に凝縮してお届けします。Galaxy S9|S9+で広がるコミュニケーションの可能性が感じられるはず。どうぞご覧ください。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 23, 0, 0, 0, NewYork)),
				}, {
					title: "香港MVNOキャリアの「海外SIM屋」化が止まらない：山根博士のスマホよもやま話",
					link:  "https://japanese.engadget.com/2018/06/01/sim-mvno-sim/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/sim-mvno-sim/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/ec9baab9f75e65f7c63c2d07f4ea2323/206416136/EG180516_01.jpg" />` + "\n\n" +
						`筆者の居住する香港は携帯電話の契約者数で世界のトップクラスを誇る激戦区です。スマートフォンの1人2台持ちは当たり前。さらにはMVNO事業者も多く、料金の低価格化は歯止めがききません。4G開始後はデータ定額が相次いで廃止されたものの、今では月額4000円程度で速度制限なしのデータ定額をMNO各社が提供しています。` + "\n\n" +
						`大手のキャリアが手ごろな値段でデータ定額を出してくると、低料金を売りにしていたMVNOはたまったものではありません。ここ数年でMVNOの倒産もあり、市中で販売されていたプリペイドSIMがいきなり使えなくなる、なんてこともありました。香港の低料金化だけでは勝負も難しく、MVNO各社は海外向けサービスを拡充しています。` + "\n\n" +
						`そして気が付けば香港では世界各国向けのプリペイドSIMがMVNO、MNOから販売されるという不思議な現象が起きています。香港の電脳街、深水埗（シャムシュイポ）へ行くとSIM屋台が数多くあります。アジアの他の国でも見られる光景ですが、香港の屋台では香港用のSIMだけではなく、世界各国対応のSIMも販売されているのです。もちろん日本旅行用のSIMも多種揃っています。登録不要のものが大半で、それらは面倒な開通作業も不要です。` + "\n\n\n\n" +
						`そもそも人口約700万、狭い土地の香港ですから、市民のほとんどが毎月のように海外へでかけます。東京から大阪へ出張するのは国内出張ですが、香港なら台北やマニラへいく感覚で、海外出張となります。また電車で1時間の中国・深センも海外です。つまりスマートフォンの海外利用が日常的なものになっているのです。` + "\n\n" +
						`香港のMVNOの中でも大手の1つがチャイナユニコム香港（中国聯通香港）です。中国大陸のチャイナユニコムが、香港ではMVNO事業を展開しているわけです。そんな関係なことから、MVNO参入当時から中国大陸向けのプリペイドSIMを販売していました。また今は販売停止になってしまった、香港と中国の電話番号が2つ使えるプリペイドSIMも人気がありました。` + "\n\n\n\n" +
						`その後チャイナユニコム香港は海外ローミングを提供するソリューション企業と提携し、次々と海外向けプリペイドSIMを充実させていきます。一時はタイなど海外キャリアのプリペイドSIMを直接自社で販売することもありましたが、今ではほぼ自社ブランドのSIMに置き換わっています。` + "\n\n" +
						`チャイナユニコム香港のWEBページを見ると、「Travel SIM」として各国・地域向けのプリペイドSIMが揃えられています。「Great China Area」は中国や台湾、マカオ。「Asia Area」は東南アジア各国や日本、韓国。そして「Americas And Australia Area」はアメリカやオーストラリア、ヨーロッパ向けです。` + "\n\n\n\n" +
						`それぞれのSIMはパッケージに各国の民族衣装を着たキャラクターが乗っているのがカワイイですね。チャイナユニコム香港の店でもこのキャラクターを海外SIMのマスコットとして使っています。SIMの価格は滞在先と利用量に応じて様々です。` + "\n\n" +
						`その中でも有用なのは中国対応のプリペイドSIM。チャイナユニコム香港は香港のキャリアなので、中国大陸で使う時は海外キャリア扱い。つまりチャイナユニコム香港のプリペイドSIMは、中国でローミング利用となります。そのため壁越えが可能、海外SNSの利用ができます。最近はドコモなどの国際ローミングも安くなりましたが、海外では通信環境のトラブルも起こることもあります。予備やサブにチャイナユニコム香港のプリペイドSIMを1枚買っておくのも安心です。` + "\n\n\n\n" +
						`なお似た名前でも「チャイナユニコム」（中国聯通）のプリペイドSIMは中国大陸のキャリアの製品なので注意が必要です。見分け方は簡単で、パッケージの金額が香港ドル、つまりHK$で書かれていればチャイナユニコム香港のもの。中国のものは人民元表記です。` + "\n\n" +
						`意外に使えるのが日本用プリペイドSIM。日本販売のプリペイドSIMより料金は割安です。また日本で使う時はローミングになりますから、訳ありなときに使えます。日本8日間定額で2000円程度の製品もあるのですが、3GBを超えると速度規制になります。このあたりは各プリペイドSIMの製品情報を確認しておく必要があります。` + "\n\n\n" +
						`アジア周遊は最近発売になった製品で、LCCで各国を移動するときにも便利です。日本、韓国、台湾、マカオ、香港、シンガポール、タイ、マレーシアなどで利用可能。ヨーロッパ周遊も比較的新しいもので、容量は2GB/3GB/4GBと3種類あります。アジアもヨーロッパもプリペイドSIMは比較的買いやすいとはいえ、現地で時間の無いときはこのような周遊SIMを買っておくと便利でしょう。` + "\n\n\n\n" +
						`さてプリペイドSIMも、今ではタイのキャリアの製品がアジアや世界各国に対応した「神SIM」化しており、日本でもアマゾンなどで販売されています。チャイナユニコム香港もタイのSIMはかなり意識しているようで、周遊タイプを相次いで投入しているわけです。なにせ香港のSIM屋台では、海外販売SIMも販売されています。香港の市民はもはや「どこのキャリアのSIMか」を意識せず、行きたい国のSIMを安価に買うことができるのです。` + "\n\n\n\n" +
						`このようにもはや香港MVNOのライバルは同じ香港のMNO、MVNOだけではなく、海外キャリアになっています。競争の激化が料金の低価格化をすすめ、体力のない企業は倒産するほど厳しい市場ですが、それによりスマートフォンがさらに使いやすいものになっているのは事実。チャイナユニコム香港は日本のアマゾンでも直接プリペイドSIMの販売を行い、さらなる拡販を進めているのです。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 21, 0, 0, 0, NewYork)),
				}, {
					title: "デジタルお絵描きでバトる「LIMITS」初観戦：小彩楓のよちよちIT体験記",
					link:  "https://japanese.engadget.com/2018/06/01/limits-it/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/limits-it/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/ad4c4c12fd39d544cbb7c7d590dd93f9/206413619/S__77996050.jpg" />` + "\n" +
						`渋谷ヒカリエホールにて5月12〜13日、開催されたデジタルアートバトル「LIMITS」を観戦してきました！(﹡ˆ-ˆ﹡)` + "\n\n" +
						`今回は、その感想をイラストレーターなりの目線で書いてみたいと思います。` + "\n\n" +
						`まずはLIMITSのルールを簡単に。アーティストによる1対1の対決で、本番直前にルーレットで決められた2つのワードから連想した絵を各々が20分間で完成させます。` + "\n\n \n" +
						`関連: デジタルアートバトル LIMITSが世界大会開催` + "\n\n" +
						`勝敗は4名の審査員とネット投票により決定します。今回は、世界各地の予選を勝ち抜いてきた16名によるトーナメント戦でした。` + "\n\n" +
						`描いた絵で競う大会は、ほかにもありますが、LIMITSの最大の特徴は、絵の完成度だけで勝敗が決まるわけではなく、途中経過も重要な審査対象となる点です。` + "\n\n" +
						`途中経過であまり完成度に期待がもてない作品が、後半のどんでん返しですばらしい作品に仕上がったり、観客のイメージしていた雰囲気とはまったく違った作品に仕上がったりと、見ている人をいい意味で裏切り、驚かせることが重要となります。最初に描いたものを途中ですべて消してしまうなんてことも──そのぶん時間はかかりますが、見ている人に完成図を予想させず、いかに最後まで楽しませることができるかが鍵となります🏆🗝` + "\n\n" +
						`絵の対決というより、パフォーマンス対決といったほうが適切かもしれませんね。` + "\n\n" +
						`基本的に絵を描く際は、構図を意識します。完成図を頭の中でイメージし「アタリ」をつけ、全体のバランスを取りながら描き進めていくほうが、失敗が少なく、時間の短縮にもなります。` + "\n\n" +
						`しかし、その描き方では見てる側の面白味に欠いてしまうため、色の付け方を含め、どういう順序で描き進めれば、観客に完成図を予想されずに完成度の高いものを仕上げられるか。そして、これを瞬時に判断しなくてはなりません...( *˙ _˙* ;)` + "\n\n" +
						`どんなに熟練したアーティストでも、LIMITSで優勝を目指すなら特訓が必要だと思いました。` + "\n\n" +
						`今回、優勝を果たしたアメリカ代表のAhmed Aldooriさんは、途中のパフォーマンスでもたびたび歓声が湧き、完成した作品も動きや立体感があって見応えのあるものでした！(﹡ˆ ˆ﹡)` + "\n\n" +
						`とくに準決勝で、突然ドラゴンが現われるパフォーマンスは、思わず「わぁ〜っ」と声をあげてしまいました！( *˙◯˙* )` + "\n\n" +
						`デジタルならではのパフォーマンスですよね。` + "\n\n" +
						`手描きでは不可能なことが次々と起こるLIMITSアートバトル。デジタルの魅力をより一層感じることができました☺️` + "\n\n\n" +
						`▲優勝賞金は500万円！` + "\n\n" +
						`次回の大会もぜひ見に行きたいです！(﹡ˆ_ˆ﹡)`,
					pubDate: RFC822(time.Date(2018, 6, 1, 20, 0, 0, 0, NewYork)),
				}, {
					title: "アップル、MacのCPU自社開発をすでにスタートか。オレゴン州の「秘密の研究施設」で技術者募集",
					link:  "https://japanese.engadget.com/2018/06/01/mac-cpu/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/mac-cpu/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/a88718422c56173987654640f08a2f28/206421080/main.jpg" />` + "\n\n" +
						`アップルがMacのメインプロセッサーを自社製チップに置き換えるべく開発を開始した模様です。当初の話だと、アップルが現在Macで使用しているインテルのプロセッサーを2020年から自社開発のチップに置き換える作業を開始するとのことでした。しかし、最新の情報では、オレゴンにある秘密の研究施設で、すでに開発はスタートしていると伝えられています。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 18, 0, 0, 0, NewYork)),
				}, {
					title: "2013年の今日、Haswellこと「第4世代Coreプロセッサー」が正式発表前に発売されました：今日は何の日？",
					link:  "https://japanese.engadget.com/2018/06/01/2013-haswell-4-core/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/2013-haswell-4-core/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/54dc465cabe234ab53a1e24857b18202/206418081/00.jpg" />` + "\n" +
						`おもなできごと` + "\n" +
						`・2013年6月2日、インテルが「第4世代Coreプロセッサー」（Haswell）を発売` + "\n" +
						`・2014年6月2日、NexTV-FがCSデジタルで4Kテレビ試験放送「Channel 4K」を開始` + "\n" +
						`・2014年6月2日、サムスンが商用では初となるTizen採用スマホ「Samsung Z」を発表` + "\n" +
						`・2015年6月2日、KADOKAWAが「週刊アスキー」完全電子化創刊号を発売` + "\n" +
						`・2017年6月2日、GoProが空撮対応ドローン「Karma」を国内で発売`,
					pubDate: RFC822(time.Date(2018, 6, 1, 16, 30, 0, 0, NewYork)),
				}, {
					title: "ソフトバンクの10兆円ファンド、GMの自動運転開発会社に約2428億円出資。業界に影響力確保も",
					link:  "https://japanese.engadget.com/2018/06/01/10-gm-2428/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/10-gm-2428/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/3e5b9758851d8401bbe965a83f0904f8/206419017/gmc.jpg" />` + "\n" +
						`米ゼネラル・モータース(GM)は、自動運転技術を開発するグループ会社Cruiseが、ソフトバンクの投資会社Softbank Vision Fund(SVF)から22億2500万ドル(約2428億円)の出資を受けることを発表しました。ソフトバンクは少し前からUber、Didi Chuxingといったネット配車サービスへの投資をしてきましたが、次の一手は自動運転の分野にコマを進めることになります。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 6, 0, 0, 0, NewYork)),
				}, {
					title: "Arm、35％も性能向上の「Cortex-A76」などモバイル向け新プロセッサ3製品発表",
					link:  "https://japanese.engadget.com/2018/06/01/arm-35-cortex-a76-3/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/arm-35-cortex-a76-3/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/677e01c6e50e659850834a06726442eb/206418982/20180601nintel.jpeg" />` + "\n\n" +
						`［Engadget US版より（原文へ）］` + "\n\n" +
						`消費者がよりよいグラフィックス、素早いレスポンスとリアルなXR（VRやMR、AR）体験をモバイルデバイスに求めることで、CPUやグラフィックス、ビデオ処理のプロセッサはますますそれに追いつくように要求されています。しかしArmがサンフランシスコで火曜日にイベントで発表した3つのプロセッサは、より高いパフォーマンス、没入感の高いゲームグラフィックス、UHD 8K解像度動画の再生を実現し、さらに将来のモバイルデバイスのために高性能なAI（人工知能）を提供するのです。` + "\n\n" +
						`新プロセッサのアーキテクチャは、Armによる2019年版クライアントIPプラットフォームやARM AI/ML開発プラットフォーム「Project Trillium」に基づいています。まず、「ARM Cortex-A76」は同社のハイパフォーマンス市場向けの最新CPUです。Cortex-A7xシリーズはラップトップ向けに設計されており、いくつかのWindows 10搭載パソコンにも搭載されています。まず、昨年モデルのA75は20時間以上の駆動時間を実現していました。そしてArmのIPグループのRene Haas社長によれば、新型のA76は40%高効率になりながら35％の性能向上を実現しています。` + "\n\n" +
						`ハイパフォーマンスGPUとなるArmのMali-Gシリーズも、2019年の次世代プロセッサとして登場します。G76は報道によれば、2017年モデルのMali-G72に比べて30%も高効率かつ高性能。これにより、モバイルデバイスでの幅広いハイエンドゲームや、重量級のAR/VR機能をゲームプレイに統合したアプリをサポートすることができます。` + "\n\n" +
						`モバイル動画愛好家、特にRED Hydrogen Oneが高くて買えない人にも朗報です。Armの発表によれば、新型の「Mali-V76」VPN（ビデオ・プロセッシング・ユニット）は最大60fpsの8K UHD解像度動画、あるいは4個の4K/60fps動画のデコードが可能です。さらに解像度を通常のHDにまで落とすことで、V76は最大で16個もの動画ストリームを表示することができるのです。` + "\n\n" +
						`編集部が日本向けに翻訳・編集したものです。` + "\n" +
						`原文著者：Andrew Tarantola`,
					pubDate: RFC822(time.Date(2018, 6, 1, 5, 40, 0, 0, NewYork)),
				}, {
					title: "ドローンが描いたTIME誌の表紙をドローンで撮影した写真がTIME誌の表紙に。6月1日発売のTIME誌に掲載",
					link:  "https://japanese.engadget.com/2018/06/01/time-time-6-1-time/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/time-time-6-1-time/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/34b540c043eb1311b5e6727daa2de580/206421699/33084455_10209707176355626_2273325386613915648_n.gif" />` + "\n" +
						`インテルのドローンが米TIME誌の表紙になります。といってもドローン単体の写真ではなく、958機のドローンの群れが夜空に描いたTIME誌の表紙(の写真)が6月1日発売のTIME誌の表紙になるということです。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 5, 0, 0, 0, NewYork)),
				}, {
					title: "Galaxyの「高度な機能」で覚えておきたい3つの設定：Galaxy Tips",
					link:  "https://japanese.engadget.com/2018/06/01/galaxy-3-galaxy-tips/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/galaxy-3-galaxy-tips/"},
					desc: `<img src="https://o.aolcdn.com/images/dims?crop=2736%2C1824%2C0%2C0&quality=85&format=jpg&resize=1200%2C800&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F1fa744053681b6ebc39ab7af4e25a226%2F206416078%2Ftop.JPG&client=a1acac3e1b3290917d92&signature=e05ec74e16fa9ffbfe2f83878cf58ec46c0f8208" />Galaxyの設定には、「高度な機能」という項目があります。今回は同設定項目より、必ず覚えておきたい3つの機能をピックアップしてご紹介。デフォルトでオンになっている項目もありますが、カスタマイズすることでさらに便利に使えるはずです。` + "\n\n" +
						`※本記事の手順画面では、Android 8.0のGalaxy S8を使用しています。` + "\n\n" +
						`<p><a href="https://japanese.engadget.com/gallery/3-21/" title="「高度な機能」から使いたい設定3選">【ギャラリー】「高度な機能」から使いたい設定3選 (4枚)</a></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 4, 0, 0, 0, NewYork)),
				}, {
					title: "ガジェットモバイラー向けバックパック OnePack（ワンパック）が発売",
					link:  "https://japanese.engadget.com/2018/06/01/onepack/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/onepack/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/4e75653088905d3b9e2f655f48f96dd5/206418827/op001.gif" />` + "\n" +
						`ガジェット好きモバイラー向けバックパック OnePack（ワンパック）` + "\n\n\n" +
						`ノートPCにタブレット、スマホにイヤホンにバッテリー......ハードモバイラーなみなさんは、きっとこれらの収納にいろいろな工夫をされてきたと思います。こちらはとくに荷物の多い、リュック派向けの新商品になります。` + "\n \n\n\n\n" +
						`超高機能バックパック OnePack` + "\n\n" +
						`商品詳細を見る`,
					pubDate: RFC822(time.Date(2018, 6, 1, 3, 0, 0, 0, NewYork)),
				}, {
					title: "MSの音楽再生アプリGroove Music、iOS / Android版が12月で使用不能に。削除推奨",
					link:  "https://japanese.engadget.com/2018/06/01/android-ios-groove-music-12/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/android-ios-groove-music-12/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/c18f194873ca324aec2ce9c447ea7466/206418783/msg.jpg" />` + "\n" +
						`マイクロソフトの音楽プレーヤーアプリ"Groove Music"のAndroidおよびiOSアプリが12月1日に使用不能になります。` + "\n\n" +
						`Groove MusicアプリはローカルやOneDriveにある音楽ファイルや、マイクロソフトが提供していた音楽ストリーミングサービスGroove Music Passの音楽を再生するためのプレーヤーソフトです。しかしGroove Music Passは2017年12月31日にそのサービスを終了し、そのユーザーたちはSpotifyへと引き継がれました。SpotifyはSpotify側のアプリで再生するため、Groove MusicアプリはローカルやOneDriveにある音楽を再生するためのアプリとしての余生を過ごす状態になっていました。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 2, 0, 0, 0, NewYork)),
				}, {
					title: "「iPhoneのNFC開放」を考察する。WWDC 2018で発表と噂：モバイル決済最前線",
					link:  "https://japanese.engadget.com/2018/06/01/wwdc18-iphone-nfc/",
					guid:  RSSGUID{GUID: "https://japanese.engadget.com/2018/06/01/wwdc18-iphone-nfc/"},
					desc: `<img src="https://s.aolcdn.com/hss/storage/midas/598e9f5a58eca16778246339df0f43e3/206418749/ph01.jpg" />` + "\n" +
						`The Informationの5月25日（米国時間）の報道によれば、Appleは現在iPhoneに内蔵されているNFC通信の仕組みをさらに開放して決済以外のサービス、より具体的には公共交通機関の乗車チケットやホテルの"鍵"などを利用可能にする計画だという。` + "\n\n" +
						`同社はiOS 11.3以降のバージョンですでに中国の北京と上海の都市交通への乗車が可能なベータ版のサービスを公開しているほか、日本ではSuicaを通じて交通サービスや物販利用が可能となっている。今回の報道は、この仕組みをさらに世界中の広範囲なサービスに展開していくことが目的だとみられる。` + "\n\n" +
						`6月4日（米国時間）には米カリフォルニア州サンノゼでAppleの開発者会議「WWDC18」が開催されるが、早ければこのタイミングでNFC開放に関する何らかの発表が行われるとみられている。今回はiPhoneとNFC、そして海外の交通サービス事情について、事前に少しだけ情報を整理しておく。` + "\n\n" +
						`NFCの一部機能しか実装されていないiPhoneの"NFC"` + "\n" +
						`2014年にApple Payがデビューしたとき、iPhone 6に初めて搭載されたNFCのアンテナは決済専用だった。Apple Payに対応するクレジットカードまたはデビットカードを登録すると銀行（イシュア）からバーチャルカードが発行され、これを使って店頭またはオンラインでの決済に使える。` + "\n\n" +
						`店頭ではEMV Contactlessと呼ばれる非接触方式の通信で国際ブランドのカード決済ネットワークを利用し、財布から物理的なカードを取り出さずに支払いが行えるというのがセールスポイントだった。あれから3年半、筆者は世界で日々増え続けるNFC決済対応店舗の中でApple Payを便利に活用し続けている。` + "\n\n" +
						`さて、本来のモバイルNFCとは近接通信（Near Field Communication）における3つの通信モード（「Card Emulation」「Reader/Writer」「Peer-to-Peer」）を備えたものを指す。だがiPhoneにおける"NFC"では実質的に「Card Emulation」の機能しかサポートしておらず、その機能もApple Pay内でAppleが許可したサービスしか利用できないため、実質的にクローズドなものとなっている。` + "\n\n" +
						`カードやタグ情報の読み書きが可能な「Reader/Writer」については、iPhone 7以降でSuicaカードの吸い出しという機能限定で利用可能になったものの、一般には開放されていない。iOS 11ではCore NFCのフレームワークが追加されており、NFC Forumが定義する5種類のタグの読み書きが可能になっているものの、これはまだ一歩に過ぎない。モバイル端末同士が対向での通信を行う「Peer-to-Peer」に至っては機能そのものが考慮されておらず、Appleがどのような計画を持っているのかさえ不明だ。` + "\n\n" +
						`つまり現状のiPhoneの"NFC"は「そもそもNFCのフル機能を備えていない」「Card Emulationに必要なセキュアエレメント（SE）が外部開放されていない」という2つの課題を抱えている。` + "\n\n\n" +
						`▲NFC Forumで定義されるNFCの3つのモードとApple Pay` + "\n\n" +
						`今回のThe Informationの報道は、このうちの後者について、セキュアエレメントをサードパーティの一部に開放するという話だ。開放範囲や条件は不明だが、冒頭にもあるように「公共交通機関」「ホテルの"鍵"」「入館証（身分証）」といった決済以外の部分でサービスの適用範囲を広げるものが中心になるとみられ、特に"鍵"や入館証といった特定の施設やエリアでのみ利用可能な仕組みについて、セキュアエレメントの利用制限を緩和し、ユーザーが指定のアプリを導入することで既存の物理的なICカードをiPhoneで代用できるようになると推測している。` + "\n\n" +
						`最近の比較的新しいホテルでは部屋の鍵が非接触通信に対応したICカードタイプのものが増えているが、例えばiPhone向けにホテルが"鍵"の機能をオンライン配布できるアプリを提供することで、利用者はフロントを経由せずにそのままオンラインチェックインして部屋に直行することも可能になる。またThe Informationによれば、Appleは新キャンパスのApple Park内においてHID Globalと共同で、このiPhoneによる入館証の仕組みをすでに展開済みだという。HIDの認証ゲートの仕組みは多くの企業やビルで採用されており、これが外部展開されても不思議ではないだろう。` + "\n" +
						`最初の交通系ICサービス対応の事例は西海岸から` + "\n" +
						`ここでやや複雑となるのが「交通系ICカード」の話だ。すでに紹介したように日本国内ではSuica、ベータ版ではあるものの北京と上海の都市交通のICカードがApple Payではサポートされており、Walletアプリに登録して利用することができる。現在Apple Payで乗車可能な公共交通は同社のサポートページで一覧を確認できるが、そのほとんどは普段使いのクレジットカードやデビットカードを非接触通信で"かざす"ことで改札での運賃支払いが可能な「オープンループ（Open Loop）」と呼ばれる仕組みを採用している。` + "\n\n" +
						`代表的なものはロンドン交通局（TfL）のサービスだが、Apple Payのサポートページに記載されているものだけを挙げてもシカゴのVentra、ポートランドのTriMet、モスクワメトロはそれに該当する。中国の広州（Guangzhou）と杭州（Hangzhou）はCUP（China UnionPay）となっているが、これもいわゆる銀聯カードを使ったオープンループだといえる。オープンループは地域ごとにICカードを購入したり、逐次チャージを行う必要がないため、外国人のような一見客にも優しい仕組みなのが特徴といえる。` + "\n\n\n\n" +
						`▲現在AppleがサポートページでうたっているApple Pay乗車が可能な世界の交通機関` + "\n\n" +
						`Apple Payは対応する国際ブランドのカードさえ対応していれば、理論的にはどのオープンループの公共交通も利用可能なので手間がない。現在シンガポールで現地交通局（LTA）とMastercardが共同でオープンループのトライアルを1年近くにわたって実行しているほか、2018年中には米ニューヨークのMTAがオープンループに対応した改札システムの広域展開を開始し、従来の磁気カードベースのMetroCardを数年で置き換えていく計画だ。` + "\n\n" +
						`このほか、オーストラリアではシドニーを含むニューサウスウェールズ（NSW）地域で展開されているOpalカードをオープンループで置き換えていく計画もあり、昨年2017年には一部フェリー路線でクレジットカードやデビットカードを使った改札システムの運用が始まっている。` + "\n\n\n\n" +
						`▲オープンループ乗車の基本モデルとなったロンドンの公共交通` + "\n\n" +
						`オープンループのシステムは少しずつ増えているものの、依然として世界の都市交通システムは都市単位で閉じていることがほとんどで、互いに相互運用は行われていない。旅行者は、都市を移動するごとに切符やICカードを買い求め、手持ちのスマートフォンを使うことなく公共交通への乗車を強いられている。` + "\n\n" +
						`日本ではおサイフケータイのモバイルSuicaやApple Payが簡単に利用できるため問題ないが、世界的にみれば交通系ICカードをモバイル対応させるという取り組みは少数派だ。ZDNetが先日、オーストラリアのメルボルンを含むビクトリア地域圏で利用されるMykiカードをモバイル対応させたトライアルサービスを2019年初頭までにAndroid端末を対象に開始すると報じて話題となったが、中国でXiaomiやHuaweiが自社端末搭載の決済サービスで「中国国内の多くの都市交通のモバイル乗車」を可能にしていることを除けば、まだまだ個別の取り組みにとどまっている。` + "\n\n" +
						`Apple Payの交通系サービス対応とは、こうした各都市ごとに異なる交通系ICの仕組みを順次取り込んでいくことを意味する。The Informationによれば、Cubicとの提携でApple Pay内に交通系ICカードを"Card Emulation"で取り込む仕組みを実装していこうとしているようだ。` + "\n\n" +
						`Cubicと社名を聞いてもピンとこないかもしれないが、ロンドンでTfLのオープンループを実装した運行システム会社だといえばわかるだろうか。前述のシンガポールやニューヨークのシステムにも噛んでおり、世界的にみてスタンダードな存在だと考えている。` + "\n\n" +
						`さらにCubicといえば、米サンフランシスコやWWDC18が開催されるサンノゼ一帯のベイエリアで運用されている「Clipper」という交通系ICカードのシステムも提供している。筆者はここ数年ほど、AppleがiPhoneのClipper実装に向けて関係各方面と調整やテストを続けているという話を何度か聞いており、おそらくCubicのApple Pay対応事例として最初に挙げられるものの1つにClipperが含まれると予想している。` + "\n\n" +
						`このほか、Xiaomiなどの事例ですでに実績のある中国系の交通系サービス取り込みもそれほど難しくないと考えられ、今後iOS 12の世代で一気に対応が進んでいくことになるだろう。` + "\n\n\n" +
						`▲米サンフランシスコ・ベイエリア周辺で利用されるClipperカードと英ロンドン（TfL）のOysterカード` + "\n" +
						`セキュアエレメントをどこまで開放するのか` + "\n" +
						`「NFC開放」というと「NFCアンテナの一般開発者への開放」のようにも捉えられるが、ビジネス的な側面から考えれば「セキュアエレメントの領域解放」という意味合いに近い。現在のところ、iPhoneのセキュアエレメントにはApple内部にあるサーバを経由してしかアクセスできず、ごく一部の例外（Suicaアプリ）を除いてアクセスする手段は用意されていない。` + "\n\n" +
						`これは決済サービスの提供で重要となるカード情報といった個人情報をApple自身がすべて制御下に置いて主導権を握っているというだけでなく、例えばデバイス紛失や乗り換えなどがあったとしても、あらかじめApple IDにカード情報を紐付けておけば、セットアップのプロセスを通じてAppleのサーバから自動的に書き戻しが行われるという、非常に便利な機能が利用できる。これは既存のおサイフケータイにはない、非常に大きなメリットだ。` + "\n\n" +
						`今回の「セキュアエレメントの領域解放」で気になるポイントは2点あり、「交通系ICカードだけでなく、すべてのカード情報はAppleの制御下に入るのか（つまりAppleのサーバを経由する必要があるのか）」「特定のアプリケーション（例えばHID Globalの入館証）ではデベロッパー（この場合はHID）単位で領域解放が行われ、この領域内では各デベロッパーが自由に読み書きができるのか（つまり入館証をAppleの介在なしに登録できるのか）」という部分に注視している。` + "\n\n" +
						`デベロッパーの使い勝手としては後者に軍配が上がるが、ユーザー的な利便性やAppleのこれまでの戦略を考えれば前者である可能性が高く、WWDC18で発表される場合の内容に注目したい。いずれにせよ、セキュアエレメントはそれ自体が資産であり、かつては携帯キャリアとGoogleがNFC世界での主導権を巡って激しい争いを繰り広げた最大の戦場でもある。いまいちど、この不毛な争いをApple Payで終結させたAppleの采配に注目したい。`,
					pubDate: RFC822(time.Date(2018, 6, 1, 1, 30, 0, 0, NewYork)),
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}

				if ch.Items[i].Author != "" {
					t.Errorf("must empty: %q", ch.Items[i].Author)
				}

				if ch.Items[i].Categories != nil {
					t.Errorf("must nil: %q", ch.Items[i].Categories)
				}

				if ch.Items[i].Comments != "" {
					t.Errorf("must empty: %q", ch.Items[i].Comments)
				}

				if ch.Items[i].Enclosure != nil {
					t.Errorf("must nil: %q", ch.Items[i].Enclosure)
				}

				if target := items[i].guid; !ch.Items[i].GUID.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].GUID)
				}

				if target := items[i].pubDate; !ch.Items[i].PubDate.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].PubDate)
				}

				if ch.Items[i].Source != nil {
					t.Errorf("must nil: %q", ch.Items[i].Source)
				}
			}
		}
	})

	source = "solidot.rss"
	t.Run(source, func(t *testing.T) {
		source = "sample_rss/" + source

		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "2.0"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "最新更新 – Solidot"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if ch.Link != "" {
			t.Errorf("must empty: %q", ch.Link)
		}

		if target := "奇客的资讯，重要的东西。"; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "zh-cn"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if ch.Copyright != "" {
			t.Errorf("must empty: %q", ch.Copyright)
		}

		if ch.ManagingEditor != "" {
			t.Errorf("must empty: %q", ch.ManagingEditor)
		}

		if ch.WebMaster != "" {
			t.Errorf("must empty: %q", ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if target := RFC822(time.Date(
			2018, 6, 4, 12, 10, 47, 0, Beijing,
		)); !ch.LastBuildDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if ch.Docs != "" {
			t.Errorf("must empty: %q", ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 20; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image != nil {
			t.Errorf("must nil: %q", ch.Image)
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if ch.Items == nil {
			t.Errorf("must equal not nil: %q", ch.Items)
		}

		if target := 20; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title   string
				link    string
				desc    string
				pubDate RFC822
			}{
				{
					title:   "微软拒绝更改受争议的 GVFS 项目名字",
					link:    "https://www.solidot.org/story?sid=56716",
					desc:    `微软去年透露了 Git Virtual File System（GVFS）项目，GVFS 是 Git 版本控制系统的一个开源插件，允许 Git 处理 TB 规模的代码库，比如 270 GB 的 Windows 代码库。该项目公布之初<a href="https://tech.slashdot.org/story/18/06/03/0626246/microsoft-sticks-with-controversial-gvfs-name-despite-backlash" target="_blank">就引发了争议</a>，原因是 GVFS 这个名字已经有开源项目<a href="https://medium.com/@DuroSoft/despite-backlash-microsoft-sticks-with-controversial-gvfs-name-57d6e175af9c" target="_blank">使用了</a>。GNOME 项目的虚拟文件系统组件就叫 GVfs，两个项目都与虚拟文件系统有关，因此相同的缩写显然会引发混淆，而微软的知名度也很容易将 GVfs 淹没在搜索结果深处。GVFS 是一个开源项目，因此开发者在 GitHub 上提出了改名的请求，并递交了 <a href="https://github.com/Microsoft/GVFS/pull/51" target="_blank">pull request </a>允许微软在代码库里替换 GVFS  到任意一个微软想要的名字。然而微软关闭了所有改名的问题，关闭了相关的  pull request。软件巨人显然无意修改名字。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 4, 12, 2, 4, 0, Beijing)),
				}, {
					title:   "全球烟民数量下降，电子烟使用者在增加",
					link:    "https://www.solidot.org/story?sid=56715",
					desc:    `世界卫生组织最新的数字显示，2018 年全球约有 11 亿人有抽烟的习惯，比 2000 年约有 11.4 亿人少，而与此同时电子烟的使用人口<a href="http://www.bbc.com/zhongwen/simp/uk-44340480" target="_blank">在慢慢增多</a>。总部在伦敦的全球零售市场分析研究公司 Euromonitor 的调查显示，2011 年全球有约 700 万人有抽电子烟的习惯，到了 2016 年，这个数字升至 3500 万。Euromonitor 同时估计，抽电子烟的人口到 2021 年会进一步升至 5500 万人。全球在电子烟的花费也水涨船高：电子烟和相关产品在全球的销售额五年前只有 42 亿美元，至今已涨至 226 亿美元。这家公司的另一项调查显示，美国、日本和英国是全球最大的电子烟市场，这三个国家的消费者在 2016 年合共买了价值 163 亿美元的电子烟和相关产品。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 4, 11, 28, 44, 0, Beijing)),
				}, {
					title:   "Linux 4.17 发布",
					link:    "https://www.solidot.org/story?sid=56714",
					desc:    `Linus Torvalds 在内核邮件列表上<a href="https://lkml.org/lkml/2018/6/3/142" target="_blank">宣布</a>释出 4.17 kernel。这个版本仍然叫 4.17 而不是 5.0，Linus 称 5.0 会在不久的未来到来，不过暂时还没有理由更新大版本号。他估计可能会在 4.20 左右当手指脚趾加起来都不能数完小版本号之后切换到新的大版本号。Linux 4.17 的<a href="https://kernelnewbies.org/Linux_4.17" target="_blank">主要新特性</a>包括：改进 CPU 调度器的<a href="https://lwn.net/Articles/741171/" target="_blank">负载估算</a>，raw BPF 跟踪点，XF 文件系统支持 lazytime ，内核 TLS 协议完全支持，<a href="https://lwn.net/Articles/635522/" target="_blank">histograms 触发器</a>，修补最新 Spectre 漏洞变种，以及移除八种基本没人再使用的过时处理器架构，支持 AMDGPU WattMan；支持 Intel HDCP；Vega 12 GPU 和 NVIDIA Xavier SoC 支持；默认启用  AMDGPU DC；改进电源管理，等等。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 4, 11, 13, 12, 0, Beijing)),
				}, {
					title:   "微软同意收购 GitHub，用户涌向 GitLab",
					link:    "https://www.solidot.org/story?sid=56713",
					desc:    `彭博社援引知情人士的消息<a href="https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github?" target="_blank">报道</a>，微软同意收购最大的源代码托管平台 GitHub。一名匿名知情人士称，GitHub 宁愿选择出售而不是上市，它选择微软是因为被该公司 CEO Satya Nadella 打动了。目前还不清楚收购的条款，GitHub 在 2015 年估值 20 亿美元。这笔交易对双方都有利，GitHub 还没有盈利，而微软则越来越多的依赖开源软件。这一消息传出之后，大量用户<a href="https://monitor.gitlab.net/dashboard/db/github-importer?orgId=1" target="_blank">涌向</a>了 GitHub 的竞争对手 GitLab，他们将托管在 GitHub 上的项目<a href="https://docs.gitlab.com/ee/user/project/import/github.html" target="_blank">导入</a> GitLab。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 4, 9, 50, 4, 0, Beijing)),
				}, {
					title:   "VLC 发布 ARM64 Windows 应用",
					link:    "https://www.solidot.org/story?sid=56712",
					desc:    `运行在 ARM 架构处理器上的 Windows PC 已经现身，但支持 64 位 ARM 的原生 Windows 应用十分稀少。虽然微软提供了 Windows on Windows(简称 WOW) 的抽象层，通过模拟器运行 x86 程序，但模拟器此前只支持 32 位应用，不支持 64 位应用，也不支持部分访问内核驱动的 32 位应用。现在流行的开源多媒体播放器 VLC 成为<a href="https://www.engadget.com/2018/06/01/vlc-one-of-first-arm64-windows-apps/" target="_blank">首批支持</a> ARM64 架构的 Windows 应用。它的 Windows 应用<a href="https://www.videolan.org/vlc/download-windows.html" target="_blank">提供了两个版本</a>：64 位 Windows 和ARM 64 版本。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 3, 23, 8, 57, 0, Beijing)),
				}, {
					title:   "RMS 认为应该对危害社会的科技巨头征税",
					link:    "https://www.solidot.org/story?sid=56711",
					desc:    `如何监管对社会影响力越来越大的科技巨头？美国国内<a href="https://boingboing.net/2018/05/22/too-big-to-fail-2.html" target="_blank">有两派观点</a>，一种杰佛逊派的，另一种是汉密尔顿派的。杰佛逊派认为应该立即终止巨头们的收购狂欢，他们认为巨头的权力过于强大，过于集中，阻碍了小型企业冒险开发突破性技术，杰佛逊派崇尚“小而美”的民主。汉密尔顿派则对垄断性企业比较友好，他们认为只要巨头们不要合谋涨价，政府不应该插手。他们认为大型企业在集中管理和保护数据方面似乎更有责任感，而你不知道数据落在中小企业手中会如何被滥用，为防止数据失控，数据封建主义是必要的，汉密尔顿派拥护中央集权。对于这两派的观点，自由软件基金会主席 Richard Stallman（RMS）<a href="https://stallman.org/archives/2018-mar-jun.html#1_June_2018_(Commercial_surveillance_of_individuals)" target="_blank">认为</a>破坏隐私的大规模商业监视给予国家一个完美的镇压基础，数据收集本身就是一种危险。他认为应该根据危害社会的程度对这些监控资本主义的代表性企业<a href="https://yro.slashdot.org/story/18/06/02/231259/richard-stallman-asks-should-big-tech-be-taxed-for-hurting-society" target="_blank">进行征税</a>。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 3, 22, 37, 46, 0, Beijing)),
				}, {
					title:   "中兴面临 17 亿美元罚单",
					link:    "https://www.solidot.org/story?sid=56710",
					desc:    `路透社援引知情人士的消息<a href="https://cn.reuters.com/article/exclusive-us-zte-fine-0603-idCNKCS1IZ0A0" target="_blank">报道</a>，特朗普政府可能很快向中兴开出高达 17 亿美元的罚单。此前特朗普曾称将对中兴加罚 13 亿美元，17 亿美元并不表示金额增加，而是包含了中兴去年同意向美国商务部支付的 3.61 亿美元民事罚金。美国商务部今年 4 月因中兴违反和解协议而对其实施了 7 年的贸易禁令，导致其因缺乏美国零部件而停止主要经营活动。美国商务部还在寻求对中兴进行不受约束的实地考察，以确认美国的零部件使用情况是否与中兴说法一致，并希望在网站公布其产品使用美国零部件的数据。美国还希望中兴 30 天内改组董事会和高管团队，但还未敲定协议，消息人士表示，罚金数额可能改变，条款可能进行调整。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 3, 21, 59, 24, 0, Beijing)),
				}, {
					title:   "中国玩家比西方玩家更接受“花钱买优势”",
					link:    "https://www.solidot.org/story?sid=56709",
					desc:    `西方玩家可能不赞成在游戏中花钱获得优势的做法，但中国玩家<a href="https://www.cnbc.com/2018/05/30/pay-to-win-video-games-differences-between-us-and-chinese-gamers.html" target="_blank">对此早已习以为常</a>。对待 pay-to-win 的不同态度可能源自于不同的文化规范和游戏市场不同的起点。中国曾在长达 13 年里禁止游戏主机，2000 年到 2013 年之间的中国主机市场一直只存在于灰市，因此 PC 是中国最主流的游戏平台，其中需要多次支付费用的网络游戏是最流行的消费产品。中国玩家已经习惯为游戏多次花钱，当游戏趋势转向采用微交易模式的免费游戏时，他们也习惯花钱获得游戏中的优势，比如获得更好的装备和更好的游戏人物。西方游戏的开发商也已经开始拥抱这一趋势，西方的玩家最终可能会和东方玩家一样接受 pay-to-win 的概念。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 3, 11, 53, 26, 0, Beijing)),
				}, {
					title:   "将 Tor 整合进 Firefox",
					link:    "https://www.solidot.org/story?sid=56708",
					desc:    `Tor Browser 是基于 Firefox ESR，开发者需要在他们维护的 Firefox 分支上花费大量时间打补丁，为了让 Tor 开发者能集中精力在 Tor 上，作为上游项目的 Mozilla 开始将 Tor Browser 补丁整合进 Firefox。过去一年半中，Firefox 先后整合了两大来自 Tor 浏览器功能：<a href="https://www.solidot.org/story?sid=54573" target="_blank">第一方隔离</a>和<a href="https://www.solidot.org/story?sid=50921" target="_blank">防指纹跟踪</a>。这些功能仍然存在可用性和易用性问题，如第一方隔离默认没有启用。接下来 Mozilla 将更深入整合 Tor。它启动了名为 <a href="https://wiki.mozilla.org/Security/Fusion" target="_blank">Fusion Project</a> 的项目，目标包括改善防指纹跟踪，实现代理绕过框架，寻找方法将 Tor proxy 整合到 Firefox，在真正的隐私模式下启用第一方隔离、防指纹跟踪和 Tor proxy——事实上变成了 Tor Browser。该项目的<a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018Rome/Notes/FusionProject" target="_blank">最终目标</a>是让 Tor 项目能集中在研究而不是维护分支上。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 2, 23, 58, 47, 0, Beijing)),
				}, {
					title: "JavaScript 编程精解 中文第三版 翻译完成",
					link:  "https://www.solidot.org/story?sid=56707",
					desc: `<a href="mailto:admin@flygon.net">Wizard</a> 写道 "` + "\n" +
						`原书：<a href="http://eloquentjavascript.net/">Eloquent JavaScript 3rd edition</a>` + "\n" +
						`<p></p>` + "\n" +
						`译者：<a href="https://github.com/wizardforcel">飞龙</a>` + "\n" +
						`<p></p>` + "\n" +
						`自豪地采用<a href="https://translate.google.cn/">谷歌翻译</a>` + "\n" +
						`<p></p>` + "\n" +
						`部分参考了<a href="https://book.douban.com/subject/26707144/">《JavaScript 编程精解（第 2 版）》</a>` + "\n" +
						`<p></p>` + "\n" +
						`<li style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px;"><a href="https://www.gitbook.com/book/wizardforcel/eloquent-js-3e/details" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; border: none; outline: 0px; transition: color 0.3s; text-decoration-line: none; color: rgb(68, 102, 187);">在线阅读</a></li>` + "\n" +
						`<li style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px;"><a href="https://www.gitbook.com/download/pdf/book/wizardforcel/eloquent-js-3e" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; border: none; outline: 0px; transition: color 0.3s; text-decoration-line: none; color: rgb(68, 102, 187);">PDF格式</a></li>` + "\n" +
						`<li style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px;"><a href="https://www.gitbook.com/download/epub/book/wizardforcel/eloquent-js-3e" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; border: none; outline: 0px; transition: color 0.3s; text-decoration-line: none; color: rgb(68, 102, 187);">EPUB格式</a></li>` + "\n" +
						`<li style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px;"><a href="https://www.gitbook.com/download/mobi/book/wizardforcel/eloquent-js-3e" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; border: none; outline: 0px; transition: color 0.3s; text-decoration-line: none; color: rgb(68, 102, 187);">MOBI格式</a></li>` + "\n" +
						`<li style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; margin: 0px; padding: 0px;"><a href="https://github.com/wizardforcel/eloquent-js-3e-zh" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; border: none; outline: 0px; transition: color 0.3s; text-decoration-line: none; color: rgb(68, 102, 187);">代码仓库` + "\n\n" +
						`<p></p>` + "\n" +
						`<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 2, 23, 20, 35, 0, Beijing)),
				}, {
					title:   "Google 退出平板业务",
					link:    "https://www.solidot.org/story?sid=56706",
					desc:    `Google Android 网站 android.com/tablets 页面已经删除，访问该网址会跳转到主页。前一天这个网页还存在，第二天它就消失了，Google 悄悄的<a href="https://techcrunch.com/2018/06/01/google-quits-selling-tablets/" target="_blank">退出了平板业务</a>。Google 平板业务的青黄不接早已众所周知，其最后一款自有品牌的平板 Pixel C 还是在 2015 年推出的，此后一直没有后续产品。搜索巨人已经将注意力主要集中到 Chrome OS硬件上。Google 退出平板业务并不意味着 Android 平板的消失，事实上还有很多厂商制造和发售 Android 平板。去年平板出货量仍然超过 1.6 亿。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 2, 21, 58, 48, 0, Beijing)),
				}, {
					title:   "微软与 GitHub 协商收购事宜",
					link:    "https://www.solidot.org/story?sid=56705",
					desc:    `知情人士<a href="http://www.businessinsider.com/2-billion-startup-github-could-be-for-sale-microsoft-2018-5/?IR=T" target="_blank">透露</a>，微软最近与 GitHub 协商收购事宜，两家公司的对话时断时续，过去几周双方的对话开始认真起来。GitHub 是最大的源码托管平台，该公司在 2015 年以估值 20 亿美元从红杉资本融资 2.5 亿美元，而收购金额可能达到 50 亿美元甚至更高。GitHub 的经济状况良好，公司创始人兼 CEO Chris Wanstrath 已经宣布将辞职，该公司过去十个月一直在寻找继任者。微软收购 GitHub 一事引发了<a href="https://news.ycombinator.com/item?id=17208293" target="_blank">许多讨论</a>，很多人对此<a href="https://news.slashdot.org/story/18/06/01/2034202/microsoft-is-talking-about-acquiring-github-says-report" target="_blank">表示担忧</a>，因为微软并不是一个好的买家。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 2, 21, 48, 13, 0, Beijing)),
				}, {
					title: "Valve 修复了一个影响过去十年 Steam 版本的漏洞",
					link:  "https://www.solidot.org/story?sid=56704",
					desc: `Valve <a href="https://www.bleepingcomputer.com/news/security/valve-patches-security-bug-that-existed-in-steam-client-for-the-past-ten-years/" target="_blank">修复了</a>一个影响过去十年所有 Steam 版本的严重漏洞，该漏洞允许攻击者在目标计算机上执行任意代码。Steam 是最流行的 PC 数字游戏发行平台，有千万级用户。发现该漏洞的安全研究员 Tom Court 称该漏洞是一个远程代码执行漏洞，能通过发送网络请求利用，攻击者只需要发送畸形的 UDP 包到目标的 Steam 客户端，就能触发漏洞在目标的计算机上执行恶意代码。` + "\n" +
						`<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 21, 4, 27, 0, Beijing)),
				}, {
					title:   "三星无需被迫更新旧型号手机的系统",
					link:    "https://www.solidot.org/story?sid=56703",
					desc:    `在赢得荷兰法庭的诉讼之后，三星将<a href="http://www.bbc.com/news/technology-44316364" target="_blank">不需要被迫去更新</a>售出超过两年时间的智能手机系统。消费者保护组织  Consumentenbond 认为，三星至少应该在手机开始销售四年内更新软件。定期更新软件能解决安全问题，而旧型号的智能手机通常不会收到最新更新。三星的手机安装的是 Google 的 Android 系统，而 Google 会定期释出修复安全漏洞的软件更新，这些更新会提供给厂商，然后由厂商再决定提供给客户。三星声称，它保证在手机售出两年内提供软件更新。法院拒绝了该组织的要求，认为 Consumentenbond 的主张涉及“未来行为”而“不能采纳”。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 20, 49, 29, 0, Beijing)),
				}, {
					title:   "小米利润最高的是广告业务",
					link:    "https://www.solidot.org/story?sid=56702",
					desc:    `小米正准备上市，从其披露的招股书来看，对小米营收贡献最大的是智能手机业务，而基于广告的互联网服务虽然收入规模不大，但毛利率远远高于其他业务。招股书显示互联网服务 2017 年毛利率为 60.2%，2016 年为 64.4%。广告一直都是互联网服务变现的<a href="http://www.bjnews.com.cn/finance/2018/06/01/489343.html" target="_blank">主要方式</a>，不过 MIUI 系统内置广告引发了小米用户的反感。此前，有网友称自己使用的百度输入法小米版植入广告。百度手机输入法回应称，该广告是手机厂商在输入法手机定制版上的自定义行为，已经第一时间沟通该厂商下线广告。各大应用市场提供百度输入法官方版、华为定制版等其他版本的百度输入法下载，均无任何广告行为。据相关媒体报道，去年小米推出了 “小米会员” 内测服务，米粉仅需支付 9.9 元，就可以享受到一系列服务，比如专属主题、云服务空间、有品购物平台的优惠券等，还有去除广告功能。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 20, 21, 32, 0, Beijing)),
				}, {
					title:   "阿里巴巴的自主驾驶卡车装备了固态激光雷达",
					link:    "https://www.solidot.org/story?sid=56701",
					desc:    `阿里巴巴旗下的菜鸟<a href="https://spectrum.ieee.org/cars-that-think/transportation/sensors/chinas-alibaba-puts-robosenses-solidstate-lidar-in-a-selfdriving-delivery-bot" target="_blank">推出</a>了世界首个使用固态激光雷达导航的无人驾驶卡车 G Plu。固态激光雷达 RS-LiDAR-M1Pre 由深圳公司<a href="http://www.robosense.ai/" target="_blank">速腾聚创</a>（RoboSense）制造。相比目前常见的机械多线雷达，MEMS 微振镜（MEMS micro mirror）扫描方案下的固态激光雷达正在成为新趋势。采用 MEMS 微振镜扫描方案，激光发射器和接收器只需要几个，通过 MEMS 微振镜在两个方向上的摆动进行扫描，因为摆动角度非常精细所以角分辨率非常高，整个视场角内垂直角分辨达到 0.2°。相比之下，传统机械多线 LiDAR 要达到相同的效果，需要上百组激光发射器和接收器同时旋转扫描，这样会大幅提高物料成本和人力调教成本，降低良品率和可靠性。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 19, 54, 32, 0, Beijing)),
				}, {
					title:   "中国农村儿童流行肥胖症",
					link:    "https://www.solidot.org/story?sid=56700",
					desc:    `研究人员<a href="http://www.bbc.com/zhongwen/simp/science/2016/04/160427_china_rural_children_junk_food" target="_blank">警告说</a>，随着中国农村儿童摄入西式含高糖和碳水化合物的食品越来越多，农村儿童的肥胖症也日益严重。 山东的一项研究发现，2014 年，年龄在 19 岁以下的青少年中，有 17% 的男孩患肥胖症；而女孩比例为 9%。这比 1985 年的比例大幅提升，当年仅有 1% 的青少年患肥胖症。研究发表在《<a href="http://journals.sagepub.com/home/cpr#" target="_blank">欧洲预防心脏病学杂志</a>》上。相关数据来自六个对 28,000 名 7 岁至 18 岁农村儿童的官方调查。该研究发现，体重超重的男孩从 0.7% 升至 16.4%，超重的女孩从 1.5% 升至近 14%。山东省疾病预防控制中心张迎修表示，“过去３０年来，中国在社会经济和营养方面都发生了巨大改变”。他说，与过去相比，中国人现在吃得更多，更少锻炼身体。传统中国饮食习惯已发生转变，人们摄入含更高脂肪和热量，纤维含量低的食品”。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 19, 39, 34, 0, Beijing)),
				}, {
					title:   "腾讯和阿里巴巴竞争主导权",
					link:    "https://www.solidot.org/story?sid=56699",
					desc:    `科技界对主导权最激烈的争夺<a href="https://cn.nytimes.com/technology/20180601/china-tencent-alibaba/" target="_blank">正在中国展开</a>。它也可能预示着其他地方科技巨头的未来。腾讯控股和阿里巴巴集团正在升级一场没有限制的竞争，主导 7.7 亿网民交流、购物、出行、娱乐，甚至投资储蓄、看病的方式。从很久以前起，这两大巨头的争斗就已经从自己的核心业务——腾讯是游戏和社交媒体，阿里巴巴是电子商务——扩展到了中国人生活的其他领域。他们在短信、微博和外卖领域展开了竞争。它们在视频流媒体和云计算领域针锋相对。如此广泛的双头垄断在美国很难轻松复制。根深蒂固的竞争者，加上政府干预的威胁，通常会导致苹果、亚马逊、谷歌和 Facebook 等公司无法仓促地扩展到相邻的业务领域。中国的互联网巨头拥有独一无二的强大盟友：中国政府。北京盈科律师事务所的合伙人胡文友表示，腾讯和阿里巴巴在北京庇护之下避免了反垄断的限制。它们庞大的规模有利于当局进行控制。它们有太多利益在里面了。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 17, 55, 6, 0, Beijing)),
				}, {
					title:   "谷歌在中国的应用商店推出应用",
					link:    "https://www.solidot.org/story?sid=56698",
					desc:    `谷歌在中国<a href="http://blog.sina.com.cn/s/blog_9c079b040102z5ky.html" target="_blank">发布了</a>一款应用叫“<span>Google 文件极客</span>”，而该应用不是通过 Google Play 而是通过<a href="https://shouji.baidu.com/software/24053020.html" target="_blank">百度</a>、华为、腾讯和小米应用商店提供给中国用户。根据百度提供的数据，该应用在百度应用市场的下载量不到八千。谷歌官方博客称，“人们需要有一种更好的方式，来管理手机里的各种文件，因此，我们在去年 12 月发布了 Files Go 这款应用。今天，我们很高兴的发布了这款应用针对中国的全新定制版本——Google 文件极客。Google 文件极客将帮你清理空间，更快地找到文件，并更方便的与他人分享文件，应用安装包大小仅为 8.4MB。”<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 17, 9, 9, 0, Beijing)),
				}, {
					title:   "Mozilla 邀请用户测试 DNS over HTTPS",
					link:    "https://www.solidot.org/story?sid=56697",
					desc:    `Mozilla 开始<a href="https://blog.nightly.mozilla.org/2018/06/01/improving-dns-privacy-in-firefox/" target="_blank">邀请</a> Nightly 版用户<a href="https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/" target="_blank">测试</a>更安全的域名解析方法：Trusted Recursive Resolver (TRR)，通过 HTTPs 加密发出 DNS 请求。今天的 DNS 请求是通过 UDP 或 TCP 协议，没有加密，也不安全。采用 DNS over HTTPs (DoH) 将让 DNS 解析更安全。TRR 提供了方法选择信任的 DNS 解析服务器去处理请求。Mozilla 考虑未来对所有用户启用这项功能。计划于 9 月释出的 Firefox` + "\u00a0" + ` 62 将提供该功能，现在 Firefox` + "\u00a0" + ` 62 已经发布了 Nightly 版本，用户需要通过设置 about:config 搜索 network.trr，将 network.trr.mode 的值设为 2 来启用 DoH，通过 network.trr.uri 来设置 DoH 服务器，目前可选择 Cloudflare 的服务器 https://mozilla.cloudflare-dns.com/dns-query。查看 about:networking 页的 DNS 标签可以了解你使用的域名解析服务器。<p><img src="https://img.solidot.org/0/446/liiLIZF8Uh6yM.jpg" height="120" style="display:block"/></p>`,
					pubDate: RFC822(time.Date(2018, 6, 1, 16, 10, 4, 0, Beijing)),
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range ch.Items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal \n%q: \n%q", target, ch.Items[i].Description)
				}

				if ch.Items[i].Author != "" {
					t.Errorf("must empty: %q", ch.Items[i].Author)
				}

				if ch.Items[i].Categories != nil {
					t.Errorf("must nil: %q", ch.Items[i].Categories)
				}

				if ch.Items[i].Comments != "" {
					t.Errorf("must empty: %q", ch.Items[i].Comments)
				}

				if ch.Items[i].Enclosure != nil {
					t.Errorf("must nil: %q", ch.Items[i].Enclosure)
				}

				if ch.Items[i].GUID != nil {
					t.Errorf("must nil: %q", ch.Items[i].GUID)
				}

				if target := items[i].pubDate; !ch.Items[i].PubDate.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].PubDate)
				}

				if ch.Items[i].Source != nil {
					t.Errorf("must nil: %q", ch.Items[i].Source)
				}
			}
		}
	})
}

func TestFeedFromURL(t *testing.T) {
	var ru RSSUtil
	var err error
	var source string

	source = "https://cyber.harvard.edu/rss/examples/sampleRss091.xml"
	t.Run(source, func(t *testing.T) {
		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "0.91"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "WriteTheWeb"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "http://writetheweb.com"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "News for web users that write back"; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "en-us"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if target := "Copyright 2000, WriteTheWeb team."; ch.Copyright != target {
			t.Errorf("must equal %q: %q", target, ch.Copyright)
		}

		if target := "editor@writetheweb.com"; ch.ManagingEditor != target {
			t.Errorf("must equal %q: %q", target, ch.ManagingEditor)
		}

		if target := "webmaster@writetheweb.com"; ch.WebMaster != target {
			t.Errorf("must equal %q: %q", target, ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if ch.LastBuildDate != nil {
			t.Errorf("must nil: %q", ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if ch.Docs != "" {
			t.Errorf("must empty: %q", ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image == nil {
			t.Errorf("must not nil: %q", ch.Image)
		} else {
			img := RSSImage{
				URL:         "http://writetheweb.com/images/mynetscape88.gif",
				Title:       "WriteTheWeb",
				Link:        "http://writetheweb.com",
				Width:       88,
				Height:      31,
				Description: "News for web users that write back",
			}

			if target := img; !ch.Image.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Image)
			}

			if target := img.URL; ch.Image.URL != target {
				t.Errorf("must equal %q: %q", target, ch.Image.URL)
			}

			if target := img.Title; ch.Image.Title != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Title)
			}

			if target := img.Link; ch.Image.Link != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Link)
			}

			if target := img.Width; ch.Image.Width != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Width)
			}

			if target := img.Height; ch.Image.Height != target {
				t.Errorf("must equal %d: %d", target, ch.Image.Height)
			}

			if target := img.Description; ch.Image.Description != target {
				t.Errorf("must equal %q: %q", target, ch.Image.Description)
			}
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 6; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title string
				link  string
				desc  string
			}{
				{
					title: "Giving the world a pluggable Gnutella",
					link:  "http://writetheweb.com/read.php?item=24",
					desc:  "WorldOS is a framework on which to build programs that work like Freenet or Gnutella -allowing distributed applications using peer-to-peer routing.",
				}, {
					title: "Syndication discussions hot up",
					link:  "http://writetheweb.com/read.php?item=23",
					desc:  "After a period of dormancy, the Syndication mailing list has become active again, with contributions from leaders in traditional media and Web syndication.",
				}, {
					title: "Personal web server integrates file sharing and messaging",
					link:  "http://writetheweb.com/read.php?item=22",
					desc:  "The Magi Project is an innovative project to create a combined personal web server and messaging system that enables the sharing and synchronization of information across desktop, laptop and palmtop devices.",
				}, {
					title: "Syndication and Metadata",
					link:  "http://writetheweb.com/read.php?item=21",
					desc:  "RSS is probably the best known metadata format around. RDF is probably one of the least understood. In this essay, published on my O'Reilly Network weblog, I argue that the next generation of RSS should be based on RDF.",
				}, {
					title: "UK bloggers get organised",
					link:  "http://writetheweb.com/read.php?item=20",
					desc:  "Looks like the weblogs scene is gathering pace beyond the shores of the US. There's now a UK-specific page on weblogs.com, and a mailing list at egroups.",
				}, {
					title: "Yournamehere.com more important than anything",
					link:  "http://writetheweb.com/read.php?item=19",
					desc:  "Whatever you're publishing on the web, your site name is the most valuable asset you have, according to Carl Steadman.",
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range ch.Items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}
			}
		}
	})

	source = "https://cyber.harvard.edu/rss/examples/sampleRss092.xml"
	t.Run(source, func(t *testing.T) {
		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "0.92"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Dave Winer: Grateful Dead"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "http://www.scripting.com/blog/categories/gratefulDead.html"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "A high-fidelity Grateful Dead song every day. This is" +
			" where we're experimenting with enclosures on RSS news items" +
			" that download when you're not using your computer. If it works" +
			" (it will) it will be the end of the Click-And-Wait multimedia" +
			" experience on the Internet."; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if ch.Language != "" {
			t.Errorf("must empty: %q", ch.Language)
		}

		if ch.Copyright != "" {
			t.Errorf("must empty: %q", ch.Copyright)
		}

		if target := "dave@userland.com (Dave Winer)"; ch.ManagingEditor != target {
			t.Errorf("must equal %q: %q", target, ch.ManagingEditor)
		}

		if target := "dave@userland.com (Dave Winer)"; ch.WebMaster != target {
			t.Errorf("must equal %q: %q", target, ch.WebMaster)
		}

		if ch.PubDate != nil {
			t.Errorf("must nil: %q", ch.PubDate)
		}

		if target := RFC822(
			time.Date(2001, 4, 13, 19, 23, 2, 0, time.UTC),
		); !ch.LastBuildDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if ch.Generator != "" {
			t.Errorf("must empty: %q", ch.Generator)
		}

		if target := "http://backend.userland.com/rss092"; ch.Docs != target {
			t.Errorf("must equal %q: %q", target, ch.Docs)
		}

		if ch.Cloud == nil {
			t.Errorf("must not nil: %q", ch.Cloud)
		} else {
			cloud := RSSCloud{
				Domain:            "data.ourfavoritesongs.com",
				Port:              80,
				Path:              "/RPC2",
				RegisterProcedure: "ourFavoriteSongs.rssPleaseNotify",
				Protocol:          "xml-rpc",
			}

			if target := cloud; !ch.Cloud.Equal(target) {
				t.Errorf("must equal %q: %q", target, ch.Cloud)
			}

			if target := cloud.Domain; ch.Cloud.Domain != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.Domain)
			}

			if target := cloud.Port; ch.Cloud.Port != target {
				t.Errorf("must equal %d: %d", target, ch.Cloud.Port)
			}

			if target := cloud.Path; ch.Cloud.Path != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.Path)
			}

			if target := cloud.RegisterProcedure; ch.Cloud.RegisterProcedure != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.RegisterProcedure)
			}

			if target := cloud.Protocol; ch.Cloud.Protocol != target {
				t.Errorf("must equal %q: %q", target, ch.Cloud.Protocol)
			}
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image != nil {
			t.Errorf("must nil: %q", ch.Image)
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 22; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []RSSItem{
				{
					Description: "It's been a few days since I added a song to the Grateful Dead channel. Now that there are all these new Radio users, many of whom are tuned into this channel (it's #16 on the hotlist of upstreaming Radio users, there's no way of knowing how many non-upstreaming users are subscribing, have to do something about this..). Anyway, tonight's song is a live version of Weather Report Suite from Dick's Picks Volume 7. It's wistful music. Of course a beautiful song, oft-quoted here on Scripting News. <i>A little change, the wind and rain.</i>",
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/weatherReportDicksPicsVol7.mp3",
						Length: 6182912,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Kevin Drennan started a <a href="http://deadend.editthispage.com/">Grateful Dead Weblog</a>. Hey it's cool, he even has a <a href="http://deadend.editthispage.com/directory/61">directory</a>. <i>A Frontier 7 feature.</i>`,
					Source: &RSSSource{
						Source: "Scripting News",
						URL:    "http://scriptingnews.userland.com/xml/scriptingNews2.xml",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/GDead/AGDL/other1.html">The Other One</a>, live instrumental, One From The Vault. Very rhythmic very spacy, you can listen to it many times, and enjoy something new every time.`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/theOtherOne.mp3",
						Length: 6666097,
						Type:   "audio/mpeg",
					},
				}, {
					Description: "This is a test of a change I just made. Still diggin..",
				}, {
					Description: `The HTML rendering almost <a href="http://validator.w3.org/check/referer">validates</a>. Close. Hey I wonder if anyone has ever published a style guide for ALT attributes on images? What are you supposed to say in the ALT attribute? I sure don't know. If you're blind send me an email if u cn rd ths.`,
				}, {
					Description: `<a href="http://www.cs.cmu.edu/~mleone/gdead/dead-lyrics/Franklin's_Tower.txt">Franklin's Tower</a>, a live version from One From The Vault.`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/franklinsTower.mp3",
						Length: 6701402,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Moshe Weitzman says Shakedown Street is what I'm lookin for for tonight. I'm listening right now. It's one of my favorites. "Don't tell me this town ain't got no heart." Too bright. I like the jazziness of Weather Report Suite. Dreamy and soft. How about The Other One? "Spanish lady come to me.."`,
					Source: &RSSSource{
						Source: "Scripting News",
						URL:    "http://scriptingnews.userland.com/xml/scriptingNews2.xml",
					},
				}, {
					Description: `<a href="http://www.scripting.com/mp3s/youWinAgain.mp3">The news is out</a>, all over town..<p>` + "\n" +
						`You've been seen, out runnin round. <p>` + "\n" +
						`The lyrics are <a href="http://www.cs.cmu.edu/~mleone/gdead/dead-lyrics/You_Win_Again.txt">here</a>, short and sweet. <p>` + "\n" +
						`<i>You win again!</i>`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/youWinAgain.mp3",
						Length: 3874816,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://www.getlyrics.com/lyrics/grateful-dead/wake-of-the-flood/07.htm">Weather Report Suite</a>: "Winter rain, now tell me why, summers fade, and roses die? The answer came. The wind and rain. Golden hills, now veiled in grey, summer leaves have blown away. Now what remains? The wind and rain."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/weatherReportSuite.mp3",
						Length: 12216320,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/gdead/agdl/darkstar.html">Dark Star</a> crashes, pouring its light into ashes.`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/darkStar.mp3",
						Length: 10889216,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `DaveNet: <a href="http://davenet.userland.com/2001/01/21/theUsBlues">The U.S. Blues</a>.`,
				}, {
					Description: `Still listening to the US Blues. <i>"Wave that flag, wave it wide and high.."</i> Mistake made in the 60s. We gave our country to the assholes. Ah ah. Let's take it back. Hey I'm still a hippie. <i>"You could call this song The United States Blues."</i>`,
				}, {
					Description: `<a href="http://www.sixties.com/html/garcia_stack_0.html"><img src="http://www.scripting.com/images/captainTripsSmall.gif" height="51" width="42" border="0" hspace="10" vspace="10" align="right"></a>In celebration of today's inauguration, after hearing all those great patriotic songs, America the Beautiful, even The Star Spangled Banner made my eyes mist up. It made my choice of Grateful Dead song of the night realllly easy. Here are the <a href="http://searchlyrics2.homestead.com/gd_usblues.html">lyrics</a>. Click on the audio icon to the left to give it a listen. "Red and white, blue suede shoes, I'm Uncle Sam, how do you do?" It's a different kind of patriotic music, but man I love my country and I love Jerry and the band. <i>I truly do!</i>`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/usBlues.mp3",
						Length: 5272510,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Grateful Dead: "Tennessee, Tennessee, ain't no place I'd rather be."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/tennesseeJed.mp3",
						Length: 3442648,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Ed Cone: "Had a nice Deadhead experience with my wife, who never was one but gets the vibe and knows and likes a lot of the music. Somehow she made it to the age of 40 without ever hearing Wharf Rat. We drove to Jersey and back over Christmas with the live album commonly known as Skull and Roses in the CD player much of the way, and it was cool to see her discover one the band's finest moments. That song is unique and underappreciated. Fun to hear that disc again after a few years off -- you get Jerry as blues-guitar hero on Big Railroad Blues and a nice version of Bertha."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/darkStarWharfRat.mp3",
						Length: 27503386,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/GDead/AGDL/fotd.html">Tonight's Song</a>: "If I get home before daylight I just might get some sleep tonight."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/friendOfTheDevil.mp3",
						Length: 3219742,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://arts.ucsc.edu/GDead/AGDL/uncle.html">Tonight's song</a>: "Come hear Uncle John's Band by the river side. Got some things to talk about here beside the rising tide."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/uncleJohnsBand.mp3",
						Length: 4587102,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://www.cs.cmu.edu/~mleone/gdead/dead-lyrics/Me_and_My_Uncle.txt">Me and My Uncle</a>: "I loved my uncle, God rest his soul, taught me good, Lord, taught me all I know. Taught me so well, I grabbed that gold and I left his dead ass there by the side of the road."`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/meAndMyUncle.mp3",
						Length: 2949248,
						Type:   "audio/mpeg",
					},
				}, {
					Description: "Truckin, like the doo-dah man, once told me gotta play your hand. Sometimes the cards ain't worth a dime, if you don't lay em down.",
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/truckin.mp3",
						Length: 4847908,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `Two-Way-Web: <a href="http://www.thetwowayweb.com/payloadsForRss">Payloads for RSS</a>. "When I started talking with Adam late last year, he wanted me to think about high quality video on the Internet, and I totally didn't want to hear about it."`,
				}, {
					Description: "A touch of gray, kinda suits you anyway..",
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/touchOfGrey.mp3",
						Length: 5588242,
						Type:   "audio/mpeg",
					},
				}, {
					Description: `<a href="http://www.sixties.com/html/garcia_stack_0.html"><img src="http://www.scripting.com/images/captainTripsSmall.gif" height="51" width="42" border="0" hspace="10" vspace="10" align="right"></a>In celebration of today's inauguration, after hearing all those great patriotic songs, America the Beautiful, even The Star Spangled Banner made my eyes mist up. It made my choice of Grateful Dead song of the night realllly easy. Here are the <a href="http://searchlyrics2.homestead.com/gd_usblues.html">lyrics</a>. Click on the audio icon to the left to give it a listen. "Red and white, blue suede shoes, I'm Uncle Sam, how do you do?" It's a different kind of patriotic music, but man I love my country and I love Jerry and the band. <i>I truly do!</i>`,
					Enclosure: &RSSEnclosure{
						URL:    "http://www.scripting.com/mp3s/usBlues.mp3",
						Length: 5272510,
						Type:   "audio/mpeg",
					},
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i]; !ch.Items[i].Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i])
				}
			}
		}
	})

	source = "https://cyber.harvard.edu/rss/examples/rss2sample.xml"
	t.Run(source, func(t *testing.T) {
		err = ru.Feed(source)
		if err != nil {
			t.Fatal(err)
		}

		if ru.source != source {
			t.Errorf("must equal %q: %q", source, ru.source)
		}

		rss := ru.rss // a shortcut

		if target := "2.0"; rss.Version != target {
			t.Errorf("must equal %q: %q", target, rss.Version)
		}

		ch := ru.rss.Channel // a shortcut

		if target := "Liftoff News"; ch.Title != target {
			t.Errorf("must equal %q: %q", target, ch.Title)
		}

		if target := "http://liftoff.msfc.nasa.gov/"; ch.Link != target {
			t.Errorf("must equal %q: %q", target, ch.Link)
		}

		if target := "Liftoff to Space Exploration."; ch.Description != target {
			t.Errorf("must equal %q: %q", target, ch.Description)
		}

		if target := "en-us"; ch.Language != target {
			t.Errorf("must equal %q: %q", target, ch.Language)
		}

		if ch.Copyright != "" {
			t.Errorf("must empty: %q", ch.Copyright)
		}

		if target := "editor@example.com"; ch.ManagingEditor != target {
			t.Errorf("must equal %q: %q", target, ch.ManagingEditor)
		}

		if target := "webmaster@example.com"; ch.WebMaster != target {
			t.Errorf("must equal %q: %q", target, ch.WebMaster)
		}

		if target := RFC822(time.Date(
			2003, 6, 10, 4, 0, 0, 0, time.UTC,
		)); !ch.PubDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.PubDate)
		}

		if target := RFC822(time.Date(
			2003, 6, 10, 9, 41, 1, 0, time.UTC,
		)); !ch.LastBuildDate.Equal(target) {
			t.Errorf("must equal %q: %q", target, ch.LastBuildDate)
		}

		if ch.Categories != nil {
			t.Errorf("must nil: %q", ch.Categories)
		}

		if target := "Weblog Editor 2.0"; ch.Generator != target {
			t.Errorf("must equal %q: %q", target, ch.Generator)
		}

		if target := "http://blogs.law.harvard.edu/tech/rss"; ch.Docs != target {
			t.Errorf("must equal %q: %q", target, ch.Docs)
		}

		if ch.Cloud != nil {
			t.Errorf("must nil: %q", ch.Cloud)
		}

		if target := 0; ch.TTL != target {
			t.Errorf("must equal %d: %d", target, ch.TTL)
		}

		if ch.Image != nil {
			t.Errorf("must nil: %q", ch.Image)
		}

		if ch.Rating != "" {
			t.Errorf("must empty: %q", ch.Rating)
		}

		if ch.TextInput != nil {
			t.Errorf("must nil: %q", ch.TextInput)
		}

		if ch.SkipHours != nil {
			t.Errorf("must nil: %q", ch.SkipHours)
		}

		if ch.SkipDays != nil {
			t.Errorf("must nil: %q", ch.SkipDays)
		}

		if target := 4; len(ch.Items) != target {
			t.Errorf("must equal %d: %d", target, len(ch.Items))
		} else {
			items := []struct {
				title   string
				link    string
				desc    string
				pubDate RFC822
				guid    RSSGUID
			}{
				{
					title:   "Star City",
					link:    "http://liftoff.msfc.nasa.gov/news/2003/news-starcity.asp",
					desc:    `How do Americans get ready to work with Russians aboard the International Space Station? They take a crash course in culture, language and protocol at Russia's <a href="http://howe.iki.rssi.ru/GCTC/gctc_e.htm">Star City</a>.`,
					pubDate: RFC822(time.Date(2003, 6, 3, 9, 39, 21, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/06/03.html#item573"},
				}, {
					desc:    `Sky watchers in Europe, Asia, and parts of Alaska and Canada will experience a <a href="http://science.nasa.gov/headlines/y2003/30may_solareclipse.htm">partial eclipse of the Sun</a> on Saturday, May 31st.`,
					pubDate: RFC822(time.Date(2003, 5, 30, 11, 6, 42, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/05/30.html#item572"},
				}, {
					title:   "The Engine That Does More",
					link:    "http://liftoff.msfc.nasa.gov/news/2003/news-VASIMR.asp",
					desc:    "Before man travels to Mars, NASA hopes to design new engines that will let us fly through the Solar System more quickly.  The proposed VASIMR engine would do that.",
					pubDate: RFC822(time.Date(2003, 5, 27, 8, 37, 32, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/05/27.html#item571"},
				}, {
					title:   "Astronauts' Dirty Laundry",
					link:    "http://liftoff.msfc.nasa.gov/news/2003/news-laundry.asp",
					desc:    "Compared to earlier spacecraft, the International Space Station has many luxuries, but laundry facilities are not one of them.  Instead, astronauts have other options.",
					pubDate: RFC822(time.Date(2003, 5, 20, 8, 56, 2, 0, time.UTC)),
					guid:    RSSGUID{GUID: "http://liftoff.msfc.nasa.gov/2003/05/20.html#item570"},
				},
			}

			if len(items) != target {
				t.Errorf("must equal %d: %d", target, len(items))
			}

			for i := range items {
				if target := items[i].title; ch.Items[i].Title != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Title)
				}

				if target := items[i].link; ch.Items[i].Link != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Link)
				}

				if target := items[i].desc; ch.Items[i].Description != target {
					t.Errorf("must equal %q: %q", target, ch.Items[i].Description)
				}

				if ch.Items[i].Author != "" {
					t.Errorf("must empty: %q", ch.Items[i].Author)
				}

				if ch.Items[i].Categories != nil {
					t.Errorf("must nil: %q", ch.Items[i].Categories)
				}

				if ch.Items[i].Comments != "" {
					t.Errorf("must empty: %q", ch.Items[i].Comments)
				}

				if ch.Items[i].Enclosure != nil {
					t.Errorf("must nil: %q", ch.Items[i].Enclosure)
				}

				if target := items[i].guid; !ch.Items[i].GUID.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].GUID)
				}

				if target := items[i].pubDate; !ch.Items[i].PubDate.Equal(target) {
					t.Errorf("must equal %q: %q", target, ch.Items[i].PubDate)
				}

				if ch.Items[i].Source != nil {
					t.Errorf("must nil: %q", ch.Items[i].Source)
				}
			}
		}
	})
}

func TestServe(t *testing.T) {
	const source = "sample_rss/sampleRss091.rss"

	log.SetFlags(log.Lerror) // disable logger output

	t.Run("TestUserCreatedServe", func(t *testing.T) {
		var isStop bool
		var ru RSSUtil
		ru.source = source

		go func() {
			ru.Serve(0)
			isStop = true
		}()

		time.Sleep(100 * time.Millisecond)

		ru.Stop()

		if isStop {
			t.Error("must stopped")
		}
	})

	t.Run("TestDefaultServe", func(t *testing.T) {
		var isStop bool

		go func() {
			Serve(source, 0, nil, nil, nil)
			isStop = true
		}()

		time.Sleep(100 * time.Millisecond)

		Stop()

		if isStop {
			t.Error("must stopped")
		}
	})
}

// vim: ts=4 sts=4 sw=4 noet

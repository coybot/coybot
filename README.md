CoyBot
======

Coy, The Bot! == Coy's Toy.

**CAUTION:** The behavior is in change frequently, in current. You're noticed.

## Install From Binary Release

TODO

## Install From Source Code

1. Build from source code

```sh
go get gitlab.com/coybot/coybot
sudo install -oroot -groot -m755 -t /usr/local/bin/ $(go env GOPATH)/bin/coybot
```

2. Set up environment

```sh
sudo groupadd -r coybot
sudo useradd -g coybot -M -r -s /usr/sbin/nologin coybot
sudo install -ocoybot -gcoybot -m770 -d /usr/local/etc/coybot/
sudo install -ocoybot -gcoybot -m660 -t /usr/local/etc/coybot/config.yaml $(go env GOPATH)/src/gitlab.com/coybot/coybot/config.sample.yaml
```

## License

CoyBot is distributed under GPLv3 found in the [LICENSE](./LICENSE) file.

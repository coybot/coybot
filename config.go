// vim: ts=4 sts=4 sw=4 noet

// Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
// All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
	"time"

	"gitlab.com/coybot/coybot/log"

	yaml "gopkg.in/yaml.v2"
)

var config Config
var reWhiteSpaces = regexp.MustCompile(`\s+`)

func loadConfig(filename string) {
	var data []byte
	var err error
	if !fileExists(filename) {
		log.Fatalf("Config file not found: %s", filename)
	}
	if data, err = ioutil.ReadFile(filename); err != nil {
		log.Fatal(err)
	}
	if err = yaml.Unmarshal(data, &config); err != nil {
		log.Fatal(err)
	}
	if config.Matrix != nil {
		config.Matrix.AccessToken = reWhiteSpaces.ReplaceAllString(config.Matrix.AccessToken, "")
	}
}

// validateConfig validates coybot configuration. This function returns error
// and/or warning configurations in a string list.
func validateConfig() (reasons []string, ok bool) {
	var reasons1, reasons2, reasons3 []string
	var ok1, ok2, ok3 bool

	ok = true

	reasons1, ok1 = validateRSSConfig()
	reasons = append(reasons, reasons1...)
	ok = ok && ok1

	reasons2, ok2 = validateMatrixConfig()
	reasons = append(reasons, reasons2...)
	ok = ok && ok2

	reasons3, ok3 = validateTelegramConfig()
	reasons = append(reasons, reasons3...)
	ok = ok && ok3

	return reasons, ok
}

func validateRSSConfig() (reasons []string, ok bool) {
	ok = true
	rss := config.RSS
	if rss.Name == "" {
		reasons = append(reasons, "ERROR: Empty 'RSS > Name'.")
		ok = false
	}
	if rss.Source == "" {
		reasons = append(reasons, "ERROR: Empty 'RSS > Source'.")
		ok = false
	}
	if rss.TTL < time.Duration(time.Minute*20) {
		reasons = append(reasons, "WARNING: 'RSS > TTL' is less than 20 minutes.")
	}
	return reasons, ok
}

func validateMatrixConfig() (reasons []string, ok bool) {
	ok = true
	matrix := config.Matrix
	if matrix == nil {
		reasons = append(reasons, "WARNING: empty matrix config")
		return
	}
	if matrix.Name == "" {
		reasons = append(reasons, "ERROR: Empty 'Matrix > Name'.")
		ok = false
	}
	if matrix.HomeServer == "" {
		reasons = append(reasons, "ERROR: Empty 'Matrix > Home Server'.")
		ok = false
	}
	if matrix.AccessToken == "" {
		reasons = append(reasons, "ERROR: Empty 'Matrix > Access Token'.")
		ok = false
	}
	if matrix.RoomID == "" {
		reasons = append(reasons, "ERROR: Empty 'Matrix > Room ID'.")
		ok = false
	}
	if !strings.HasPrefix(matrix.RoomID, "!") {
		reasons = append(reasons, "ERROR: Wrong 'Matrix > Room ID' which should in form '!aabbcc@example.com'.")
		ok = false
	}
	if matrix.Format == "" {
		reasons = append(reasons, "ERROR: Empty 'Matrix > Format'.")
		ok = false
	}
	if matrix.Format != "text" && matrix.Format != "html" && matrix.Format != "markdown" {
		reasons = append(reasons, "ERROR: Bad 'Matrix > Format' which must 'text', 'html' or 'markdown'.")
		ok = false
	}
	if matrix.Template == "" {
		reasons = append(reasons, "ERROR: Empty 'Matrix > Template'.")
		ok = false
	}
	return reasons, ok
}

func validateTelegramConfig() (reasons []string, ok bool) {
	ok = true
	telegram := config.Telegram
	if telegram == nil {
		reasons = append(reasons, "WARNING: empty telegram config")
		return
	}
	if telegram.Name == "" {
		reasons = append(reasons, "ERROR: Empty 'Telegram > Name'.")
		ok = false
	}
	if telegram.Token == "" {
		reasons = append(reasons, "ERROR: Empty 'Telegram > Token'.")
		ok = false
	}
	if telegram.ChatID == 0 {
		reasons = append(reasons, "ERROR: Empty 'Telegram > Chat ID'.")
		ok = false
	}
	if telegram.Format == "" {
		reasons = append(reasons, "ERROR: Empty 'Telegram > Format'.")
		ok = false
	}
	if telegram.Format != "text" && telegram.Format != "html" && telegram.Format != "markdown" {
		reasons = append(reasons, "ERROR: Bad 'Telegram > Format' which must 'text', 'html' or 'markdown'.")
		ok = false
	}
	if telegram.Template == "" {
		reasons = append(reasons, "ERROR: Empty 'Telegram > Template'.")
		ok = false
	}
	return reasons, ok
}

// Config represents the configuration.
type Config struct {
	configFile string
	RSS        struct {
		Name   string        `yaml:"Name"   json:"name"`
		Source string        `yaml:"Source" json:"source"`
		TTL    time.Duration `yaml:"TTL"    json:"ttl"`
	} `yaml:"RSS" json:"rss"`
	Matrix *struct {
		Name        string `yaml:"Name"         json:"name"`
		HomeServer  string `yaml:"Home Server"  json:"home_server"`
		AccessToken string `yaml:"Access Token" json:"access_token"`
		DeviceID    string `yaml:"Device ID"    json:"device_id"`
		RoomID      string `yaml:"Room ID"      json:"room_id"`
		Format      string `yaml:"Format"       json:"format"`
		Template    string `yaml:"Template"     json:"template"`
	} `yaml:"Matrix" json:"matrix"`
	Telegram *struct {
		Name     string `yaml:"Name"     json:"name"`
		Token    string `yaml:"Token"    json:"token"`
		ChatID   int64  `yaml:"Chat ID"  json:"chat_id"`
		Format   string `yaml:"Format"   json:"format"`
		Template string `yaml:"Template" json:"template"`
	} `yaml:"Telegram" json:"telegram"`
}

func (c Config) String() string {
	c.Matrix.AccessToken = c.Matrix.AccessToken[:20] + "..."
	b, _ := json.Marshal(c)
	return fmt.Sprintf("%s", b)
}

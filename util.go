// vim: ts=4 sts=4 sw=4 noet

// Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
// All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package main

import "os"

func fileExists(filename string) bool {
	_, err := os.Stat(filename)
	return !os.IsNotExist(err)
}

// vim: ts=4 sts=4 sw=4 noet

// Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
// All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package matrixapi

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/coybot/coybot/log"
)

type RestVerb string

const (
	RestVerbGet     RestVerb = "GET"
	RestVerbPost    RestVerb = "POST"
	RestVerbPut     RestVerb = "PUT"
	RestVerbPatch   RestVerb = "PATCH"
	RestVerbDelete  RestVerb = "DELETE"
	RestVerbHead    RestVerb = "HEAD"
	RestVerbOptions RestVerb = "OPTIONS"
)

type RestResult struct {
	Status     string
	StatusCode int
	Content    []byte
}

var restToken chan struct{}

func init() {
	restToken = make(chan struct{}, 5) // Limit requests in 5 concurent.
}

func rest(host string, verb RestVerb, uri, token string, data interface{}) (*RestResult, error) {
	if !strings.HasPrefix(host, "http") {
		host = "https://" + host
	}
	url, err := url.Parse(host)
	if err != nil {
		return nil, err
	}
	url.Path = uri

	var reader io.Reader
	if data != nil {
		var b []byte
		b, err = json.Marshal(data)
		if err != nil {
			return nil, err
		}

		reader = bytes.NewReader(b)

		log.Debugf("rest(%q, %q, %q)", verb, url, b)
	} else {
		log.Debugf("rest(%q, %q)", verb, url)
	}

	req, err := http.NewRequest(string(verb), url.String(), reader)
	if err != nil {
		return nil, err
	}

	// TODO: proxy support

	req.Header.Set("User-Agent", "matrixapi (https://gitlab.com/coy/matrixapi)")
	req.Header.Add("Content-Type", "application/json")
	if token != "" {
		req.Header.Set("Authorization", "Bearer "+token)
	}

	var resp *http.Response
	var content []byte

	client := &http.Client{}

	restToken <- struct{}{}        // acquire a token
	defer func() { <-restToken }() // release the token

	resp, err = client.Do(req)
	if err != nil {
		return nil, err
	}

	content, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return &RestResult{resp.Status, resp.StatusCode, content}, nil
}

func restGet(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbGet, uri, token, data)
}

func restPost(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbPost, uri, token, data)
}

func restPut(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbPut, uri, token, data)
}

func restPatch(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbPatch, uri, token, data)
}

func restDelete(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbDelete, uri, token, data)
}

func restHead(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbHead, uri, token, data)
}

func restOptions(host, uri, token string, data interface{}) (*RestResult, error) {
	return rest(host, RestVerbOptions, uri, token, data)
}

#!/bin/sh
# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

set -e

install_checking_tools(){
    go get github.com/golang/lint/golint    \
           github.com/fzipp/gocyclo         \
           github.com/client9/misspell/...  \
           github.com/gordonklaus/ineffassign
}

install_checking_scripts() {
    WORKDIR="$(dirname $0)"
    DOT_GIT="$WORKDIR/../.git"

    for fname in check_email.sh             \
                 check_gpgsign.sh           \
                 check_golang.sh            \
                 init_colors.inc.sh         \
                 pre-commit
    do
        chmod +x "$WORKDIR/$fname"  # force script to be executable
        ln -sfv "../../git_hooks/$fname" "$DOT_GIT/hooks/$fname"
    done
}

install_checking_tools
install_checking_scripts

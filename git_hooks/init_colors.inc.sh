# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

if [ "$GITLAB_CI" = "true" ] || [ -t 2 ] # stderr is a tty
then
    # Foregrounds
    BLACK=$(  echo -e "\033[30m")
    RED=$(    echo -e "\033[31m")
    GREEN=$(  echo -e "\033[32m")
    YELLOW=$( echo -e "\033[33m")
    BLUE=$(   echo -e "\033[34m")
    MAGENTA=$(echo -e "\033[35m")
    CYAN=$(   echo -e "\033[36m")
    WHITE=$(  echo -e "\033[37m")
    BRIGHT_BLACK=$(  echo -e "\033[90m")
    BRIGHT_RED=$(    echo -e "\033[91m")
    BRIGHT_GREEN=$(  echo -e "\033[92m")
    BRIGHT_YELLOW=$( echo -e "\033[93m")
    BRIGHT_BLUE=$(   echo -e "\033[94m")
    BRIGHT_MAGENTA=$(echo -e "\033[95m")
    BRIGHT_CYAN=$(   echo -e "\033[96m")
    BRIGHT_WHITE=$(  echo -e "\033[97m")

    # Controls
    BOLD=$(   echo -e "\033[1m")
    REVERSE=$(echo -e "\033[7m")
    RESET=$(  echo -e "\033[0m")
fi

#!/bin/sh
# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

set -e

uninstall_checking_scripts() {
    DOT_GIT="$(dirname $0)/../.git"

    for fname in check_email.sh             \
                 check_gpgsign.sh           \
                 check_golang.sh            \
                 init_colors.inc.sh         \
                 pre-commit
    do
        rm -v "$DOT_GIT/hooks/$fname"
    done
}

uninstall_checking_scripts

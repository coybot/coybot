// vim: ts=4 sts=4 sw=4 noet

// Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
// All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"regexp"
	"sort"
	"strings"
	"syscall"
	"time"

	"gitlab.com/coybot/coybot/log"
	matrixapi "gitlab.com/coybot/coybot/matrix/api"
	"gitlab.com/coybot/coybot/rssutil"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func init() {
	var isHelp bool
	var isDebug bool
	var isValidate bool

	flag.BoolVar(&isHelp, "help", false, "print this message")
	flag.BoolVar(&isDebug, "debug", false, "print debug information")
	flag.BoolVar(&isValidate, "validate", false, "validate the configuration and exit")
	flag.StringVar(&config.configFile, "config", "config.yaml", "config file")
	flag.Parse()

	if isHelp {
		flag.Usage()
		os.Exit(0)
	}

	if isDebug {
		log.SetFlags(log.LstdFlags | log.Lshortfile | log.Ltrace)
	}

	initConfig(isValidate)
}

func initConfig(onlyValidate bool) {
	loadConfig(config.configFile)
	reasons, ok := validateConfig()
	if len(reasons) > 0 {
		for _, s := range reasons {
			if strings.HasPrefix(s, "ERROR: ") {
				log.Error(s[len("ERROR: "):])
			} else if strings.HasPrefix(s, "WARNING: ") {
				log.Warning(s[len("WARNING: "):])
			} else {
				fmt.Println(s)
			}
		}
	}
	if !ok {
		b, _ := json.MarshalIndent(config, "", "  ")
		fmt.Printf("%s\n", b)
		os.Exit(1)
	}
	if onlyValidate {
		fmt.Println("Validate OK")
		os.Exit(0)
	}
}

func main() {
	var userData []sender

	if config.Matrix != nil {
		userData = append(userData, matrixSender{
			name: config.Matrix.Name,
			api: &matrixapi.Matrix{
				HomeServer:  config.Matrix.HomeServer,
				AccessToken: config.Matrix.AccessToken,
			},
			roomID:   config.Matrix.RoomID,
			template: config.Matrix.Template,
			format:   config.Matrix.Format,
		})
		mustAddTemplate(config.Matrix.Name, config.Matrix.Template)
	}

	if config.Telegram != nil {
		tgapi, err := tgbotapi.NewBotAPI(config.Telegram.Token)
		if err != nil {
			log.Fatal(err)
		}
		userData = append(userData, telegramSender{
			name:     config.Telegram.Name,
			api:      tgapi,
			chatID:   config.Telegram.ChatID,
			template: config.Telegram.Template,
			format:   config.Telegram.Format,
		})
		mustAddTemplate(config.Telegram.Name, config.Telegram.Template)
	}

	go func() {
		log.Fatal(rssutil.Serve(config.RSS.Source, config.RSS.TTL, userData, OnRSSFeedInit, OnRSSUpdate))
	}()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	endless := true
	for endless {
		switch <-sc {
		case syscall.SIGHUP:
			// TODO reload config
			log.Error("TODO: Reload")
			initConfig(false)
		case syscall.SIGINT, syscall.SIGTERM:
			fmt.Print("\r") // Clear '^C' char
			log.Info("Stop")
			rssutil.Stop()
			signal.Stop(sc)
			close(sc)
			endless = false
		}
	}
}

type messager interface {
	Type() string
	Message() string
}

type message struct {
	format string // "text", "html" or "markdown"
	msg    string
}

func (m message) Type() string    { return m.format }
func (m message) Message() string { return m.msg }
func newRSSMessage(name, template, format string, item rssutil.RSSItem) *message {
	buf := bytes.NewBuffer(nil)
	if err := tpl.ExecuteTemplate(buf, name, item); err != nil {
		log.Error(err)
	}
	return &message{format: format, msg: buf.String()}
}

type sender interface {
	Name() string
	Template() string
	Format() string
	Send(messager) error
}

type matrixSender struct {
	name     string
	api      *matrixapi.Matrix
	roomID   string
	template string
	format   string
}

func (sndr matrixSender) Name() string     { return sndr.name }
func (sndr matrixSender) Template() string { return sndr.template }
func (sndr matrixSender) Format() string   { return sndr.format }
func (sndr matrixSender) Send(msgr messager) error {
	var msg *matrixapi.RoomMessageText
	switch msgr.Type() {
	case "markdown":
		msg = matrixapi.NewRoomMessageText(msgr.Message(), "")
	case "html":
		msg = matrixapi.NewRoomMessageText(
			strings.TrimSpace(
				regexp.MustCompile(`\[(.*?)\]\(.*?\)`).
					ReplaceAllString(
						htmlToMarkdown(msgr.Message()),
						"$1"),
			),
			msgr.Message(),
		)
	default:
		msg = matrixapi.NewRoomMessageText(msgr.Message(), "")
	}
	_, err := sndr.api.SendRoomMessage(sndr.roomID, msg)
	return err
}

type telegramSender struct {
	name     string
	api      *tgbotapi.BotAPI
	chatID   int64
	template string
	format   string
}

func (sndr telegramSender) Name() string     { return sndr.name }
func (sndr telegramSender) Template() string { return sndr.template }
func (sndr telegramSender) Format() string   { return sndr.format }
func (sndr telegramSender) Send(msgr messager) error {
	m := tgbotapi.NewMessage(sndr.chatID, msgr.Message())
	m.ParseMode = msgr.Type()
	m.DisableNotification, m.DisableWebPagePreview = true, true
	_, err := sndr.api.Send(m)
	return err
}

// OnRSSFeedInit is a notifier which will be triggered on RSS source is feed for the first time.
func OnRSSFeedInit(items rssutil.RSSItems, userData interface{}) {
	log.Infoln("OnRSSFeedInit()", len(items), "items")
	log.Debugf("userData: %[1]T, %#[1]v", userData)
}

// OnRSSUpdate is a notifier which will be triggered on new RSS items are coming.
func OnRSSUpdate(items rssutil.RSSItems, userData interface{}) {
	log.Infoln("OnRSSUpdate()", len(items), "items")
	log.Debugf("userData: %[1]T, %#[1]v", userData)

	sort.Slice(items, func(i, j int) bool { return (items[j].PubDate.After(*items[i].PubDate)) })

	senders := userData.([]sender)

	for _, item := range items {
		for _, sndr := range senders {
			go func(sndr sender, item rssutil.RSSItem) {
				m := newRSSMessage(sndr.Name(), sndr.Template(), sndr.Format(), item)
				if err := sndr.Send(m); err != nil {
					log.Errorf("%s %s", sndr.Name(), err)
				}
				log.Infof("Sent message to %s: Title:%q", sndr.Name(), item.Title)
			}(sndr, item)
		}
		time.Sleep(100 * time.Millisecond) // Be gentle on sending.
	}
}

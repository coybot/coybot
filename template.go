// vim: ts=4 sts=4 sw=4 noet

// Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
// All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package main

import (
	"regexp"
	"strings"
	"text/template"

	"gitlab.com/coybot/coybot/log"

	"github.com/lunny/html2md"
)

var (
	tpl            *template.Template
	reMarkdownLink *regexp.Regexp
)

func init() {
	tpl = template.New("").Funcs(template.FuncMap{
		"removeAd":       removeAd,
		"htmlToMarkdown": htmlToMarkdown,
	})

	reMarkdownLink = regexp.MustCompile(`\[(.*?)\]\((.*?)\)`)
}

func addTemplate(name, text string) (err error) {
	tpl, err = tpl.New(name).Parse(text)
	if err != nil {
		log.Error(err)
	}
	return err
}

func mustAddTemplate(name, text string) {
	if err := addTemplate(name, text); err != nil {
		log.Fatal(err)
	}
}

func removeAd(src, ad string) string {
	return strings.Replace(src, ad, "", -1)
}

func htmlToMarkdown(src string) string {
	return html2md.Convert(src)
}
